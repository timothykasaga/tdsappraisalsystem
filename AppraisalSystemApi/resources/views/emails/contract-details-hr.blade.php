<div>
    <h5>EXPIRE IN {{$periodGroup->category}} MONTH(S)</h5>
    <table border="1" style="border-collapse: collapse">
        <thead>
            <tr>
                <th>No.</th>
                <th>Employee Name</th>
                <th>Contract Reference</th>
                <th>Contract Start Date</th>
                <th>Contract End Date</th>
                <th>Months Left</th>
                <th>Days Left</th>
            </tr>
        </thead>
        <tbody>
            @foreach($periodGroup->contracts as $contract)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$contract->firstName .' '.$contract->lastName}}</td>
                    <td>{{$contract->contractReference}}</td>
                    <td>{{$contract->startDate}}</td>
                    <td>{{$contract->expiryDate}}</td>
                    <td>{{$contract->monthsLeft}}</td>
                    <td>{{$contract->daysLeft}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>