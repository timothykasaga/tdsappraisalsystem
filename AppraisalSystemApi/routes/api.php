<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




/* Begin Organization Routes */

Route::get('/organizations', 'OrganizationController@all')->name('organizations.all');

Route::post('/organization', 'OrganizationController@store')->name('organizations.store');

Route::get('/organization/{organization}', 'OrganizationController@show')->name('organizations.show');

Route::put('/organization/{organization}', 'OrganizationController@update')->name('organizations.update');

Route::delete('/organization/{organization}', 'OrganizationController@destroy')->name('organizations.destroy');

/* End Organization Routes */





/* Begin Regional Office Routes */

Route::get('/regional-offices', 'RegionalOfficeController@all')->name('regional-offices.all');

Route::post('/regional-office', 'RegionalOfficeController@store')->name('regional-offices.store');

Route::get('/regional-office/{regionalOffice}', 'RegionalOfficeController@show')->name('regional-offices.show');

Route::put('/regional-office/{regionalOffice}', 'RegionalOfficeController@update')->name('regional-offices.update');

Route::delete('/regional-office/{regionalOffice}', 'RegionalOfficeController@destroy')->name('regional-offices.destroy');

/* End Regional Office Routes */




/* Begin Department Routes */

Route::get('/departments', 'DepartmentController@all')->name('departments.all');

Route::post('/department', 'DepartmentController@store')->name('departments.store');

Route::get('/department/{department}', 'DepartmentController@show')->name('departments.show');

Route::put('/department/{department}', 'DepartmentController@update')->name('departments.update');

Route::delete('/department/{department}', 'DepartmentController@destroy')->name('departments.destroy');

/* End Department Routes */




/* Begin Role Code Routes */

Route::get('/role-codes', 'RoleCodeController@all')->name('role-codes.all');

Route::post('/role-code', 'RoleCodeController@store')->name('role-codes.store');

Route::get('/role-code/{roleCode}', 'RoleCodeController@show')->name('role-codes.show');

Route::put('/role-code/{roleCode}', 'RoleCodeController@update')->name('role-codes.update');

Route::delete('/role-code/{roleCode}', 'RoleCodeController@destroy')->name('role-codes.destroy');

/* End Role Code Routes */




/* Begin Employee Category Routes */

Route::get('/employee-categories', 'EmployeeCategoryController@all')->name('employee-categories.all');

Route::post('/employee-category', 'EmployeeCategoryController@store')->name('employee-categories.store');

Route::get('/employee-category/{employeeCategory}', 'EmployeeCategoryController@show')->name('employee-categories.show');

Route::put('/employee-category/{employeeCategory}', 'EmployeeCategoryController@update')->name('employee-categories.update');

Route::delete('/employee-category/{employeeCategory}', 'EmployeeCategoryController@destroy')->name('employee-categories.destroy');

/* End Employee Category Routes */




/* Begin User Routes */

Route::get('/users/auth-user', 'UserController@authenticatedUser')->name('users.auth-user');

Route::get('/users', 'UserController@all')->name('users.all');

Route::post('/user', 'UserController@store')->name('users.store');

Route::get('/user/{user}', 'UserController@show')->name('users.show');

Route::put('/user/{user}', 'UserController@update')->name('users.update');

Route::put('/user/owner-profile-update/{user}', 'UserController@updateUserByProfileOwner')->name('users.update-by-owner');

Route::delete('/user/{user}', 'UserController@destroy')->name('users.destroy');

Route::post('/user/login', 'UserController@login')->name('user.login');

Route::post('/user/reset-password', 'UserController@resetPassword')->name('user.reset-password');

Route::post('/user/change-password', 'UserController@changePassword')->name('user.change-password');

Route::post('/user/password/change-default', 'UserController@changeDefaultPassword')->name('user.password.change-default');;

Route::get('/login',function (){

    return \App\Helpers\ResponseHandler::failureResponse("Authentication Required");

})->name('login');

/* End User Routes */



/* Begin User Academic Bg Routes */

Route::get('/user-academic-backgrounds/{user}', 'UserAcademicBackgroundController@all')->name('user-academic-backgrounds.all');

Route::post('/user-academic-background', 'UserAcademicBackgroundController@store')->name('user-academic-backgrounds.store');

Route::get('/user-academic-background/{userAcademicBackground}', 'UserAcademicBackgroundController@show')->name('user-academic-backgrounds.show');

Route::put('/user-academic-background/{userAcademicBackground}', 'UserAcademicBackgroundController@update')->name('user-academic-backgrounds.update');

Route::delete('/user-academic-background/{userAcademicBackground}', 'UserAcademicBackgroundController@destroy')->name('user-academic-backgrounds.destroy');

/* End User Academic Bg Routes */



/* Begin User Contracts Routes */

Route::get('/user-contracts/{user}', 'UserContractController@all')->name('user-contracts.all');

Route::post('/user-contract', 'UserContractController@store')->name('user-contracts.store');

Route::get('/user-contract/{userContract}', 'UserContractController@show')->name('user-contracts.show');

Route::put('/user-contract/{userContract}', 'UserContractController@update')->name('user-contracts.update');

Route::delete('/user-contract/{userContract}', 'UserContractController@destroy')->name('user-contracts.destroy');

/* End User Contracts Routes */



/* Begin Application Stats Routes */

Route::get('/app-stats', 'ApplicationStatController@all')->name('app-stats.all');

/* End Application Stats Routes */



/* Begin Appraisal Routes */

Route::get('/appraisal/{appraisal}', 'AppraisalController@show')->name('appraisals.show');
Route::post('/appraisal', 'AppraisalController@store')->name('appraisals.store');
Route::post('/appraisal/workflow-start', 'AppraisalController@workflowStart')->name('appraisals.workflow-start');
Route::post('/appraisal/workflow-move', 'AppraisalController@workflowMove')->name('appraisals.workflow-move');
Route::get('/appraisal/workflow-cancel/{appraisal}', 'AppraisalController@workflowCancel')->name('appraisals.workflow-cancel');
Route::post('/appraisals', 'AppraisalController@all')->name('appraisals.all');

Route::post('/appraisal/update-approvers', 'AppraisalController@updateApprovers')->name('appraisals.update-approvers');

Route::get('/appraisals/latest', 'AppraisalController@latestAppraisals')->name('appraisals.latest');

/* End Appraisal Routes */



/* Begin Appraisal Strategic Objective Routes */

Route::get('/appraisal-strategic-objectives',
    'AppraisalStrategicObjectiveController@all')->name('appraisal-strategic-objectives.all');
Route::post('/appraisal-strategic-objective',
    'AppraisalStrategicObjectiveController@store')->name('appraisal-strategic-objectives.store');
Route::put('/appraisal-strategic-objective/{appraisalStrategicObjective}',
    'AppraisalStrategicObjectiveController@update')->name('appraisal-strategic-objectives.update');
Route::delete('/appraisal-strategic-objective/{appraisalStrategicObjective}',
    'AppraisalStrategicObjectiveController@destroy')->name('appraisal-strategic-objectives.destroy');


/* End Appraisal Strategic Objective Routes */



/* Begin Appraisal Competence Categories Routes */

Route::get('/appraisal-competence-categories/{appraisalReference}',
    'AppraisalCompetenceCategoryController@all')->name('appraisal-competence-categories.all');

Route::post('/appraisal-competence-category',
    'AppraisalCompetenceCategoryController@store')->name('appraisal-competence-categories.store');

Route::put('/appraisal-competence-category/{appraisalCompetenceCategory}',
    'AppraisalCompetenceCategoryController@update')->name('appraisal-competence-categories.update');

Route::delete('/appraisal-competence-category/{appraisalCompetenceCategory}',
    'AppraisalCompetenceCategoryController@destroy')->name('appraisal-competence-categories.destroy');

Route::get('/admin/appraisal-competence-categories',
    'AppraisalCompetenceCategoryController@adminAll')->name('admin.appraisal-competence-categories.all');
/* End Appraisal Competence Categories Routes */



/* Begin Appraisal Competences */

Route::get('/appraisal-competences',
    'AppraisalCompetenceController@all')->name('appraisal-competences.all');

Route::get('/appraisal-competences/{id}',
    'AppraisalCompetenceController@allCompetenceCategoryCompetences')->name('appraisal-competences.all-for-category-id');

Route::post('/appraisal-competence',
    'AppraisalCompetenceController@store') ->name('appraisal-competences.store');

Route::put('/appraisal-competence/{appraisalCompetence}',
    'AppraisalCompetenceController@update') ->name('appraisal-competences.update');

Route::delete('/appraisal-competence/{appraisalCompetence}',
    'AppraisalCompetenceController@destroy') ->name('appraisal-competences.destroy');
/* End Appraisal Competences */




/* Begin Appraisal Personal Details Routes */

Route::get('/appraisal-personal-details',
    'AppraisalPersonalDetailController@all')->name('appraisal-personal-details.all');

Route::put('/appraisal-personal-detail/{appraisalPersonalDetail}',
    'AppraisalPersonalDetailController@update')->name('appraisal-personal-details.update');

/* End Appraisal Academic Personal Details Routes */



/* Begin Appraisal Academic Backgrounds Routes */

Route::get('/appraisal-academic-backgrounds',
    'AppraisalAcademicBackgroundController@all')->name('appraisal-academic-backgrounds.all');

Route::post('/appraisal-academic-backgrounds/save',
    'AppraisalAcademicBackgroundController@store')->name('appraisal-academic-backgrounds.store');

Route::post('/appraisal-academic-backgrounds/update',
    'AppraisalAcademicBackgroundController@update')->name('appraisal-academic-backgrounds.update');

/* End Appraisal Academic Backgrounds Routes */




/* Begin Appraisal Key Duties Routes */

Route::get('/appraisal-key-duties',
    'AppraisalKeyDutyController@all')->name('appraisal-key-duties.all');

Route::post('/appraisal-key-duties/save',
    'AppraisalKeyDutyController@store')->name('appraisal-key-duties.store');

Route::post('/appraisal-key-duties/update',
    'AppraisalKeyDutyController@update')->name('appraisal-key-duties.update');

/* End Appraisal Key Duties Routes */



/* Begin Appraisal Assignment Routes */

Route::get('/appraisal-assignments',
    'AppraisalAssignmentController@all')->name('appraisal-assignments.all');

Route::post('/appraisal-assignments/save',
    'AppraisalAssignmentController@store')->name('appraisal-assignments.store');

Route::post('/appraisal-assignments/update',
    'AppraisalAssignmentController@update')->name('appraisal-assignments.update');

/* End Appraisal Assignment Routes */



/* Begin Appraisal Additional Assignment Routes */

Route::get('/appraisal-additional-assignments',
    'AppraisalAdditionalAssignmentController@all')->name('appraisal-additional-assignments.all');

Route::post('/appraisal-additional-assignments/save',
    'AppraisalAdditionalAssignmentController@store')->name('appraisal-additional-assignments.store');

Route::post('/appraisal-additional-assignments/update',
    'AppraisalAdditionalAssignmentController@update')->name('appraisal-additional-assignments.update');

/* End Appraisal Additional Assignment Routes */



/* Begin Appraisal Competence Assessments Routes */

Route::get('/appraisal-competence-assessments',
    'AppraisalCompetenceAssessmentController@all')->name('appraisal-competence-assessments.all');

Route::post('/appraisal-competence-assessments/save',
    'AppraisalCompetenceAssessmentController@store')->name('appraisal-competence-assessments.store');

Route::post('/appraisal-competence-assessments/update',
    'AppraisalCompetenceAssessmentController@update')->name('appraisal-competence-assessments.update');

/* End Appraisal Competence Assessments Routes */



/* Begin Appraisal Performance Gaps Routes */

Route::get('/appraisal-performance-gaps',
    'AppraisalTrainPerformanceGapController@all')->name('appraisal-performance-gaps.all');

Route::post('/appraisal-performance-gaps/save',
    'AppraisalTrainPerformanceGapController@store')->name('appraisal-performance-gaps.store');

Route::post('/appraisal-performance-gaps/update',
    'AppraisalTrainPerformanceGapController@update')->name('appraisal-performance-gaps.update');

/* End Appraisal Performance Gaps Routes */


/* Begin Appraisal Performance Challenges Routes */

Route::get('/appraisal-performance-challenges',
    'AppraisalTrainChallengeController@all')->name('appraisal-performance-challenges.all');

Route::post('/appraisal-performance-challenges/save',
    'AppraisalTrainChallengeController@store')->name('appraisal-performance-challenges.store');

Route::post('/appraisal-performance-challenges/update',
    'AppraisalTrainChallengeController@update')->name('appraisal-performance-challenges.update');

/* End Appraisal Performance Challenges Routes */


/* Begin Appraisal Work plans Routes */

Route::get('/appraisal-work-plans',
    'AppraisalWorkplanController@all')->name('appraisal-work-plans.all');

Route::post('/appraisal-work-plans/save',
    'AppraisalWorkplanController@store')->name('appraisal-work-plans.store');

Route::post('/appraisal-work-plans/update',
    'AppraisalWorkplanController@update')->name('appraisal-work-plans.update');

/* End Appraisal Work plans  Routes */



//******************************************

/* Begin appraisalAssignmentScore Routes */
Route::get('/appraisal-assignment-scores', 'AppraisalAssignmentScoreController@all')->name('appraisal-assignment-scores.all');

Route::post('/appraisal-assignment-score', 'AppraisalAssignmentScoreController@store')->name('appraisal-assignment-scores.store');

Route::get('/appraisal-assignment-score/{appraisalAssignmentScore}', 'AppraisalAssignmentScoreController@show')->name('appraisal-assignment-scores.show');

Route::put('/appraisal-assignment-score/{appraisalAssignmentScore}', 'AppraisalAssignmentScoreController@update')->name('appraisal-assignment-scores.update');

Route::delete('/appraisal-assignment-score/{appraisalAssignmentScore}', 'AppraisalAssignmentScoreController@destroy')->name('appraisal-assignment-scores.destroy');
/* End appraisalAssignmentScore */

/* Begin appraisalAdditionalAssignmentScore Routes */
Route::get('/appraisal-additional-assignment-scores', 'AppraisalAdditionalAssignmentScoreController@all')->name('appraisal-additional-assignment-scores.all');

Route::post('/appraisal-additional-assignment-score', 'AppraisalAdditionalAssignmentScoreController@store')->name('appraisal-additional-assignment-scores.store');

Route::get('/appraisal-additional-assignment-score/{assignmentScore}', 'AppraisalAdditionalAssignmentScoreController@show')->name('appraisal-additional-assignment-scores.show');

Route::put('/appraisal-additional-assignment-score/{assignmentScore}', 'AppraisalAdditionalAssignmentScoreController@update')->name('appraisal-additional-assignment-scores.update');

Route::delete('/appraisal-additional-assignment-score/{assignmentScore}', 'AppraisalAdditionalAssignmentScoreController@destroy')->name('appraisal-additional-assignment-scores.destroy');
/* End appraisalAdditionalAssignmentScore Routes */

/* Begin appraisalAssignmentSummary Routes */
Route::get('/appraisal-assignment-summaries', 'AppraisalAssignmentSummaryController@all')->name('appraisal-assignment-summaries.all');

Route::post('/appraisal-assignment-summary', 'AppraisalAssignmentSummaryController@store')->name('appraisal-assignment-summaries.store');

Route::get('/appraisal-assignment-summary/{appraisalAssignmentSummary}', 'AppraisalAssignmentSummaryController@show')->name('appraisal-assignment-summaries.show');

Route::put('/appraisal-assignment-summary/{appraisalAssignmentSummary}', 'AppraisalAssignmentSummaryController@update')->name('appraisal-assignment-summaries.update');

Route::delete('/appraisal-assignment-summary/{appraisalAssignmentSummary}', 'AppraisalAssignmentSummaryController@destroy')->name('appraisal-assignment-summaries.destroy');
/* End appraisalAssignmentSummary Routes */

/* Begin appraisalCompetenceAssessmentScore Routes */
Route::get('/appraisal-competence-assessment-scores', 'AppraisalCompetenceAssessmentScoreController@all')->name('appraisal-competence-assessment-scores.all');

Route::post('/appraisal-competence-assessment-score', 'AppraisalCompetenceAssessmentScoreController@store')->name('appraisal-competence-assessment-scores.store');

Route::get('/appraisal-competence-assessment-score/{assessmentScore}', 'AppraisalCompetenceAssessmentScoreController@show')->name('appraisal-competence-assessment-scores.show');

Route::put('/appraisal-competence-assessment-score/{assessmentScore}', 'AppraisalCompetenceAssessmentScoreController@update')->name('appraisal-competence-assessment-scores.update');

Route::delete('/appraisal-competence-assessment-score/{assessmentScore}', 'AppraisalCompetenceAssessmentScoreController@destroy')->name('appraisal-competence-assessment-scores.destroy');
/* End appraisalCompetenceAssessmentScore Routes */

/* Begin appraisalCompetenceAssessmentSummary Routes */
Route::get('/appraisal-competence-assessment-summaries', 'AppraisalCompetenceAssessmentSummaryController@all')->name('appraisal-competence-assessment-summaries.all');

Route::post('/appraisal-competence-assessment-summary', 'AppraisalCompetenceAssessmentSummaryController@store')->name('appraisal-competence-assessment-summaries.store');

Route::get('/appraisal-competence-assessment-summary/{assessmentSummary}', 'AppraisalCompetenceAssessmentSummaryController@show')->name('appraisal-competence-assessment-summaries.show');

Route::put('/appraisal-competence-assessment-summary/{assessmentSummary}', 'AppraisalCompetenceAssessmentSummaryController@update')->name('appraisal-competence-assessment-summaries.update');

Route::delete('/appraisal-competence-assessment-summary/{assessmentSummary}', 'AppraisalCompetenceAssessmentSummaryController@destroy')->name('appraisal-competence-assessment-summaries.destroy');
/* End appraisalCompetenceAssessmentSummary Routes */

/* Begin appraisalTrainSummary Routes */
Route::get('/appraisal-train-summaries', 'AppraisalTrainSummaryController@all')->name('appraisal-train-summaries.all');

Route::post('/appraisal-train-summary', 'AppraisalTrainSummaryController@store')->name('appraisal-train-summaries.store');

Route::get('/appraisal-train-summary/{appraisalTrainSummary}', 'AppraisalTrainSummaryController@show')->name('appraisal-train-summaries.show');

Route::put('/appraisal-train-summary/{appraisalTrainSummary}', 'AppraisalTrainSummaryController@update')->name('appraisal-train-summaries.update');

Route::delete('/appraisal-train-summary/{appraisalTrainSummary}', 'AppraisalTrainSummaryController@destroy')->name('appraisal-train-summaries.destroy');
/* End appraisalTrainSummary Routes */


//*******************************************************



/* Begin Appraisal Appraiser Comments Routes */

Route::get('/appraisal-appraiser-comments', 'AppraisalAppraiserCommentController@all')->name('appraisal-appraiser-comments.all');

Route::post('/appraisal-appraiser-comment', 'AppraisalAppraiserCommentController@store')->name('appraisal-appraiser-comments.store');

Route::get('/appraisal-appraiser-comment/{appraisalAppraiserComment}', 'AppraisalAppraiserCommentController@show')->name('appraisal-appraiser-comments.show');

Route::put('/appraisal-appraiser-comment/{appraisalAppraiserComment}', 'AppraisalAppraiserCommentController@update')->name('appraisal-appraiser-comments.update');

Route::delete('/appraisal-appraiser-comment/{appraisalAppraiserComment}', 'AppraisalAppraiserCommentController@destroy')->name('appraisal-appraiser-comments.destroy');

/* End Appraisal Appraiser Comments Routes */



/* Begin Appraisal Strengths And Weaknesses Routes */

Route::get('/appraisal-strengths-and-weaknesses', 'AppraisalStrengthAndWeeknessController@all')->name('appraisal-strengths-and-weaknesses.all');

Route::post('/appraisal-strengths-and-weakness', 'AppraisalStrengthAndWeeknessController@store')->name('appraisal-strengths-and-weaknesses.store');

Route::get('/appraisal-strengths-and-weakness/{appraisalStrengthAndWeekness}', 'AppraisalStrengthAndWeeknessController@show')->name('appraisal-strengths-and-weaknesses.show');

Route::put('/appraisal-strengths-and-weakness/{appraisalStrengthAndWeekness}', 'AppraisalStrengthAndWeeknessController@update')->name('appraisal-strengths-and-weaknesses.update');

Route::delete('/appraisal-strengths-and-weakness/{appraisalStrengthAndWeekness}', 'AppraisalStrengthAndWeeknessController@destroy')->name('appraisal-strengths-and-weaknesses.destroy');

/* End Appraisal Strengths and weaknesses Routes */



/* Begin Appraisal Recommendations Routes */

Route::get('/appraisal-appraiser-recommendations', 'AppraisalAppraiserRecommendationController@all')->name('appraisal-appraiser-recommendations.all');

Route::post('/appraisal-appraiser-recommendation', 'AppraisalAppraiserRecommendationController@store')->name('appraisal-appraiser-recommendations.store');

Route::get('/appraisal-appraiser-recommendation/{appraisalAppraiserRecommendation}', 'AppraisalAppraiserRecommendationController@show')->name('appraisal-appraiser-recommendations.show');

Route::put('/appraisal-appraiser-recommendation/{appraisalAppraiserRecommendation}', 'AppraisalAppraiserRecommendationController@update')->name('appraisal-appraiser-recommendations.update');

Route::delete('/appraisal-appraiser-recommendation/{appraisalAppraiserRecommendation}', 'AppraisalAppraiserRecommendationController@destroy')->name('appraisal-appraiser-recommendations.destroy');

/* End Appraisal Recommendations Routes */


/* Begin Appraisal Supervisor declaration Routes */

Route::get('/appraisal-supervisor-declarations', 'AppraisalSupervisorDeclarationController@all')->name('appraisal-supervisor-declarations.all');

Route::post('/appraisal-supervisor-declaration', 'AppraisalSupervisorDeclarationController@store')->name('appraisal-supervisor-declarations.store');

Route::get('/appraisal-supervisor-declaration/{appraisalSupervisorDeclaration}', 'AppraisalSupervisorDeclarationController@show')->name('appraisal-supervisor-declarations.show');

Route::put('/appraisal-supervisor-declaration/{appraisalSupervisorDeclaration}', 'AppraisalSupervisorDeclarationController@update')->name('appraisal-supervisor-declarations.update');

Route::delete('/appraisal-supervisor-declaration/{appraisalSupervisorDeclaration}', 'AppraisalSupervisorDeclarationController@destroy')->name('appraisal-supervisor-declarations.destroy');

/* End Appraisal Supervisor Declarations Routes */



/* Begin Appraisal HOD Comments Routes */

Route::get('/appraisal-hod-comments', 'AppraisalHodCommentController@all')->name('appraisal-hod-comments.all');

Route::post('/appraisal-hod-comment', 'AppraisalHodCommentController@store')->name('appraisal-hod-comments.store');

Route::get('/appraisal-hod-comment/{appraisalHodComment}', 'AppraisalHodCommentController@show')->name('appraisal-hod-comments.show');

Route::put('/appraisal-hod-comment/{appraisalHodComment}', 'AppraisalHodCommentController@update')->name('appraisal-hod-comments.update');

Route::delete('/appraisal-hod-comment/{appraisalHodComment}', 'AppraisalHodCommentController@destroy')->name('appraisal-hod-comments.destroy');

/* End Appraisal Hod Comments Routes */



/* Begin Appraisal Appraisee Remarks Routes */

Route::get('/appraisal-appraisee-remarks', 'AppraisalAppraiseeRemarkController@all')->name('appraisal-appraisee-remarks.all');

Route::post('/appraisal-appraisee-remark', 'AppraisalAppraiseeRemarkController@store')->name('appraisal-appraisee-remarks.store');

Route::get('/appraisal-appraisee-remark/{appraisalAppraiseeRemark}', 'AppraisalAppraiseeRemarkController@show')->name('appraisal-appraisee-remarks.show');

Route::put('/appraisal-appraisee-remark/{appraisalAppraiseeRemark}', 'AppraisalAppraiseeRemarkController@update')->name('appraisal-appraisee-remarks.update');

Route::delete('/appraisal-appraisee-remark/{appraisalAppraiseeRemark}', 'AppraisalAppraiseeRemarkController@destroy')->name('appraisal-appraisee-remarks.destroy');

/* End Appraisal Appraisee Remarks Routes */


/* Begin Appraisal Director Comments Routes */

Route::get('/appraisal-director-comments', 'AppraisalDirectorCommentController@all')->name('appraisal-director-comments.all');

Route::post('/appraisal-director-comment', 'AppraisalDirectorCommentController@store')->name('appraisal-director-comments.store');

Route::get('/appraisal-director-comment/{appraisalDirectorComment}', 'AppraisalDirectorCommentController@show')->name('appraisal-director-comments.show');

Route::put('/appraisal-director-comment/{appraisalDirectorComment}', 'AppraisalDirectorCommentController@update')->name('appraisal-director-comments.update');

Route::delete('/appraisal-director-comment/{appraisalDirectorComment}', 'AppraisalDirectorCommentController@destroy')->name('appraisal-director-comments.destroy');

/* End Appraisal Director Comments Routes */


/*Begin Error log routes*/

Route::post('/error-log', 'ErrorLogController@store')->name('error-log.store');

/*End Error log routes*/


/* Begin Letters Routes */

Route::post('/letters', 'LetterController@all')->name('letters.all');

Route::post('/letter', 'LetterController@store')->name('letters.store');

Route::get('/letter/{letter}', 'LetterController@show')->name('letters.show');

Route::put('/letter/{letter}', 'LetterController@update')->name('letters.update');

Route::delete('/letter/{letter}', 'LetterController@destroy')->name('letters.destroy');

Route::post('/letter/workflow-start', 'LetterController@letterWorkflowStart')->name('letters.workflow-start');

Route::post('/letter/workflow-move', 'LetterController@letterWorkflowMove')->name('letters.workflow-move');

/* End Letters Routes */

/* Begin letter movement routes */

Route::get('/letter-movements/{letterCode}', 'LetterMovementController@all')->name('letter-movements.all');

Route::post('/letter-movement', 'LetterMovementController@store')->name('letter-movements.store');

Route::get('/letter-movement/{letterMovement}', 'LetterMovementController@show')->name('letter-movements.show');

Route::put('/letter-movement/{letterMovement}', 'LetterMovementController@update')->name('letter-movements.update');

Route::delete('/letter-movement/{letterMovement}', 'LetterMovementController@destroy')->name('letter-movements.destroy');

/* End letter movement routes */



