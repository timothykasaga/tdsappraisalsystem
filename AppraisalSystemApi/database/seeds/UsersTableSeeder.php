<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'username' => 'administrator',
            'first_name' => 'admin',
            'last_name' => 'user',
            'email' => 'it@ppda.go.ug',
            'role_code' => 'ADMIN',
            'category_code' => 'C0001',
            'department_code' => 'IT',
            'regional_office_code' => 'R0001',
            'org_code' => 'PPDA',
            'password' => bcrypt('Timothy93.com'),
            'created_by' => 'SYSTEM',
        ]);

    }
}
