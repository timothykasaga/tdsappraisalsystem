<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('letter_code');
            $table->string('movement_status')->nullable();

            $table->string('receptionist_id')->nullable();
            $table->string('receptionist_status')->nullable()->default('PENDING');
            $table->date('receptionist_action_date')->nullable();

            $table->date('registry_submission_date')->nullable();
            $table->string('registry_user_id')->nullable();
            $table->string('registry_status')->nullable()->default('PENDING');
            $table->date('registry_action_date')->nullable();

            $table->date('executive_director_submission_date')->nullable();
            $table->string('executive_director_id')->nullable();
            $table->string('executive_director_status')->nullable()->default('PENDING');
            $table->date('executive_director_action_date')->nullable();

            $table->date('employee_submission_date')->nullable();
            $table->string('employee_assigned')->nullable();
            $table->string('employee_status')->nullable()->default('PENDING');
            $table->date('employee_action_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letters');
    }
}
