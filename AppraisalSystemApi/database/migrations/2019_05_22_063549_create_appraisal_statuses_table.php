<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_statuses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('status_code');
            $table->string('status_description');
            $table->string('from_status');
            $table->string('to_status_approved');
            $table->string('to_status_rejected');
            $table->boolean('editable_owner')->default(false);
            $table->boolean('editable_supervisor')->default(false);
            $table->boolean('editable_department_head')->default(false);
            $table->boolean('editable_executive_director')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_statuses');
    }
}
