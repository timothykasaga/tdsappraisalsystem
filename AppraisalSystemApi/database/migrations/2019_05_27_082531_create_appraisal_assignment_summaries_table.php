<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalAssignmentSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('appraisal_assignment_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->double('section_d_percentage_score');
            $table->double('section_d_weighed_score');
            $table->text('appraiser_comment');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_assignment_summaries');
    }
}
