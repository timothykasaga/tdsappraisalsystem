<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('username')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('other_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');


            $table->string('role_code')->nullable();
            $table->string('letter_movement_role_code')->nullable();
            $table->string('category_code')->nullable();
            $table->string('regional_office_code')->nullable();
            $table->string('department_code')->nullable();
            $table->string('org_code')->nullable();


            $table->string('staff_number')->nullable();
            $table->string('designation')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('contract_start_date')->nullable();
            $table->date('contract_expiry_date')->nullable();


            $table->string('phone')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('reset')->default(true);
            $table->boolean('logged_on')->default(false);
            $table->string('access_token')->nullable();
            $table->dateTime('access_token_expiry_date')->nullable();
            $table->string('created_by');
            $table->dateTime('last_login_date')->nullable();
            $table->dateTime('last_password_change_date')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
