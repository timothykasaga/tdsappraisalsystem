<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletedUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_users', function (Blueprint $table) {

            $table->increments('id');
            $table->bigInteger('original_id');
            $table->string('username');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('other_name')->nullable();
            $table->string('email');
            $table->string('password');


            $table->string('role_code')->nullable();
            $table->string('category_code')->nullable();
            $table->string('regional_office_code')->nullable();
            $table->string('department_code')->nullable();
            $table->string('org_code')->nullable();


            $table->string('staff_number')->nullable();
            $table->string('designation')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('contract_start_date')->nullable();
            $table->date('contract_expiry_date')->nullable();

            $table->string('phone')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('reset')->default(true);
            $table->boolean('logged_on')->default(false);
            $table->string('access_token')->nullable();
            $table->dateTime('access_token_expiry_date')->nullable();
            $table->string('created_by');
            $table->dateTime('last_login_date')->nullable();
            $table->dateTime('last_password_change_date')->nullable();
            $table->string('deleted_by');
            $table->dateTime('deletion_date');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deleted_users', function (Blueprint $table) {
            //
        });
    }
}
