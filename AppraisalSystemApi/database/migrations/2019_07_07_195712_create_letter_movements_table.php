<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letter_code');
            $table->string('from_person');
            $table->date('from_date');
            $table->string('to_person');
            $table->date('to_date');
            $table->date('deadline_for_action');
            $table->text('required_action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_movements');
    }
}
