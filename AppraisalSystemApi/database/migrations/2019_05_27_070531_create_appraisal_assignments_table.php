<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('appraisal_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->bigInteger('appraisal_strategic_objective_id');
            $table->text('expected_output');
            $table->text('actual_performance');
            $table->double('maximum_rating')->nullable();
            $table->double('appraisee_rating')->nullable();
            $table->double('appraiser_rating')->nullable();
            $table->double('agreed_rating')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_assignments');
    }
}
