<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalPersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_personal_details', function (Blueprint $table) {

            $table->increments('id');
            $table->string('appraisal_reference')->unique();
            $table->string('staff_number')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('other_name')->nullable();
            $table->string('department');
            $table->string('designation');
            $table->string('employee_category')->nullable(0);
            $table->dateTime('date_of_birth');
            $table->dateTime('contract_start_date')->nullable();
            $table->dateTime('contract_expiry_date')->nullable();
            $table->dateTime('appraisal_period_start_date');
            $table->dateTime('appraisal_period_end_date');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_personal_details');
    }
}
