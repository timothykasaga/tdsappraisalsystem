<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letter_code');
            $table->string('sender');
            $table->string('subject')->nullable();
            $table->string('letter_type');
            $table->string('sender_reference_number')->nullable();
            $table->date('letter_date')->nullable();
            $table->string('attention_to')->nullable();
            $table->string('signatory')->nullable();
            $table->date('receipt_date');
            $table->string('receiving_officer');
            $table->date('date_to_destination')->nullable();
            $table->string('invoice_number')->nullable();
            $table->double('invoice_value')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_details');
    }
}
