<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalWorkplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_workplans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->text('job_assignment');
            $table->text('expected_output');
            $table->double('maximum_rating');
            $table->string('time_frame');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_workplans');
    }
}
