<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalCompetenceAssessmentSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_competence_assessment_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->double('section_e_percentage_score');
            $table->double('section_e_weighed_score');
            $table->double('section_d_score');
            $table->double('section_e_score');
            $table->double('appraisal_total_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_competence_assessment_summaries');
    }
}
