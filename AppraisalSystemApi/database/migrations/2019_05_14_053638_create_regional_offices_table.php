<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionalOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('regional_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('regional_office_code');
            $table->string('org_code');
            $table->string('email');
            $table->string('location')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('contact_person_contact')->nullable();
            $table->string('description')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional_offices');
    }
}
