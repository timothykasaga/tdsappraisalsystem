<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalAppraiseeRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_appraisee_remarks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->string('appraisee_name');
            $table->string('agreement_decision');
            $table->text('disagreement_reason')->nullable();
            $table->string('declaration_name');
            $table->string('declaration_initials');
            $table->date('declaration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_appraisee_remarks');
    }
}
