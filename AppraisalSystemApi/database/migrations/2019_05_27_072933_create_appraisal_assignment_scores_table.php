<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalAssignmentScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_assignment_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->double('total_maximum_rating')->nullable();
            $table->double('total_appraisee_rating')->nullable();
            $table->double('total_appraiser_rating')->nullable();
            $table->double('total_agreed_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_assignment_scores');
    }
}
