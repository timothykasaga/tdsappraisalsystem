<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalCompetenceAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_competence_assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->bigInteger('competence_category_id');
            $table->bigInteger('appraisal_competence_id');
            $table->double('maximum_rating')->nullable();
            $table->double('appraisee_rating')->nullable();
            $table->double('appraiser_rating')->nullable();
            $table->double('agreed_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_competence_assessments');
    }
}
