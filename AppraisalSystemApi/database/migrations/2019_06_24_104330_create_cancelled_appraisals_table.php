<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancelledAppraisalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancelled_appraisals', function (Blueprint $table) {
            $table->increments('id');

            $table->string('appraisal_reference');
            $table->string('appraisal_type');
            $table->string('owner_id');
            $table->string('appraisal_status');
            $table->string('generated_pdf_name');

            $table->string('department_head_id')->nullable();
            $table->string('department_head_decision')->default('PENDING')->nullable();
            $table->string('department_head_remark')->nullable();
            $table->dateTime('department_head_submission_date')->nullable();
            $table->dateTime('department_head_action_date')->nullable();

            $table->string('supervisor_id')->nullable();
            $table->string('supervisor_decision')->default('PENDING')->nullable();
            $table->string('supervisor_remark')->nullable();
            $table->dateTime('supervisor_submission_date')->nullable();
            $table->dateTime('supervisor_action_date')->nullable();

            $table->string('executive_director_id')->nullable();
            $table->string('executive_director_decision')->default('PENDING')->nullable();
            $table->string('executive_director_remark')->nullable();
            $table->dateTime('executive_director_submission_date')->nullable();
            $table->dateTime('executive_director_action_date')->nullable();

            $table->string('cancelled_by');
            $table->dateTime('date_cancelled');
            $table->string('appraisal_status_before_cancel');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelled_appraisals');
    }
}
