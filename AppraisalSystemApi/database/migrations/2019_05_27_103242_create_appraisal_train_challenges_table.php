<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalTrainChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_train_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->text('challenge');
            $table->text('causes');
            $table->text('recommendation');
            $table->string('when');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_train_challenges');
    }
}
