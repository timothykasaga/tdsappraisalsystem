<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletedOrganizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_organizations', function (Blueprint $table) {

            $table->increments('id');
            $table->bigInteger('original_id');
            $table->string('deleted_by');
            $table->dateTime('deletion_date');
            $table->string('name');
            $table->string('org_code');
            $table->string('email');
            $table->string('location')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('contact_person_contact')->nullable();
            $table->string('description')->nullable();
            $table->string('created_by');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deleted_organizations', function (Blueprint $table) {
            //
        });
    }
}
