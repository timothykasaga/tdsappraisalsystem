<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalSupervisorDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_supervisor_declarations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appraisal_reference');
            $table->string('appraiser_name');
            $table->string('appraisee_name');
            $table->string('duration');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_supervisor_declarations');
    }
}
