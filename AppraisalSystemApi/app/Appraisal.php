<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appraisal extends Model
{

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'appraisal_reference';
    }

    public function appraisalPersonalDetail(){
        return $this->hasOne('App\AppraisalPersonalDetail','appraisal_reference','appraisal_reference');
    }


    public function appraisalAcademicBackgrounds(){
        return $this->hasMany('App\AppraisalAcademicBackground','appraisal_reference','appraisal_reference');
    }

    public function appraisalKeyDuties(){
        return $this->hasMany('App\AppraisalKeyDuty','appraisal_reference','appraisal_reference');
    }

    public function appraisalAssignments(){
        return $this->hasMany('App\AppraisalAssignment','appraisal_reference','appraisal_reference');
    }

    public function appraisalAdditionalAssignments(){
        return $this->hasMany('App\AppraisalAdditionalAssignment','appraisal_reference','appraisal_reference');
    }

    public function appraisalTrainPerformanceGaps(){
        return $this->hasMany('App\AppraisalTrainPerformanceGap','appraisal_reference','appraisal_reference');
    }

    public function appraisalTrainChallenges(){
        return $this->hasMany('App\AppraisalTrainChallenge','appraisal_reference','appraisal_reference');
    }

    public function appraisalWorkplans(){
        return $this->hasMany('App\appraisalWorkplan','appraisal_reference','appraisal_reference');
    }

    public function appraisalCompetenceAssessments(){
        return $this->hasMany('App\AppraisalCompetenceAssessment','appraisal_reference','appraisal_reference');
    }

    public function appraisalCompetenceAssessmentScore(){
        return $this->hasOne('App\AppraisalCompetenceAssessmentScore','appraisal_reference','appraisal_reference');
    }

    public function appraisalCompetenceAssessmentSummary(){
        return $this->hasOne('App\AppraisalCompetenceAssessmentSummary','appraisal_reference','appraisal_reference');
    }

    public function appraisalAdditionalAssignmentScore(){
        return $this->hasOne('App\AppraisalAdditionalAssignmentScore','appraisal_reference','appraisal_reference');
    }

    public function appraisalAssignmentScore(){
        return $this->hasOne('App\AppraisalAssignmentScore','appraisal_reference','appraisal_reference');
    }

    public function appraisalAssignmentSummary(){
        return $this->hasOne('App\AppraisalAssignmentSummary','appraisal_reference','appraisal_reference');
    }

    public function appraisalTrainSummary(){
        return $this->hasOne('App\AppraisalTrainSummary','appraisal_reference','appraisal_reference');
    }



    public function appraisalAppraiserComment(){
        return $this->hasOne('App\AppraisalAppraiserComment','appraisal_reference','appraisal_reference');
    }


    public function appraisalStrengthAndWeekness(){
        return $this->hasOne('App\AppraisalStrengthAndWeekness','appraisal_reference','appraisal_reference');
    }

    public function appraisalAppraiserRecommendation(){
        return $this->hasOne('App\AppraisalAppraiserRecommendation','appraisal_reference','appraisal_reference');
    }

    public function appraisalSupervisorDeclaration(){
        return $this->hasOne('App\AppraisalSupervisorDeclaration','appraisal_reference','appraisal_reference');
    }

    public function appraisalHodComment(){
        return $this->hasOne('App\AppraisalHodComment','appraisal_reference','appraisal_reference');
    }

    public function appraisalAppraiseeRemark(){
        return $this->hasOne('App\AppraisalAppraiseeRemark','appraisal_reference','appraisal_reference');
    }

    public function appraisalDirectorComment(){
        return $this->hasOne('App\AppraisalDirectorComment','appraisal_reference','appraisal_reference');
    }

}
