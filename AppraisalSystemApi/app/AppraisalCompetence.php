<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalCompetence extends Model
{

    public function appraisalCompetenceCategory(){
        return $this->belongsTo('App\AppraisalCompetenceCategory');
    }

}
