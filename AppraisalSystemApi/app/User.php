<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{

    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'first_name','last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * Date properties
     * */
    protected $dates = ['last_password_change_date','last_login_date','access_token_expiry_date'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'username';
    }


    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }



    /*
     * Beginning of relationships
     * */

    public function appraisals(){
        return $this->hasMany('App\Appraisal','owner_id','username');
    }


    public function organization(){
        return $this->belongsTo('App\Organization','org_code','org_code');
    }

    public function roleCode(){
        return $this->belongsTo('App\RoleCode','role_code','role_code');
    }

    public function employeeCategory(){
        return $this->belongsTo('App\EmployeeCategory','category_code','category_code');
    }

    public function department(){
        return $this->belongsTo('App\Department','department_code','department_code');
    }

    public function regionalOffice(){
        return $this->belongsTo('App\RegionalOffice','regional_office_code','regional_office_code');
    }

    public function userAcademicBackgrounds(){
        return $this->hasMany('App\UserAcademicBackground','username','username');
    }

    public function receptionistLetters(){
        return $this->hasMany('App\Letter','receptionist_id','username');
    }

    /*
     * End of relationships
     * */


}
