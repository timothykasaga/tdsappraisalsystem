<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

        $role = $this->roleCode;
        return
        [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'role_name' => $role->role_name
        ];

    }
}
