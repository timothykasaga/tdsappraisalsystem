<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\ActionStatus;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\ConstLetterMovtStatus;
use App\Helpers\EmailHandler;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Letter;
use App\LetterMovement;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LetterMovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $letterCode
     * @return \Illuminate\Http\Response
     */
    public function index($letterCode)
    {

        try{

            $letterMovement = LetterMovement::where('letter_code','=',$letterCode)->first();
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            $resp['data'] = $letterMovement;
            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'letter_code'=>'required|exists:letters,letter_code',
                'from_person'=>'required|min:2',
                'to_person'=>'required|min:2',
                'deadline_for_action'=>'required|date',
                'required_action'=>'required|min:2',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['letter_code','from_person','to_person','deadline_for_action','required_action'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            $letterCode = $request['letter_code'];
            $letter = Letter::where('letter_code','=', $letterCode)->first();

            if($letter == null){
                $resp = ResponseHandler::failureResponse("FAILED TO GET LETTER DETAILS FOR LETTER [".$letterCode."]");
                return json_encode($resp);
            }


            /*
             * Save the details
             * */
            $letterMovement = new LetterMovement();

            $dateNow = Carbon::now();

            $letterMovement->letter_code = $letterCode;
            $letterMovement->from_person = $request['from_person'];
            $letterMovement->from_date = $dateNow;
            $letterMovement->to_person = $request['to_person'];
            $letterMovement->to_date = $dateNow;
            $letterMovement->deadline_for_action = $request['deadline_for_action'];
            $letterMovement->required_action = $request['required_action'];

            /*
             * save the details
             * */
            $letterMovement->save();


            /*
             * As as assigned if its the initial assignment by User from the ED's office
             * */
            if($letter->movement_status == ConstLetterMovtStatus::AT_EXECUTIVE_DIRECTOR){
                $this->markLetterAsInitialAssignment($letter, $request['to_person']);
            }else{
                $this->markLetterAsOtherAssignment($letter, $request['to_person']);
            }


            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED LETTER MOVEMENT FLOW ".json_encode($letterMovement);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LetterMovement  $letterMovement
     * @return \Illuminate\Http\Response
     */
    public function show(LetterMovement $letterMovement)
    {

        $resp = [];

        try{

            $resp['data'] = $letterMovement;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;
            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LetterMovement  $letterMovement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LetterMovement $letterMovement)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'letter_code'=>'required|exists:letters,letter_code',
                'from_person'=>'required|min:2',
                'to_person'=>'required|min:2',
                'deadline_for_action'=>'required|date',
                'required_action'=>'required|min:2',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['letter_code','from_person','to_person','deadline_for_action','required_action'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * update the details
             * */
            $letterMovement->from_person = $request['from_person'];
            $letterMovement->to_person = $request['to_person'];
            $letterMovement->deadline_for_action = $request['deadline_for_action'];
            $letterMovement->required_action = $request['required_action'];
            $letterMovement->save();

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED LETTER MOVEMENT FLOW ".json_encode($letterMovement);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LetterMovement  $letterMovement
     * @return \Illuminate\Http\Response
     */
    public function destroy(LetterMovement $letterMovement)
    {
        //
    }

    private function markLetterAsInitialAssignment(Letter $letter, $personAssignedToInitially) {

        $letter->executive_director_action_date = Carbon::now();
        $letter->executive_director_status = ActionStatus::SUBMITTED;

        $letter->movement_status = ConstLetterMovtStatus::ASSIGNED;
        $letter->employee_assigned = $personAssignedToInitially;
        $letter->employee_submission_date = Carbon::now();
        $letter->save();

        $this->sendEmailTOEmployeeAssigned($personAssignedToInitially);

    }

    private function markLetterAsOtherAssignment(Letter $letter, $personAssignedTo) {

        $letter->employee_assigned = $personAssignedTo;
        $letter->save();

        $this->sendEmailTOEmployeeAssigned($personAssignedTo);

    }

    private function sendEmailTOEmployeeAssigned($username){

        $user = User::where('username','=',$username)->first();

        if($user == null){
            return;
        }

        $email = $user->email;
        $emailMsg = "Hello, ".$user->first_name." there is a letter that has been assigned to you for action. Thank You";
        $emailSubject = Globals::$MSG_LETTER_FOR_ACTION;

        $baseResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $emailSubject,$emailMsg);

    }

}
