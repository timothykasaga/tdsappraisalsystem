<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\User;
use App\UserAcademicBackground;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\UserAcademicBackground as UserAcademicBackgroundResource;

class UserAcademicBackgroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function all(User $user)
    {
        $resp = [];

        try{

            $academicBackgrounds = UserAcademicBackground::where('username','=',$user->username)->get();
            $resp['data'] = $academicBackgrounds;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resp = [];
        try{

            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username',
                'institution'=>'required|min:2',
                'year_of_study'=>'required|min:2',
                'award'=>'required|min:2',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['institution','year_of_study','award'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $academicBackground = new UserAcademicBackground();
            $academicBackground->username = $request['username'];
            $academicBackground->institution = $request['institution'];
            $academicBackground->year_of_study = $request['year_of_study'];
            $academicBackground->award = $request['award'];

            $academicBackground->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "USER ACADEMIC BACKGROUND ".json_encode($academicBackground);
            $trail->username = $request['username'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserAcademicBackground  $userAcademicBackground
     * @return \Illuminate\Http\Response
     */
    public function show(UserAcademicBackground $userAcademicBackground)
    {
        $resp = [];

        try{

            $resp['data'] = new UserAcademicBackgroundResource($userAcademicBackground);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserAcademicBackground  $userAcademicBackground
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAcademicBackground $userAcademicBackground)
    {

        $resp = [];
        try{

            $validator = Validator::make($request->all(), [
                'institution'=>'required|min:2',
                'year_of_study'=>'required|min:2',
                'award'=>'required|min:2',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['institution','year_of_study','award'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF USER ACADEMIC BG DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * update department details
             * */
            $userAcademicBackground->institution = $request['institution'];
            $userAcademicBackground->year_of_study = $request['year_of_study'];
            $userAcademicBackground->award = $request['award'];

            $userAcademicBackground->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new UserAcademicBackgroundResource($userAcademicBackground);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED USER ACADEMIC BG " . json_encode($userAcademicBackground).json_encode($request['institution']);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserAcademicBackground  $userAcademicBackground
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserAcademicBackground $userAcademicBackground)
    {

        try{

            if(!$userAcademicBackground->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE INSTITUTE"));
            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED INSTITUTION " . json_encode($userAcademicBackground);
            $trail->username = Auth::user()->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }
}
