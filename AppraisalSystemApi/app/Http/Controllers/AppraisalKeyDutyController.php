<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalKeyDuty;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalKeyDutyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         * 1. Validate the request
         * 2. Check whether data is being saved owner of the appraisal
         * 3. Get the appraisal ID
         * 4. Loop through the records of the key duties as we save
         * 5. Return the response with appraisal reference
         * */

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'key_duties'=>'required|array',  'key_duties.*.objective_id'=>'required|numeric',
                'key_duties.*.job_assignment'=>'required|min:2', 'key_duties.*.expected_output'=>'required|min:2',
                'key_duties.*.maximum_rating'=>'required', 'key_duties.*.time_frame'=>'required|min:2',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','key_duties','key_duties.*.objective_id','key_duties.*.job_assignment',
                                    'key_duties.*.expected_output','key_duties.*.maximum_rating','key_duties.*.time_frame'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $keyDuties = $request['key_duties'];

            foreach ($keyDuties as $duty){

                $keyDuty = new AppraisalKeyDuty();
                $keyDuty->appraisal_reference = $appraisalRef;
                $keyDuty->appraisal_strategic_objective_id = $duty['objective_id'];
                $keyDuty->job_assignment = $duty['job_assignment'];
                $keyDuty->expected_output = $duty['expected_output'];
                $keyDuty->maximum_rating = $duty['maximum_rating'];
                $keyDuty->time_frame = $duty['time_frame'];

                $keyDuty->save();

            }

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($keyDuties)." KEY DUTIES SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED KEY DUTY INFO ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalKeyDuty  $appraisalKeyDuty
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalKeyDuty $appraisalKeyDuty)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param AppraisalKeyDuty $appraisalKeyDuty
     */
    public function update(Request $request)
    {

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'key_duties'=>'required|array',  'key_duties.*.objective_id'=>'required|numeric',
                'key_duties.*.job_assignment'=>'required|min:2', 'key_duties.*.expected_output'=>'required|min:2',
                'key_duties.*.maximum_rating'=>'required', 'key_duties.*.time_frame'=>'required|min:2',
                'key_duties.*.record_id'=>'sometimes|exists:appraisal_key_duties,id',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','key_duties','key_duties.*.objective_id','key_duties.*.job_assignment',
                                    'key_duties.*.expected_output','key_duties.*.maximum_rating','key_duties.*.time_frame','key_duties.*.record_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the schools as we save the details
             * */
            $keyDuties = $request['key_duties'];

            /*
             * Holds list of IDs that we going to keep for this appraisal, any other IDs we are going to delete them
             * */
            $newRecordIds = [];

            foreach ($keyDuties as $duty){

                $keyDuty = new AppraisalKeyDuty();
                $keyDuty->appraisal_reference = $appraisalRef;
                $keyDuty->appraisal_strategic_objective_id = $duty['objective_id'];
                $keyDuty->job_assignment = $duty['job_assignment'];
                $keyDuty->expected_output = $duty['expected_output'];
                $keyDuty->maximum_rating = $duty['maximum_rating'];
                $keyDuty->time_frame = $duty['time_frame'];


                /*
                 * Check if we doing an update on an existent record or a new record came in during the update
                 * */
                if(!array_key_exists('record_id', $duty)) {
                    $keyDuty->save();
                    $recordId = $keyDuty->id;
                }else{

                    /*
                     * Verify that this record actually belongs to this appraisal, to prevent cases where a guy submits
                     * His appraisal reference but uses it to update records of other guys
                     * */
                    $recordId = $duty['record_id'];
                    $existentKeyDuty = AppraisalKeyDuty::find($recordId);

                    /*
                     * If exists and is right we then do an update
                     * */
                    if($existentKeyDuty != null && $existentKeyDuty->appraisal_reference == $appraisalRef){

                        $existentKeyDuty->appraisal_strategic_objective_id = $duty['objective_id'];
                        $existentKeyDuty->job_assignment = $duty['job_assignment'];
                        $existentKeyDuty->expected_output = $duty['expected_output'];
                        $existentKeyDuty->maximum_rating = $duty['maximum_rating'];
                        $existentKeyDuty->time_frame = $duty['time_frame'];
                        $existentKeyDuty->save();

                    }

                }

                /*
                 * Update the list of new Record IDS
                 * */
                $newRecordIds[] = $recordId;

            }


            /*
             * Now we delete the other guys that have not come in the request.
             * But from my understanding you should always archive or use Laravel's soft delete method
             * */
            AppraisalKeyDuty::whereNotIn('id',$newRecordIds)->where('appraisal_reference','=',$appraisalRef)->delete();


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($keyDuties)." KEY DUTIES SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED KEY DUTY INFO ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalKeyDuty  $appraisalKeyDuty
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalKeyDuty $appraisalKeyDuty)
    {
        //
    }
}
