<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\DeletedOrganization;
use App\Helpers\AppMessages;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\Http\Requests\OrganizationRequest;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Organization as OrganizationResource;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //declare the response object
        $resp = [];

        try{

            //get the organizations
            $organizations = Organization::all();

            //return the organizations using the OrganizationResource helper for things like pagination

            //$data = OrganizationResource::collection(Organization::where('id','=','1')->paginate());

            $resp['data'] = $organizations;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            //log the error
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            //get the failure response
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            //return the response
            return json_encode($resp);

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param OrganizationRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{

            //validate using Laravel's validator class

            $validator = Validator::make($request->all(), [
                'name'=>'required|min:2',
                'email'=>'required|email',
                'created_by'=>'required'
            ]);

            //validation has failed so, we return the validation errors
            if ($validator->fails()) {

                //fields validated
                $validatedFields = ['name','org_code','email','created_by'];

                //get the first validation error
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                //return the validation error
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }



            //everything is fine now



            //I decide to get these fields manually just to sure but you can as well saving using the request object
            $organization = new Organization();
            $organization->name = strtoupper($request['name']);
            $organization->org_code = Security::generateTableIdentifier(Security::TAB_ORG);
            $organization->email = $request['email'];
            $organization->location = $request['location'];
            $organization->contact_person_name = $request['contact_person_name'];
            $organization->contact_person_contact = $request['contact_person_contact'];
            $organization->description = $request['description'];
            $organization->created_by = $request['created_by'];

            //save the organization details
            $organization->save();

            //compose success response
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            //return the success response
            return json_encode($resp);


        }catch (\Exception $exception){

            //log the error
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            //get the failure response
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            //return the response
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {

        //declare the response object
        $resp = [];

        try{

            //in this approach of route-model-binding, the model that matches is injected so we just return it
            $resp['data'] = new OrganizationResource($organization);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            //log the error
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            //get the failure response
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            //return the response
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {

        //declare the response object
        $resp = [];

        try{

            //validate using Laravel's validator class

            $validator = Validator::make($request->all(), [
                'name'=>'required|min:2',
                'org_code'=> 'exists:organizations,org_code',
                'email'=>'required|email',
                'created_by'=>'required',
                'updated_by'=>'required',//add constraint for an existent user
                'executive_director'=>'required',//add constraint for an existent user
            ]);

            //validation has failed so, we return the validation errors
            if ($validator->fails()) {

                //fields validated
                $validatedFields = ['name','org_code','email','created_by','updated_by','executive_director'];

                //get the first validation error
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                //return the validation error
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            //update the organization details
            $organization->name = $request['name'];
            $organization->email = $request['email'];
            $organization->created_by = $request['created_by'];
            $organization->location = $request['location'];
            $organization->contact_person_name = $request['contact_person_name'];
            $organization->contact_person_contact = $request['contact_person_contact'];
            $organization->description = $request['description'];
            $organization->executive_director = $request['executive_director'];

            //save the updated details
            $organization->save();

            //in this approach of route-model-binding, the model that matches is injected so we just return it
            $resp['data'] = new OrganizationResource($organization);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            //log the error
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            //get the failure response
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            //return the response
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Organization $organization)
    {

        try{

            /*
            * validate using Laravel's validator class
            * */
            $validator = Validator::make($request->all(), [
                'deleted_by'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['deleted_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$DELETE;
                $trail->action = "FAILED ORG DELETION DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['deleted_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            /*
             * Check for deleted_by
             * 1. Transfer to deleted_users table and if exists in deleted_users, delete from users
             * 2. Insert into the audit trail
             * */

            $deletedOrg = new DeletedOrganization();
            $deletedOrg->original_id = $organization->id;
            $deletedOrg->name = $organization->name;
            $deletedOrg->org_code = $organization->org_code;
            $deletedOrg->email = $organization->email;
            $deletedOrg->location = $organization->location;
            $deletedOrg->contact_person_name = $organization->contact_person_name;
            $deletedOrg->contact_person_contact = $organization->contact_person_contact;
            $deletedOrg->description = $organization->description;
            $deletedOrg->created_by = $organization->created_by;
            $deletedOrg->created_at = $organization->created_at;
            $deletedOrg->updated_at = Carbon::now();
            $deletedOrg->deleted_by = $request['deleted_by'];
            $deletedOrg->deletion_date = Carbon::now();

            /*
             * Attempt to archive the user record
             * */
            if(!$deletedOrg->save()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON ARCHIVING THE ORGANIZATION BEFORE DELETION"));
            }


            /*
             * Attempt to delete the user
             * */
            if(!$organization->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE ORGANIZATION"));
            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED ORG " . json_encode($organization);
            $trail->username = $request['deleted_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * Return success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

}
