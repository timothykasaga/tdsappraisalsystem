<?php

namespace App\Http\Controllers;

use App\AppraisalStrategicObjective;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Http\Resources\Appraisal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalStrategicObjectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {

        /*
        * declare the response object
        * */
        $resp = [];

        try{

            $user = Auth::user();

            /*
             * Failed to get authenticated user
             * */
            if($user == null){
                $msg = Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER;
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);
            }

            /*
             * get data
             * */
            $orgCode = $user->org_code;
            $objectives = AppraisalStrategicObjective::where('org_code','=',$orgCode)->with('organization')->get();

            /*
             * build success response
             * */
            $resp['data'] = $objectives;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'objective'=>'required|min:2',
                'org_code'=>'required|exists:organizations,org_code',
                'created_by'=>'required|exists:users,username'
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['objective', 'org_code','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $objective = new AppraisalStrategicObjective();
            $objective->org_code = $request['org_code'];
            $objective->objective = $request['objective'];
            $objective->created_by = $request['created_by'];

            /*
             * save the details
             * */
            $objective->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED STRATEGIC OBJECTIVE CATEGORY ".json_encode($objective);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalStrategicObjective  $appraisalStrategicObjective
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalStrategicObjective $appraisalStrategicObjective)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalStrategicObjective  $appraisalStrategicObjective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalStrategicObjective $appraisalStrategicObjective)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'objective'=>'required|min:2',
                'org_code'=>'required|exists:organizations,org_code',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['objective', 'org_code'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }


            $appraisalStrategicObjective->org_code = $request['org_code'];
            $appraisalStrategicObjective->objective = $request['objective'];

            /*
             * save the details
             * */
            $appraisalStrategicObjective->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED STRATEGIC OBJECTIVE CATEGORY ".json_encode($appraisalStrategicObjective);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalStrategicObjective  $appraisalStrategicObjective
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalStrategicObjective $appraisalStrategicObjective)
    {

        try{

            if(!$appraisalStrategicObjective->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE STRATEGIC OBJECTIVE"));
            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED STRATEGIC OBJECTIVE " . json_encode($appraisalStrategicObjective);
            $trail->username = Auth::user()->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }
}
