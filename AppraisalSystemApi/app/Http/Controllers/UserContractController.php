<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\User;
use App\UserContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\UserContract as UserContractResource;

class UserContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function all(User $user)
    {

        $resp = [];

        try{

            $userContracts = UserContract::where('username','=',$user->username)->orderBy('created_at','desc')->get();
            $resp['data'] = $userContracts;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resp = [];
        try{

            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username',
                'start_date'=>'required|date',
                'expiry_date'=>'required|date',
                'created_by'=>'required|exists:users,username'
            ]);

            if ($validator->fails()) {
                $validatedFields = ['institution','contract_reference','start_date','expiry_date','created_by'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);
            }

            $userContract = new UserContract();
            $userContract->username = $request['username'];
            $userContract->contract_reference = $request['contract_reference'];
            $userContract->start_date = $request['start_date'];
            $userContract->expiry_date = $request['expiry_date'];
            $userContract->created_by = $request['created_by'];

            $userContract->save();

            //update user contract details
            $username = $request['username'];
            $startDate = $request['start_date'];
            $expiryDate = $request['expiry_date'];
            UserController::updateUserContractDetails($username, $startDate, $expiryDate);


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED USER CONTRACT ".json_encode($userContract);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserContract  $userContract
     * @return \Illuminate\Http\Response
     */
    public function show(UserContract $userContract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserContract  $userContract
     * @return \Illuminate\Http\Response
     */
    public function edit(UserContract $userContract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserContract  $userContract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserContract $userContract)
    {
        $resp = [];
        try{

            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username',
                'start_date'=>'required|date',
                'expiry_date'=>'required|date',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['username','contract_reference','start_date','expiry_date'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF USER USER CONTRACT DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * update department details
             * */
            $userContract->contract_reference = $request['contract_reference'];
            $userContract->start_date = $request['start_date'];
            $userContract->expiry_date = $request['expiry_date'];
            $userContract->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new UserContractResource($userContract);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED USER CONTRACT " . json_encode($userContract);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserContract  $userContract
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserContract $userContract)
    {
        try{

            if(!$userContract->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE CONTRACT DETAILS"));
            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED USER CONTRACT " . json_encode($userContract);
            $trail->username = Auth::user()->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }
}
