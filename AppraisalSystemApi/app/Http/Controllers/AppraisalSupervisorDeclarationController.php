<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalSupervisorDeclaration;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use App\Http\Resources\Appraisal as AppraisalResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalSupervisorDeclarationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'appraiser_name'=>'required|min:2',
                'appraisee_name'=>'required|min:2',
                'duration'=>'required|min:2',
                'start_date'=>'required|date',
                'end_date'=>'required|date',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','appraiser_name','appraisee_name','duration','start_date','end_date'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->supervisor_id  && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $appraiserComment = new AppraisalSupervisorDeclaration();
            $appraiserComment->appraisal_reference = $appraisalRef;
            $appraiserComment->appraiser_name = $request['appraiser_name'];
            $appraiserComment->appraisee_name = $request['appraisee_name'];
            $appraiserComment->duration = $request['duration'];
            $appraiserComment->start_date = $request['start_date'];
            $appraiserComment->end_date = $request['end_date'];

            $appraiserComment->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "SUPERVISOR DECLARATION SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED SUPERVISOR DECLARATION ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalSupervisorDeclaration  $appraisalSupervisorDeclaration
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalSupervisorDeclaration $appraisalSupervisorDeclaration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalSupervisorDeclaration  $appraisalSupervisorDeclaration
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalSupervisorDeclaration $appraisalSupervisorDeclaration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalSupervisorDeclaration  $appraisalSupervisorDeclaration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalSupervisorDeclaration $appraisalSupervisorDeclaration)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'appraiser_name'=>'required|min:2',
                'appraisee_name'=>'required|min:2',
                'duration'=>'required|min:2',
                'start_date'=>'required|date',
                'end_date'=>'required|date',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','appraiser_name','appraisee_name','duration','start_date','end_date'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF SUPERVISOR DECLARATION DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->supervisor_id  && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * update the details
             * */
            $appraisalSupervisorDeclaration->appraiser_name = $request['appraiser_name'];
            $appraisalSupervisorDeclaration->appraisee_name = $request['appraisee_name'];
            $appraisalSupervisorDeclaration->duration = $request['duration'];
            $appraisalSupervisorDeclaration->start_date = $request['start_date'];
            $appraisalSupervisorDeclaration->end_date = $request['end_date'];
            $appraisalSupervisorDeclaration->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED SUPERVISOR DECLARATION " . json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalSupervisorDeclaration  $appraisalSupervisorDeclaration
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalSupervisorDeclaration $appraisalSupervisorDeclaration)
    {
        //
    }
}
