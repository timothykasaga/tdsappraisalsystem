<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalCompetenceAssessmentScore;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalCompetenceAssessmentScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'total_maximum_rating'=>'required|numeric',
                'total_appraisee_rating'=>'sometimes|numeric', 'total_appraiser_rating'=>'sometimes|numeric',
                'total_agreed_rating'=>'sometimes|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','total_maximum_rating','total_appraisee_rating', 'total_appraiser_rating','total_agreed_rating',];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $assessmentScore = new AppraisalCompetenceAssessmentScore();
            $assessmentScore->appraisal_reference = $appraisalRef;
            $assessmentScore->total_maximum_rating = $request['total_maximum_rating'];
            $assessmentScore->total_appraisee_rating = isset($request['total_appraisee_rating']) ? $request['total_appraisee_rating'] : null;
            $assessmentScore->total_appraiser_rating = isset($request['total_appraiser_rating']) ? $request['total_appraiser_rating'] : null;
            $assessmentScore->total_agreed_rating = isset($request['total_agreed_rating']) ? $request['total_agreed_rating'] : null;

            $assessmentScore->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "COMPETENCE ASSESSMENT SCORES SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED COMPETENCE ASSESSMENT SCORES ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalCompetenceAssessmentScore  $appraisalCompetenceAssessmentScore
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalCompetenceAssessmentScore $appraisalCompetenceAssessmentScore)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalCompetenceAssessmentScore  $assessmentScore
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, AppraisalCompetenceAssessmentScore $assessmentScore)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference'=>'required|exists:appraisals,appraisal_reference',
                'total_maximum_rating'=>'required|numeric',
                'total_appraisee_rating'=>'sometimes|numeric',
                'total_appraiser_rating'=>'sometimes|numeric',
                'total_agreed_rating'=>'sometimes|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','total_maximum_rating','total_appraisee_rating','total_appraiser_rating','total_agreed_rating'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF COMPETENCE ASSESSMENT SCORE DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * update the details
             * */
            $assessmentScore->appraisal_reference = $request['appraisal_reference'];
            $assessmentScore->total_maximum_rating = $request['total_maximum_rating'];

            $assessmentScore->total_appraisee_rating = isset($request['total_appraisee_rating']) ? $request['total_appraisee_rating'] : null;
            $assessmentScore->total_appraiser_rating = isset($request['total_appraiser_rating']) ? $request['total_appraiser_rating'] : null;
            $assessmentScore->total_agreed_rating = isset($request['total_agreed_rating']) ? $request['total_agreed_rating'] : null;

            $assessmentScore->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED COMPETENCE ASSESSMENT SCORE " . json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalCompetenceAssessmentScore  $appraisalCompetenceAssessmentScore
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalCompetenceAssessmentScore $appraisalCompetenceAssessmentScore)
    {
        //
    }
}
