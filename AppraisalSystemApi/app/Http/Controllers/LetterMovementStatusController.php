<?php

namespace App\Http\Controllers;

use App\LetterMovementStatus;
use Illuminate\Http\Request;

class LetterMovementStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LetterMovementStatus  $letterMovementStatus
     * @return \Illuminate\Http\Response
     */
    public function show(LetterMovementStatus $letterMovementStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LetterMovementStatus  $letterMovementStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(LetterMovementStatus $letterMovementStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LetterMovementStatus  $letterMovementStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LetterMovementStatus $letterMovementStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LetterMovementStatus  $letterMovementStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(LetterMovementStatus $letterMovementStatus)
    {
        //
    }
}
