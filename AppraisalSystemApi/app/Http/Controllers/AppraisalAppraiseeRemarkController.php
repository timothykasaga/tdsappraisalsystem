<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalAppraiseeRemark;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use App\Http\Resources\Appraisal as AppraisalResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalAppraiseeRemarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'appraisee_name'=>'required|min:2',
                'agreement_decision'=>'required|min:2',
                'disagreement_reason'=>'sometimes',
                'declaration_name'=>'required|min:2',
                'declaration_initials'=>'required|min:2',
                'declaration_date'=>'required|date'
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference',
                    'appraisee_name',
                    'agreement_decision',
                    'disagreement_reason',
                    'declaration_name',
                    'declaration_initials',
                    'declaration_date',
                ];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id  && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            $appraiseeRemark = new AppraisalAppraiseeRemark();
            $appraiseeRemark->appraisal_reference = $appraisalRef;
            $appraiseeRemark->appraisee_name = $request['appraisee_name'];
            $appraiseeRemark->agreement_decision = $request['agreement_decision'];
            $appraiseeRemark->disagreement_reason = $request['disagreement_reason'];
            $appraiseeRemark->declaration_name = $request['declaration_name'];
            $appraiseeRemark->declaration_initials = $request['declaration_initials'];
            $appraiseeRemark->declaration_date = $request['declaration_date'];

            $appraiseeRemark->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "APPRAISEE REMARK SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED APPRAISEE REMARK ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalAppraiseeRemark  $appraisalAppraiseeRemark
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalAppraiseeRemark $appraisalAppraiseeRemark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalAppraiseeRemark  $appraisalAppraiseeRemark
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalAppraiseeRemark $appraisalAppraiseeRemark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalAppraiseeRemark  $appraisalAppraiseeRemark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalAppraiseeRemark $appraisalAppraiseeRemark)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'appraisee_name'=>'required|min:2',
                'agreement_decision'=>'required|min:2',
                'disagreement_reason'=>'sometimes',
                'declaration_name'=>'required|min:2',
                'declaration_initials'=>'required|min:2',
                'declaration_date'=>'required|date'
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference',
                                    'appraisee_name',
                                    'agreement_decision',
                                    'disagreement_reason',
                                    'declaration_name',
                                    'declaration_initials',
                                    'declaration_date'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF APPRAISEE REMARK DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id  && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * update the details
             * */
            $appraisalAppraiseeRemark->appraisee_name = $request['appraisee_name'];
            $appraisalAppraiseeRemark->agreement_decision = $request['agreement_decision'];
            $appraisalAppraiseeRemark->disagreement_reason = $request['disagreement_reason'];
            $appraisalAppraiseeRemark->declaration_name = $request['declaration_name'];
            $appraisalAppraiseeRemark->declaration_initials = $request['declaration_initials'];
            $appraisalAppraiseeRemark->declaration_date = $request['declaration_date'];
            $appraisalAppraiseeRemark->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED APPRAISEE REMARK " . json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalAppraiseeRemark  $appraisalAppraiseeRemark
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalAppraiseeRemark $appraisalAppraiseeRemark)
    {
        //
    }
}
