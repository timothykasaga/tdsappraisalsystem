<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalWorkplan;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;

use App\Http\Resources\Appraisal as AppraisalResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalWorkplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         * 1. Validate the request
         * 2. Check whether data is being saved owner of the appraisal
         * 3. Get the appraisal ID
         * 4. Loop through the records of the plans as we save
         * 5. Return the response with appraisal reference
         * */

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'plans'=>'required|array',
                'plans.*.job_assignment'=>'required|min:2',
                'plans.*.expected_output'=>'required|min:2',
                'plans.*.maximum_rating'=>'required|numeric',
                'plans.*.time_frame'=>'required|min:2',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','plans',
                    'plans.*.job_assignment',
                    'plans.*.expected_output',
                    'plans.*.maximum_rating',
                    'plans.*.time_frame',
                ];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $plans = $request['plans'];

            foreach ($plans as $plan){

                $appraisalWorkplan = new AppraisalWorkplan();
                $appraisalWorkplan->appraisal_reference = $appraisalRef;
                $appraisalWorkplan->job_assignment = $plan['job_assignment'];
                $appraisalWorkplan->expected_output = $plan['expected_output'];
                $appraisalWorkplan->maximum_rating = $plan['maximum_rating'];
                $appraisalWorkplan->time_frame = $plan['time_frame'];

                $appraisalWorkplan->save();

            }

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($plans)." WORK PLAN ITEM(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED WORK PLAN ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalWorkplan  $appraisalWorkplan
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalWorkplan $appraisalWorkplan)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param AppraisalWorkplan $appraisalWorkplan
     */
    public function update(Request $request)
    {

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'plans'=>'required|array',
                'plans.*.job_assignment'=>'required|min:2',
                'plans.*.expected_output'=>'required|min:2',
                'plans.*.maximum_rating'=>'required|numeric',
                'plans.*.time_frame'=>'required|min:2',
                'plans.*.record_id'=>'sometimes|exists:appraisal_workplans,id',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference',
                    'plans',
                    'plans.*.job_assignment',
                    'plans.*.expected_output',
                    'plans.*.maximum_rating',
                    'plans.*.time_frame',
                    'plans.*.record_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as we save the details
             * */
            $plans = $request['plans'];

            /*
             * Holds list of IDs that we going to keep for this appraisal, any other IDs we are going to delete them
             * */
            $newRecordIds = [];

            foreach ($plans as $plan){

                $appraisalWorkplan = new AppraisalWorkplan();
                $appraisalWorkplan->appraisal_reference = $appraisalRef;
                $appraisalWorkplan->job_assignment = $plan['job_assignment'];
                $appraisalWorkplan->expected_output = $plan['expected_output'];
                $appraisalWorkplan->maximum_rating = $plan['maximum_rating'];
                $appraisalWorkplan->time_frame = $plan['time_frame'];

                /*
                 * Check if we doing an update on an existent record or a new record came in during the update
                 * */
                if(!array_key_exists('record_id', $plan)) {
                    $appraisalWorkplan->save();
                    $recordId = $appraisalWorkplan->id;
                }else{

                    /*
                     * Verify that this record actually belongs to this appraisal, to prevent cases where a guy submits
                     * His appraisal reference but uses it to update records of other guys
                     * */
                    $recordId = $plan['record_id'];
                    $existentWorkplan = AppraisalWorkplan::find($recordId);

                    /*
                     * If exists and is right we then do an update
                     * */
                    if($existentWorkplan != null && $existentWorkplan->appraisal_reference == $appraisalRef){

                        $existentWorkplan->job_assignment = $plan['job_assignment'];
                        $existentWorkplan->expected_output = $plan['expected_output'];
                        $existentWorkplan->maximum_rating = $plan['maximum_rating'];
                        $existentWorkplan->time_frame = $plan['time_frame'];
                        $existentWorkplan->save();

                    }

                }

                /*
                 * Update the list of new Record IDS
                 * */
                $newRecordIds[] = $recordId;

            }


            /*
             * Now we delete the other guys that have not come in the request.
             * But from my understanding you should always archive or use Laravel's soft delete method
             * */
            AppraisalWorkplan::whereNotIn('id',$newRecordIds)->where('appraisal_reference','=',$appraisalRef)->delete();


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($plans)." WORK PLAN ITEM(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED WORK PLAN ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalWorkplan  $appraisalWorkplan
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalWorkplan $appraisalWorkplan)
    {
        //
    }
}
