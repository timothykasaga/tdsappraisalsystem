<?php

namespace App\Http\Controllers;

use App\LetterDetail;
use Illuminate\Http\Request;

class LetterDetailController extends Controller
{
    public static function saveLetterDetails(LetterDetail $details) {
        return $details->save();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LetterDetail  $letterDetail
     * @return \Illuminate\Http\Response
     */
    public function show(LetterDetail $letterDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LetterDetail  $letterDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(LetterDetail $letterDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LetterDetail  $letterDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LetterDetail $letterDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LetterDetail  $letterDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(LetterDetail $letterDetail)
    {
        //
    }
}
