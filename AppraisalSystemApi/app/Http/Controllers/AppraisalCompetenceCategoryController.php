<?php

namespace App\Http\Controllers;

use App\AppraisalCompetenceCategory;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalCompetenceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $appraisalReference
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function all($appraisalReference)
    {

        /*
          * declare the response object
          * */

        $resp = [];

        try{

            /*
             * Check if we are requesting for data for a new appraisal or an existing appraisal
             * */
            if($appraisalReference == 'NEW'){

                $user = Auth::user();

            }else{

                $appraisal = AppraisalController::findAppraisalByAppraisalReference($appraisalReference, false);

                /*
                 * We failed to get the appraisal
                 * */
                if(!isset($appraisal)){
                    $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL);
                    return json_encode($resp);
                }

                $user = $appraisal->user;

            }


            /*
             * Failed to get authenticated user
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            $competenceCategories = $this->getCategorizedCompetencesForAUser($user, true);


            /*
             * build success response
             * */
            $resp['data'] = $competenceCategories;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'competence_category'=>'required|min:2',
                'employee_category_code'=>'required|exists:employee_categories,category_code',
                'org_code'=>'required|exists:organizations,org_code',
                'created_by'=>'required|exists:users,username',
                'max_rating'=>'required'
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['competence_category','employee_category_code','org_code','created_by','max_rating'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $competenceCategory = new AppraisalCompetenceCategory();
            $competenceCategory->org_code = $request['org_code'];
            $competenceCategory->competence_category = $request['competence_category'];
            $competenceCategory->employee_category_code = $request['employee_category_code'];
            $competenceCategory->org_code = $request['org_code'];
            $competenceCategory->created_by = $request['created_by'];
            $competenceCategory->max_rating = $request['max_rating'];

            /*
             * save the details
             * */
            $competenceCategory->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED COMPETENCE CATEGORY ".json_encode($competenceCategory);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalCompetenceCategory  $appraisalCompetenceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalCompetenceCategory $appraisalCompetenceCategory)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalCompetenceCategory  $appraisalCompetenceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalCompetenceCategory $appraisalCompetenceCategory)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'competence_category'=>'required|min:2',
                'employee_category_code'=>'required|exists:employee_categories,category_code',
                'org_code'=>'required|exists:organizations,org_code',
                'max_rating'=>'required'
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {
                $validatedFields = ['competence_category','employee_category_code','org_code','created_by','max_rating'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);
            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $appraisalCompetenceCategory->org_code = $request['org_code'];
            $appraisalCompetenceCategory->competence_category = $request['competence_category'];
            $appraisalCompetenceCategory->employee_category_code = $request['employee_category_code'];
            $appraisalCompetenceCategory->org_code = $request['org_code'];
            $appraisalCompetenceCategory->max_rating = $request['max_rating'];

            $appraisalCompetenceCategory->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED COMPETENCE CATEGORY ".json_encode($appraisalCompetenceCategory);
            $trail->username = Auth::user()->username;
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalCompetenceCategory  $appraisalCompetenceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalCompetenceCategory $appraisalCompetenceCategory)
    {
        try{

            if(!$appraisalCompetenceCategory->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE COMPETENCE CATEGORY"));
            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED COMPETENCE CATEGORY " . json_encode($appraisalCompetenceCategory);
            $trail->username = Auth::user()->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }


    public function adminAll()
    {

        /*
      * declare the response object
      * */
        $resp = [];

        try{

            $user = Auth::user();

            /*
             * Failed to get authenticated user
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            $competenceCategories = AppraisalCompetenceCategory::all();

            /*
             * build success response
             * */
            $resp['data'] = $competenceCategories;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }

    /**
     * @param User $user
     * @param bool $withRelations
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getCategorizedCompetencesForAUser(User $user, $withRelations = false) {

        $relations = $withRelations ? ['appraisalCompetences'] : [];
        /*
         * get data
         * */
        $orgCode = $user->org_code;
        $employeeCategoryCode = $user->category_code;

        $competenceCategories = AppraisalCompetenceCategory::
        with($relations)->
        where('org_code', '=', $orgCode)->
        where('employee_category_code', '=', $employeeCategoryCode)->
        get();

        return $competenceCategories;

    }


}
