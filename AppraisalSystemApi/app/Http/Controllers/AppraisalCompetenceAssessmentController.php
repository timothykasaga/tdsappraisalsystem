<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalCompetenceAssessment;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalCompetenceAssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         * 1. Validate the request
         * 2. Check whether data is being saved owner of the appraisal
         * 3. Get the appraisal ID
         * 4. Loop through the records of the assessments as we save
         * 5. Return the response with appraisal reference
         * */

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'assessments'=>'required|array',
                'assessments.*.competence_category_id'=>'required', 'assignments.*.appraisal_competence_id'=>'exists:appraisal_competences,id',
                'assessments.*.maximum_rating'=>'sometimes|numeric', 'assessments.*.appraisee_rating'=>'required|sometimes',
                'assessments.*.appraiser_rating'=>'sometimes|numeric', 'assessments.*.agreed_rating'=>'required|sometimes',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','assessments',
                    'assessments.*.competence_category_id', 'assessments.*.appraisal_competence_id',
                    'assessments.*.maximum_rating', 'assessments.*.appraisee_rating',
                    'assessments.*.appraiser_rating', 'assessments.*.agreed_rating',
                ];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $assessments = $request['assessments'];

            foreach ($assessments as $assessment){

                $competenceAssessment = new AppraisalCompetenceAssessment();
                $competenceAssessment->appraisal_reference = $appraisalRef;
                $competenceAssessment->competence_category_id = $assessment['competence_category_id'];
                $competenceAssessment->appraisal_competence_id = $assessment['appraisal_competence_id'];
                $competenceAssessment->maximum_rating = array_key_exists('maximum_rating', $assessment) ? $assessment['maximum_rating'] : null;
                $competenceAssessment->appraisee_rating = array_key_exists('appraisee_rating', $assessment) ?  $assessment['appraisee_rating'] : null;
                $competenceAssessment->appraiser_rating = array_key_exists('appraiser_rating', $assessment) ? $assessment['appraiser_rating'] : null;
                $competenceAssessment->agreed_rating = array_key_exists('agreed_rating', $assessment) ? $assessment['agreed_rating'] : null;

                $competenceAssessment->save();

            }

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($assessments)." COMPETENCE ASSESSMENT(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED ASSESSMENT COMPETENCES ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalCompetenceAssessment  $appraisalCompetenceAssessment
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalCompetenceAssessment $appraisalCompetenceAssessment)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param AppraisalCompetenceAssessment $appraisalCompetenceAssessment
     */
    public function update(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'assessments'=>'required|array',
                'assessments.*.competence_category_id'=>'required', 'assignments.*.appraisal_competence_id'=>'exists:appraisal_competences,id',
                'assessments.*.maximum_rating'=>'sometimes|numeric', 'assessments.*.appraisee_rating'=>'sometimes|numeric',
                'assessments.*.appraiser_rating'=>'sometimes|numeric', 'assessments.*.agreed_rating'=>'sometimes|numeric',
                'assessments.*.record_id'=>'sometimes|exists:appraisal_competence_assessments,id',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','assessments',
                    'assessments.*.competence_category_id', 'assessments.*.appraisal_competence_id',
                    'assessments.*.maximum_rating', 'assessments.*.appraisee_rating',
                    'assessments.*.appraiser_rating', 'assessments.*.agreed_rating','assessments.*.record_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as we save the details
             * */
            $assessments = $request['assessments'];

            /*
             * Holds list of IDs that we going to keep for this appraisal, any other IDs we are going to delete them
             * */
            $newRecordIds = [];

            foreach ($assessments as $assessment){

                $competenceAssessment = new AppraisalCompetenceAssessment();
                $competenceAssessment->appraisal_reference = $appraisalRef;
                $competenceAssessment->competence_category_id = $assessment['competence_category_id'];
                $competenceAssessment->appraisal_competence_id = $assessment['appraisal_competence_id'];
                $competenceAssessment->maximum_rating = array_key_exists('maximum_rating', $assessment) ? $assessment['maximum_rating'] : null;
                $competenceAssessment->appraisee_rating = array_key_exists('appraisee_rating', $assessment) ? $assessment['appraisee_rating'] : null;
                $competenceAssessment->appraiser_rating = array_key_exists('appraiser_rating', $assessment) ? $assessment['appraiser_rating'] : null;
                $competenceAssessment->agreed_rating = array_key_exists('agreed_rating', $assessment) ? $assessment['agreed_rating'] : null;


                /*
                 * Check if we doing an update on an existent record or a new record came in during the update
                 * */
                if(!array_key_exists('record_id', $assessment)) {
                    $competenceAssessment->save();
                    $recordId = $competenceAssessment->id;
                }else{

                    /*
                     * Verify that this record actually belongs to this appraisal, to prevent cases where a guy submits
                     * His appraisal reference but uses it to update records of other guys
                     * */
                    $recordId = $assessment['record_id'];
                    $existentAssessment = AppraisalCompetenceAssessment::find($recordId);

                    /*
                     * If exists and is right we then do an update
                     * */
                    if($existentAssessment != null && $existentAssessment->appraisal_reference == $appraisalRef){

                        $existentAssessment->competence_category_id = $assessment['competence_category_id'];
                        $existentAssessment->appraisal_competence_id = $assessment['appraisal_competence_id'];

                        $existentAssessment->maximum_rating = array_key_exists('maximum_rating', $assessment) ? $assessment['maximum_rating'] : null;
                        $existentAssessment->appraisee_rating = array_key_exists('appraisee_rating', $assessment) ? $assessment['appraisee_rating'] : null;
                        $existentAssessment->appraiser_rating = array_key_exists('appraiser_rating', $assessment) ? $assessment['appraiser_rating'] : null;
                        $existentAssessment->agreed_rating = array_key_exists('agreed_rating', $assessment) ? $assessment['agreed_rating'] : null;

                        $existentAssessment->save();

                    }

                }

                /*
                 * Update the list of new Record IDS
                 * */
                $newRecordIds[] = $recordId;

            }


            /*
             * Now we delete the other guys that have not come in the request.
             * But from my understanding you should always archive or use Laravel's soft delete method
             * */
            AppraisalCompetenceAssessment::whereNotIn('id',$newRecordIds)->where('appraisal_reference','=',$appraisalRef)->delete();


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($assessments)." COMPETENCE ASSESSMENT(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED COMPETENCE ASSESSMENTS ".json_encode($assessments);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalCompetenceAssessment  $appraisalCompetenceAssessment
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalCompetenceAssessment $appraisalCompetenceAssessment)
    {
        //
    }
}
