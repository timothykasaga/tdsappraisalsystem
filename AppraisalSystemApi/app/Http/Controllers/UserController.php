<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\BaseResponse;
use App\DeletedUser;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\EmailHandler;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    public static function updateUserContractDetails($username, $startDate, $expiryDate) {

        try{

            $user = User::where('username','=',$username)->first();
            if($user == null) return;

            $user->contract_start_date = $startDate;
            $user->contract_expiry_date = $expiryDate;
            $user->save();

        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
        }

    }

    public static function findLetterMovementFlowUsersByLetterMovementRole($letterMovementRole) {

        try{

            $users = User::where('letter_movement_role_code','=',$letterMovementRole)->get();
            return $users;

        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            return [];
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        /*
        * declare the response object
        * */
        $resp = [];

        try{

            /*
             * get the users
             * */
            $users = User::with('department','roleCode','regionalOffice','organization','employeeCategory')->where('role_code','!=','ADMIN')->get();


            /*
             * build success response
             * */
            $resp['data'] = $users;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [

                'first_name'=>'required|min:2',
                'last_name'=>'required|min:2',
                'staff_number'=>'required|min:2',
                'contract_start_date'=>'required|date',
                'contract_expiry_date'=>'required|date',
                'date_of_birth'=>'required|date',
                'designation'=>'required|min:2',
                'email'=>'required|unique:users,email',
                'org_code'=>'required|exists:organizations,org_code',
                'role_code'=>'required|exists:role_codes,role_code',
                'department_code'=>'required|exists:departments,department_code',
                'regional_office_code'=>'required|exists:regional_offices,regional_office_code',
                'category_code'=>'required|exists:employee_categories,category_code',
                'created_by'=>'required',
                'letter_movement_role'=>'required',

            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['first_name','last_name','email','role_code','department_code',
                    'regional_office_code','category_code','org_code','created_by',
                    'staff_number','contract_start_date','contract_expiry_date','date_of_birth','designation','letter_movement_role'
                ];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);


            }



            /*
             * everything is fine now
             * */



            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $user = new User();
            $user->first_name = strtoupper($request['first_name']);
            $user->last_name = strtoupper($request['last_name']);
            $user->other_name = strtoupper($request['other_name']);
            $user->email = $request['email'];
            $user->phone = $request['phone'];


            $user->staff_number = $request['staff_number'];
            $user->contract_start_date = $request['contract_start_date'];
            $user->contract_expiry_date = $request['contract_expiry_date'];
            $user->date_of_birth = $request['date_of_birth'];
            $user->designation = $request['designation'];


            $user->created_by = $request['created_by'];
            $user->created_at = Carbon::now();

            $user->org_code = $request['org_code'];
            $user->role_code = $request['role_code'];
            $user->department_code = $request['department_code'];
            $user->regional_office_code = $request['regional_office_code'];
            $user->category_code = $request['category_code'];
            $user->letter_movement_role_code = $request['letter_movement_role'];



            /*
             * Generate username
             * */
            $username = $user->email;// $this->createUsername($user->first_name,$user->last_name);
            $user->username = $user->email;// $username;


            /*
             * Generate a password
             * */
            $unencryptedPassword = Security::randomPassword();

            $user->password = bcrypt($unencryptedPassword);
            $user->last_password_change_date = Carbon::now();


            /*
             * save the user details
             * */
            $user->save();


            /*
             * Send the email with the user login credentials
             * */


            /*
             * Create an authentication token using the Passport package
             * */
            $passportAuthToken =  $user->createToken('AppraisalSystemApi')-> accessToken;


            /*
             * Send email with the user credentials, this should be done through a job/queue
             * */
            $emailTo = $user->email;
            $name = $user->first_name ." ". $user->last_name;
            $subject = "PPDA APPRAISAL SYSTEM CREDENTIALS";

            $emailMessage = "Hello, ".$user->$name." Your PPDA Appraisal System Credentials Are Username: [".$username."] Password: [".$unencryptedPassword."]".
                " Thank you";

            /*
             * Send Email
             * */
            $emailResp = EmailHandler::sendPlainTextEmailPHPMAILER($emailTo, $subject, $emailMessage);

            /*
             * Check if sent the email, successfully
             * */
            if($emailResp->statusCode != Globals::$STATUS_CODE_SUCCESS){

                $msg = "USER CREATED BUT FAILED TO SEND CREDENTIALS TO EMAIL [".$emailTo."] DUE TO ERROR";
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);

            }


            /*
             * compose success response
             * */
            $resp['credentials'] = "Username[".$username."] Password [".$unencryptedPassword."]";
            $resp['token'] = $passportAuthToken;
            $resp['data'] = new UserResource($user);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED USER ".json_encode($user);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(User $user)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new UserResource($user);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, User $user)
    {

        /*
        * declare the response object
        * */
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [

                'first_name'=>'required|min:2',
                'last_name'=>'required|min:2',
                'username'=>'required|exists:users,username',
                'org_code'=>'required|exists:organizations,org_code',
                'role_code'=>'required|exists:role_codes,role_code',
                'department_code'=>'required|exists:departments,department_code',
                'regional_office_code'=>'required|exists:regional_offices,regional_office_code',
                'category_code'=>'required|exists:employee_categories,category_code',
                'created_by'=>'required',//add constraint for an existent user
                'updated_by'=>'required',//add constraint for an existent user
                'staff_number'=>'required|min:2',
                'date_of_birth'=>'required|date',
                'designation'=>'required|min:2',
                'letter_movement_role'=>'required',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['designation','date_of_birth','staff_number','first_name','last_name','username','org_code','role_code','department_code',
                    'regional_office_code','category_code','created_by','updated_by','letter_movement_role'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF USER DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['updated_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * update user details
             * */
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->org_code = $request['org_code'];
            $user->role_code = $request['role_code'];
            $user->department_code = $request['department_code'];
            $user->regional_office_code = $request['regional_office_code'];
            $user->category_code = $request['category_code'];
            $user->created_by = $request['created_by'];
            $user->letter_movement_role_code = $request['letter_movement_role'];

            $user->staff_number = $request['staff_number'];
            $user->date_of_birth = $request['date_of_birth'];
            $user->designation = $request['designation'];

            /*
             * save the updated details
             * */
            $user->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new UserResource($user);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED USER " . json_encode($user);
            $trail->username = $request['updated_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    public function updateUserByProfileOwner(Request $request, User $user)
    {
        $resp = [];
        try{
            $validator = Validator::make($request->all(), [
                'first_name'=>'required|min:2',
                'last_name'=>'required|min:2',
                'designation'=>'required|min:2',
                'date_of_birth'=>'required|date',
            ]);

            if ($validator->fails()) {
                $validatedFields = ['first_name','last_name','designation','date_of_birth'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF USER DUE TO ERROR ".json_encode($resp);
                $trail->username = $user->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);
            }

            /*
             * update user details
             * */
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->designation = $request['designation'];
            $user->date_of_birth = $request['date_of_birth'];

            $user->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $user = $this->findUserByUserId($user->id);
            $resp['data'] = new UserResource($user);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED USER " . json_encode($user);
            $trail->username = $user->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);

        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request, User $user)
    {

        try{

            /*
            * validate using Laravel's validator class
            * */
            $validator = Validator::make($request->all(), [
                'deleted_by'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['deleted_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$DELETE;
                $trail->action = "FAILED USER DELETION DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['deleted_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            /*
             * Check for deleted_by
             * 1. Transfer to deleted_users table and if exists in deleted_users, delete from users
             * 2. Insert into the audit trail
             * */

            $deletedUser = new DeletedUser();
            $deletedUser->original_id = $user->id;
            $deletedUser->username = $user->username;
            $deletedUser->first_name = $user->first_name;
            $deletedUser->last_name = $user->last_name;
            $deletedUser->other_name = $user->other_name;
            $deletedUser->email = $user->email;
            $deletedUser->password = $user->password;
            $deletedUser->role_code = $user->role_code;
            $deletedUser->category_code = $user->category_code;
            $deletedUser->regional_office_code = $user->regional_office_code;
            $deletedUser->department_code = $user->department_code;
            $deletedUser->org_code = $user->org_code;
            $deletedUser->phone = $user->phone;
            $deletedUser->active = $user->active;
            $deletedUser->reset = $user->reset;
            $deletedUser->logged_on = $user->logged_on;
            $deletedUser->access_token = $user->access_token;
            $deletedUser->access_token_expiry_date = $user->access_token_expiry_date;
            $deletedUser->created_by = $user->created_by;
            $deletedUser->last_login_date = $user->last_login_date;
            $deletedUser->last_password_change_date = $user->last_password_change_date;
            $deletedUser->created_at = $user->created_at;
            $deletedUser->updated_at = Carbon::now();
            $deletedUser->deleted_by = $request['deleted_by'];
            $deletedUser->deletion_date = Carbon::now();


            /*
             * Attempt to archive the user record
             * */
            if(!$deletedUser->save()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON ARCHIVING THE USER BEFORE DELETION"));
            }


            /*
             * Attempt to delete the user
             * */
            if(!$user->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE USER"));
            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED USER " . json_encode($user);
            $trail->username = $request['deleted_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * Return success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    /**
     * login api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){


        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * Attempt to authenticate the user
             * */
            if(!Auth::attempt(['username' => request('username'), 'password' => request('password')])){


                /*
                 * Authentication using username and password has failed
                 * */
                $resp['statusCode'] = Globals::$STATUS_CODE_FAILED;
                $resp['statusDescription'] = "INVALID CREDENTIALS SUPPLIED";


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$LOGIN;
                $trail->action = "USER LOGIN FAILED WITH ERROR ".json_encode($resp);
                $trail->username = request('username');
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            /*
             * Authentication was successful
             * */
            $user = Auth::user();


            /*
             * Generate access token for the user
             * */
            $passportAuthToken =  $user->createToken('AppraisalSystemApi')-> accessToken;


            /*
             * Get top 3 appraisal by user and top 3 appraisals assigned to user
             * */
            $appraisalsOwned = AppraisalController::findAppraisalsByUsername($user->username, 3, true);
            $appraisalsAssigned = AppraisalController::findAppraisalsByUsername($user->username, 3, false);

            /*
             * Create data object
             * */

            $user = $this->findUserByUserId($user->id);

            $data = [];
            $data['profile'] = $user;
            $data['appraisals_owned'] = $appraisalsOwned;
            $data['appraisals_assigned'] = $appraisalsAssigned;

            /*
             * Create success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = $user->reset ? Globals::$REQUIRE_PASSWORD_CHANGE : Globals::$STATUS_DESC_SUCCESS;
            $resp['token'] = $passportAuthToken;
            $resp['data'] = $data;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$LOGIN;
            $trail->action = "USER LOGIN SUCCESSFUL";
            $trail->username = request('username');
            $trail->ip_address = $request->ip();
            $trail->api_username = "";

            LogHandler::logAuditTrail($trail);

            /*
             * End of log audit trail
             * */

            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    /**
     * change password api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request){

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username',
                'old_password'=>'required|min:6',
                'new_password'=>'required|min:6',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = [ 'username','old_password','new_password' ];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$PASSWORD_CHANGE;
                $trail->action = "FAILED PASSWORD CHANGE DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['username'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            $username = $request['username'];
            $oldPassword = $request['old_password'];
            $newPassword = $request['new_password'];

            $user = User::where('username','=',$username)->first();

            /*
             * We failed to get the user by username
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse("USER WITH USERNAME [] NOT FOUND");
                return json_encode($resp);
            }

            /*
             * Get the hashed password
             * */
            $hashedPassword = $user->password;

            /*
             * Match old and new password
             * */
            if (!Hash::check($oldPassword, $hashedPassword)) {

                $resp = ResponseHandler::failureResponse("INVALID OLD PASSWORD SUPPLIED");
                return json_encode($resp);

            }

            /*
             * Update the user password
             * */
            $user->password = bcrypt($newPassword);


            /*
             * Attempt to save user
             * */
            if(!$user->save()){

                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON CHANGING USER PASSWORD");
                return json_encode($resp);

            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$PASSWORD_RESET;
            $trail->action = "PASSWORD RESET SUCCESSFUL";
            $trail->username = request('username');
            $trail->ip_address = $request->ip();
            $trail->api_username = "";

            LogHandler::logAuditTrail($trail);

            /*
             * End of log audit trail
             * */


            /*
             * Return success, Password successfully changed
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }



    public function changeDefaultPassword(Request $request){

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username', 'new_password'=>'required|min:6',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = [ 'username','new_password' ];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$PASSWORD_CHANGE;
                $trail->action = "FAILED CHANGE DEFAULT PASSWORD DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['username'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }


            $username = $request['username'];
            $newPassword = $request['new_password'];

            $user = User::where('username','=',$username)->first();

            /*
             * We failed to get the user by username
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse("USER WITH USERNAME [".$username."] NOT FOUND");
                return json_encode($resp);
            }

            /*
             * Check if logged in user is the one attempting to change their password
             * */
            if(!$this->iamChangingMyOwnPassword($username)){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Update the user password
             * */
            $user->password = bcrypt($newPassword);
            $user->reset = false;
            if(!$user->save()){

                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON CHANGING USER PASSWORD");
                return json_encode($resp);

            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$PASSWORD_RESET;
            $trail->action = "CHANGE DEFAULT PASSWORD SUCCESSFUL";
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            /*
             * Return success, Password successfully changed
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * reset password api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request){

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'username'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = [ 'username' ];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$PASSWORD_RESET;
                $trail->action = "FAILED PASSWORD RESET DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['username'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            $username = $request['username'];

            $user = User::where('username','=',$username)->first();

            /*
             * We failed to get the user by username
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse("USER WITH USERNAME [] NOT FOUND");
                return json_encode($resp);
            }

            /*
             * Generate new password
             * */
            $unecryptedPassword = Security::randomPassword(6);

            /*
             * Assign new user details
             * */
            $user->password = bcrypt($unecryptedPassword);
            $user->last_password_change_date = Carbon::now();
            $user->reset = true;

            /*
             * Attempt to save user
             * */
            if(!$user->save()){

                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON RESETTING USER PASSWORD");
                return json_encode($resp);

            }

            /*
             * Send password reset email
             * */
            $email = $user->email;
            $firstName = $user->first_name;
            $message = "Hello ".$firstName." your password has been reset, your new credentials are Username: [".$username."] Password: [".$unecryptedPassword."]. Thank You";
            $emailResp = EmailHandler::sendPlainTextEmailPHPMAILER($email,"PASSWORD RESET CREDENTIALS",$message);

            /*
             * Error occurred on sending password
             * */
            if($emailResp->statusCode != Globals::$STATUS_CODE_SUCCESS){

                $resp = ResponseHandler::failureResponse("PASSWORD RESET BUT EMAIL NOT SENT DUE TO ".$emailResp->statusDescription);
                return json_encode($resp);

            }


            /*
             * Password successfully reset and email sent
             * */


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$PASSWORD_RESET;
            $trail->action = "PASSWORD RESET SUCCESSFUL";
            $trail->username = request('username');
            $trail->ip_address = $request->ip();
            $trail->api_username = "";

            LogHandler::logAuditTrail($trail);

            /*
             * End of log audit trail
             * */


            /*
             * Return success
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }




    private function createUsername($firstName, $lastName) {

        $firstNameArr = str_split(strtolower($firstName));
        $firstNameInitial = $firstNameArr[0];

        $temporaryUsername = $firstNameInitial . strtolower($lastName);
        $username = $this->nonExistentUsername($temporaryUsername);

        return strtolower($username);

    }

    public function nonExistentUsername($username) {

        $user = User::where('username', '=', $username)->first();
        return $user == null ? $username : $this->nonExistentUsername($username.random_int(0,99));

    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * @param $userId
     * @internal param $user
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null|static|static[]
     */
    private function findUserByUserId($userId) {

        $modelRelations = ['organization', 'department', 'roleCode', 'employeeCategory', 'regionalOffice'];
        $user = User::with($modelRelations)->find($userId);
        return $user;

    }

    private function iamChangingMyOwnPassword($username) {

        return $username == Auth::user()->username;

    }

    public function authenticatedUser(){

        try{

            /*
             * Get the authenticated user
             * */
            $user = Auth::user();

            if(!isset($user)){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            $user =  self::findUserByUserId($user->id);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_CODE_SUCCESS);
            $resp['data'] = $user;
            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception, __CLASS__, __METHOD__);
            $error = Globals::generalError($exception);
            $resp = ResponseHandler::failureResponse($error);

            return json_encode($resp);

        }

    }

}
