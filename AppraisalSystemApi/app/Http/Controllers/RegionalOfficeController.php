<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\RegionalOffice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\RegionalOffice as RegionalOfficeResource;

class RegionalOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * get the regional offices
             * */
            $regionalOffices = RegionalOffice::all();


            /*
             * build success response
             * */
            $resp['data'] = $regionalOffices;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'name'=>'required|min:2',
                'org_code'=>'required|exists:organizations,org_code',
                'email'=>'required|email',
                'created_by'=>'required'
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['name','regional_office_code','org_code','email','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }



            /*
             * everything is fine now
             * */



            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $regionalOffice = new RegionalOffice();
            $regionalOffice->name = strtoupper($request['name']);
            $regionalOffice->regional_office_code = Security::generateTableIdentifier(Security::TAB_OFFICE);
            $regionalOffice->org_code = $request['org_code'];
            $regionalOffice->email = $request['email'];
            $regionalOffice->location = $request['location'];
            $regionalOffice->contact_person_name = strtoupper($request['contact_person_name']);
            $regionalOffice->contact_person_contact = $request['contact_person_contact'];
            $regionalOffice->description = $request['description'];
            $regionalOffice->created_by = $request['created_by'];

            /*
             * save the regional office details
             * */
            $regionalOffice->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED REGIONAL OFFICE ".json_encode($regionalOffice);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegionalOffice  $regionalOffice
     * @return \Illuminate\Http\Response
     */
    public function show(RegionalOffice $regionalOffice)
    {
        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new RegionalOfficeResource($regionalOffice);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegionalOffice  $regionalOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegionalOffice $regionalOffice)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'name'=>'required|min:2',
                'regional_office_code'=> 'required|exists:regional_offices,regional_office_code',
                'org_code'=> 'exists:organizations,org_code',
                'email'=>'required|email',
                'created_by'=>'required',//add constraint for an existent user
                'updated_by'=>'required',//add constraint for an existent user
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['name','regional_office_code','org_code','email','created_by','updated_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF REGIONAL OFFICE DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['updated_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * update the regional office details
             * */
            $regionalOffice->name = $request['name'];
            $regionalOffice->org_code = $request['org_code'];
            $regionalOffice->email = $request['email'];
            $regionalOffice->created_by = $request['created_by'];
            $regionalOffice->location = $request['location'];
            $regionalOffice->contact_person_name = $request['contact_person_name'];
            $regionalOffice->contact_person_contact = $request['contact_person_contact'];
            $regionalOffice->description = $request['description'];

            /*
             * save the updated details
             * */
            $regionalOffice->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new RegionalOfficeResource($regionalOffice);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED REGIONAL OFFICE " . json_encode($regionalOffice);
            $trail->username = $request['updated_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\RegionalOffice $regionalOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RegionalOffice $regionalOffice)
    {

        try{

            /*
            * validate using Laravel's validator class
            * */
            $validator = Validator::make($request->all(), [
                'deleted_by'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['deleted_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$DELETE;
                $trail->action = "FAILED REGIONAL OFFICE DELETION DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['deleted_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * Attempt to delete
             * */
            if(!$regionalOffice->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING REGIONAL OFFICE"));
            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED REGIONAL OFFICE " . json_encode($regionalOffice);
            $trail->username = $request['deleted_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * Return success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }
}
