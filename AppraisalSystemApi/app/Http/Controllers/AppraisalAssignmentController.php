<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalAssignment;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         * 1. Validate the request
         * 2. Check whether data is being saved owner of the appraisal
         * 3. Get the appraisal ID
         * 4. Loop through the records of the assignments as we save
         * 5. Return the response with appraisal reference
         * */

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'assignments'=>'required|array',  'assignments.*.objective_id'=>'required|numeric',
                'assignments.*.expected_output'=>'required|min:2', 'assignments.*.actual_performance'=>'required|min:2',
                'assignments.*.maximum_rating'=>'sometimes|numeric', 'assignments.*.appraisee_rating'=>'sometimes|numeric',
                'assignments.*.appraiser_rating'=>'sometimes|numeric', 'assignments.*.agreed_rating'=>'sometimes|numeric',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','assignments','assignments.*.objective_id','assignments.*.expected_output',
                    'assignments.*.actual_performance','assignments.*.maximum_rating','assignments.*.appraisee_rating',
                    'assignments.*.appraiser_rating','assignments.*.agreed_rating'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $assignments = $request['assignments'];

            foreach ($assignments as $assignment){

                $appraisalAssignment = new AppraisalAssignment();
                $appraisalAssignment->appraisal_reference = $appraisalRef;
                $appraisalAssignment->appraisal_strategic_objective_id = $assignment['objective_id'];
                $appraisalAssignment->expected_output = $assignment['expected_output'];
                $appraisalAssignment->actual_performance = $assignment['actual_performance'];
                $appraisalAssignment->maximum_rating = $assignment['maximum_rating'];

                /*
                 * You can save this section without these fields and then fill them after, e.g an appraisee may fill in
                 * their section and then leave the supervisor fields empty
                 * */
                if(array_key_exists('appraisee_rating',$assignment)){
                    $appraisalAssignment->appraisee_rating =  $assignment['appraisee_rating'];
                }
                if(array_key_exists('appraiser_rating',$assignment)){
                    $appraisalAssignment->appraiser_rating = $assignment['appraiser_rating'];
                }
                if(array_key_exists('agreed_rating',$assignment)){
                    $appraisalAssignment->agreed_rating = $assignment['agreed_rating'];
                }

                $appraisalAssignment->save();

            }

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($assignments)." ASSIGNMENT(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED ASSIGNMENTS ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalAssignment  $appraisalAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalAssignment $appraisalAssignment)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param AppraisalAssignment $appraisalAssignment
     */
    public function update(Request $request)
    {

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'assignments'=>'required|array',  'assignments.*.objective_id'=>'required|numeric',
                'assignments.*.expected_output'=>'required|min:2', 'assignments.*.actual_performance'=>'required|min:2',
                'assignments.*.maximum_rating'=>'required|numeric', 'assignments.*.appraisee_rating'=>'sometimes|numeric',
                'assignments.*.appraiser_rating'=>'sometimes|numeric', 'assignments.*.agreed_rating'=>'sometimes|numeric',
                'assignments.*.record_id'=>'sometimes|exists:appraisal_assignments,id',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','assignments','assignments.*.objective_id','assignments.*.expected_output',
                    'assignments.*.actual_performance','assignments.*.maximum_rating','assignments.*.appraisee_rating',
                    'assignments.*.appraiser_rating','assignments.*.agreed_rating','assignments.*.record_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as we save the details
             * */
            $assignments = $request['assignments'];

            /*
             * Holds list of IDs that we going to keep for this appraisal, any other IDs we are going to delete them
             * */
            $newRecordIds = [];

            foreach ($assignments as $assignment){

                $appraisalAssignment = new AppraisalAssignment();
                $appraisalAssignment->appraisal_reference = $appraisalRef;
                $appraisalAssignment->appraisal_strategic_objective_id = $assignment['objective_id'];
                $appraisalAssignment->expected_output = $assignment['expected_output'];
                $appraisalAssignment->actual_performance = $assignment['actual_performance'];
                $appraisalAssignment->maximum_rating = $assignment['maximum_rating'];


                /*
                 * The fields below can be empty, say the appraisal is still at the stage of the appraiser
                 * */
                if(array_key_exists('appraisee_rating', $assignment)){
                    $appraisalAssignment->appraisee_rating = $assignment['appraisee_rating'];
                }
                if(array_key_exists('appraiser_rating', $assignment)){
                    $appraisalAssignment->appraiser_rating = $assignment['appraiser_rating'];
                }
                if(array_key_exists('agreed_rating', $assignment)){
                    $appraisalAssignment->agreed_rating = $assignment['agreed_rating'];
                }

                /*
                 * Check if we doing an update on an existent record or a new record came in during the update
                 * */
                if(!array_key_exists('record_id', $assignment)) {
                    $appraisalAssignment->save();
                    $recordId = $appraisalAssignment->id;
                }else{

                    /*
                     * Verify that this record actually belongs to this appraisal, to prevent cases where a guy submits
                     * His appraisal reference but uses it to update records of other guys
                     * */
                    $recordId = $assignment['record_id'];
                    $existentAssignment = AppraisalAssignment::find($recordId);

                    /*
                     * If exists and is right we then do an update
                     * */
                    if($existentAssignment != null && $existentAssignment->appraisal_reference == $appraisalRef){

                        $existentAssignment->appraisal_strategic_objective_id = $assignment['objective_id'];
                        $existentAssignment->expected_output = $assignment['expected_output'];
                        $existentAssignment->actual_performance = $assignment['actual_performance'];
                        $existentAssignment->maximum_rating = $assignment['maximum_rating'];
                        $existentAssignment->appraisee_rating = array_key_exists('appraisee_rating',$assignment) ? $assignment['appraisee_rating'] : null;
                        $existentAssignment->appraiser_rating = array_key_exists('appraiser_rating',$assignment) ? $assignment['appraiser_rating'] : null;
                        $existentAssignment->agreed_rating = array_key_exists('agreed_rating',$assignment) ? $assignment['agreed_rating'] : null;

                        $existentAssignment->save();

                    }

                }

                /*
                 * Update the list of new Record IDS
                 * */
                $newRecordIds[] = $recordId;

            }


            /*
             * Now we delete the other guys that have not come in the request.
             * But from my understanding you should always archive or use Laravel's soft delete method
             * */
            AppraisalAssignment::whereNotIn('id',$newRecordIds)->where('appraisal_reference','=',$appraisalRef)->delete();


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($assignments)." ASSIGNMENT(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED ASSIGNMENTS ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalAssignment  $appraisalAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalAssignment $appraisalAssignment)
    {
        //
    }
}
