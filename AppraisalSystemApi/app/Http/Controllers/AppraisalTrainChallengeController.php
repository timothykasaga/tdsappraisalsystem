<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalTrainChallenge;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;

use App\Http\Resources\Appraisal as AppraisalResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalTrainChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         * 1. Validate the request
         * 2. Check whether data is being saved owner of the appraisal
         * 3. Get the appraisal ID
         * 4. Loop through the records of the assignments as we save
         * 5. Return the response with appraisal reference
         * */

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'challenges'=>'required|array',  'challenges.*.challenge'=>'required|min:2',
                'challenges.*.causes'=>'required|min:2', 'challenges.*.recommendation'=>'required|min:2',
                'challenges.*.when'=>'required'
            ]);

            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','challenges',
                    'challenges.*.challenge',
                    'challenges.*.causes',
                    'challenges.*.recommendation',
                    'challenges.*.when'
                ];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $challenges = $request['challenges'];

            foreach ($challenges as $challenge){

                $trainChallenge = new AppraisalTrainChallenge();
                $trainChallenge->appraisal_reference = $appraisalRef;
                $trainChallenge->challenge = $challenge['challenge'];
                $trainChallenge->causes = $challenge['causes'];
                $trainChallenge->recommendation = $challenge['recommendation'];
                $trainChallenge->when = $challenge['when'];

                $trainChallenge->save();

            }

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($challenges)." CHALLENGE(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVE CHALLENGES ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalTrainChallenge  $appraisalTrainChallenge
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalTrainChallenge $appraisalTrainChallenge)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param AppraisalTrainChallenge $appraisalTrainChallenge
     */
    public function update(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'challenges'=>'required|array',  'challenges.*.challenge'=>'required|min:2',
                'challenges.*.causes'=>'required|min:2', 'challenges.*.recommendation'=>'required|min:2',
                'challenges.*.when'=>'required',
                'challenges.*.record_id'=>'sometimes|exists:appraisal_train_challenges,id',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','challenges',
                    'challenges.*.challenge',
                    'challenges.*.causes',
                    'challenges.*.recommendation',
                    'challenges.*.when',
                    'challenges.*.record_id'
                ];

                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as we save the details
             * */
            $challenges = $request['challenges'];

            /*
             * Holds list of IDs that we going to keep for this appraisal, any other IDs we are going to delete them
             * */
            $newRecordIds = [];

            foreach ($challenges as $challenge){

                $trainChallenge = new AppraisalTrainChallenge();
                $trainChallenge->appraisal_reference = $appraisalRef;
                $trainChallenge->challenge = $challenge['challenge'];
                $trainChallenge->causes = $challenge['causes'];
                $trainChallenge->recommendation = $challenge['recommendation'];
                $trainChallenge->when = $challenge['when'];

                /*
                 * Check if we doing an update on an existent record or a new record came in during the update
                 * */
                if(!array_key_exists('record_id', $challenge)) {
                    $trainChallenge->save();
                    $recordId = $trainChallenge->id;
                }else{

                    /*
                     * Verify that this record actually belongs to this appraisal, to prevent cases where a guy submits
                     * His appraisal reference but uses it to update records of other guys
                     * */
                    $recordId = $challenge['record_id'];
                    $existentChallenge = AppraisalTrainChallenge::find($recordId);

                    /*
                     * If exists and is right we then do an update
                     * */
                    if($existentChallenge != null && $existentChallenge->appraisal_reference == $appraisalRef){

                        $existentChallenge->performance_gap = $challenge['challenge'];
                        $existentChallenge->causes = $challenge['causes'];
                        $existentChallenge->recommendation = $challenge['recommendation'];
                        $existentChallenge->when = $challenge['when'];

                        $existentChallenge->save();

                    }

                }

                /*
                 * Update the list of new Record IDS
                 * */
                $newRecordIds[] = $recordId;

            }


            /*
             * Now we delete the other guys that have not come in the request.
             * But from my understanding you should always archive or use Laravel's soft delete method
             * */
            AppraisalTrainChallenge::whereNotIn('id',$newRecordIds)->where('appraisal_reference','=',$appraisalRef)->delete();


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "[".count($challenges)." CHALLENGE(S) SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED CHALLENGES ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalTrainChallenge  $appraisalTrainChallenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalTrainChallenge $appraisalTrainChallenge)
    {
        //
    }
}
