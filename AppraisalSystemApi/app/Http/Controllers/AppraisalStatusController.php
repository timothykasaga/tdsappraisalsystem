<?php

namespace App\Http\Controllers;

use App\AppraisalStatus;
use Illuminate\Http\Request;

class AppraisalStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalStatus  $appraisalStatus
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalStatus $appraisalStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalStatus  $appraisalStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalStatus $appraisalStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalStatus  $appraisalStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalStatus $appraisalStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalStatus  $appraisalStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalStatus $appraisalStatus)
    {
        //
    }
}
