<?php

namespace App\Http\Controllers;

use App\ApplicationStat;
use App\Department;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\RegionalOffice;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApplicationStatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        /*
        * declare the response object
        * */
        $resp = [];

        try{

            /*
             * get the Application Stats
             * */

            /*
             * Appraisals
             * */
            $countAppraisals  = "0";
            $countAppraisalsNew  = "0";

            /*
             * Users
             * */
            $countUsers  = User::count();
            $countUsersNew  = User::whereDate('created_at','>=', Carbon::now()->subDays(Globals::$APP_NEWEST_DAYS))->count();

            /*
             * Regional Offices
             * */
            $countRegionalOffice  = RegionalOffice::count();
            $countRegionalOfficeNew  = RegionalOffice::whereDate('created_at','>=', Carbon::now()->subDays(Globals::$APP_NEWEST_DAYS))->count();;

            /*
             * Departments
             * */
            $countDepartments  = Department::count();
            $countDepartmentsNew  = Department::whereDate('created_at','>=', Carbon::now()->subDays(Globals::$APP_NEWEST_DAYS))->count();;

            $appStats = new ApplicationStat();
            $appStats->countAppraisals = $countAppraisals;
            $appStats->countAppraisalsNew = $countAppraisalsNew;

            $appStats->countUsers = $countUsers;
            $appStats->countUsersNew = $countUsersNew;

            $appStats->countDepartments = $countDepartments;
            $appStats->countDepartmentsNew = $countDepartmentsNew;

            $appStats->countRegionalOffice = $countRegionalOffice;
            $appStats->countRegionalOfficeNew = $countRegionalOfficeNew;

            /*
             * build success response
             * */
            $resp['data'] = $appStats;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

}
