<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalPersonalDetail;
use App\AuditTrail;
use App\BaseResponse;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalPersonalDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalPersonalDetail  $appraisalPersonalDetail
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalPersonalDetail $appraisalPersonalDetail)
    {
        //
    }

     public function update(Request $request, AppraisalPersonalDetail $appraisalPersonalDetail)
     {

         $resp = [];

         try{

             $validator = Validator::make($request->all(), [
                 'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                 'first_name'=>'required|min:2',
                 'last_name'=>'required|min:2',
                 'department'=>'required|min:2',
                 'designation'=>'required|min:2',
                 'date_of_birth'=>'required|min:2',
                 'appraisal_period_start_date'=>'required|date',
                 'appraisal_period_end_date'=>'required|date',
                 'contract_start_date'=>'required|min:2',
                 'contract_expiry_date'=>'required|min:2',
                 'staff_number'=>'required|min:2',
                 'employee_category'=>'required|min:2',
                 'appraisal_type'=>'required|min:2',
                 'supervisor_id'=>'required|min:2',
                 'department_head_id'=>'required|min:2',
                 'executive_director_id'=>'required|min:2',
             ]);


             if ($validator->fails()) {

                 $validatedFields = ['appraisal_reference','first_name','last_name','department','date_of_birth','employee_category',
                 'appraisal_period_start_date','appraisal_period_end_date','contract_start_date','contract_expiry_date','staff_number',
                 'appraisal_type','supervisor_id','department_head_id','executive_director_id'];
                 $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                 $resp = ResponseHandler::failureResponse($firstValidationError);

                 /*
                  * Log audit trail
                  * */
                 $trail = new AuditTrail();
                 $trail->action_category = AuditTrailActionCategories::$UPDATE;
                 $trail->action = "FAILED UPDATE OF PERSONAL INFO DUE TO ERROR ".json_encode($resp);
                 $trail->username = Auth::user()->username;
                 $trail->ip_address = $request->ip();
                 $trail->api_username = "";
                 LogHandler::logAuditTrail($trail);

                 return json_encode($resp);

             }

             /*
              * Get the appraisal
              * */
             $appraisalRef = $request['appraisal_reference'];
             $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


             /*
              * Error occurred on getting the appraisal using the reference
              * */
             if($appraisal == null){
                 $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                 return json_encode($resp);
             }

             /*
              * Check if the request is coming from the owner of the application
              * */
             if(Auth::user()->username != $appraisal->owner_id){
                 $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                 return json_encode($resp);
             }

             /*
              * Update the appraisal details
              * */
             $appraisal->appraisal_type = $request['appraisal_type'];
             $appraisal->supervisor_id = $request['supervisor_id'];
             $appraisal->department_head_id = $request['department_head_id'];
             $appraisal->executive_director_id = $request['executive_director_id'];
             $appraisal->save();

             /*
              * update the details
              * */
             $appraisalPersonalDetail->first_name = $request['first_name'];
             $appraisalPersonalDetail->last_name = $request['last_name'];
             $appraisalPersonalDetail->other_name = $request['other_name'];
             $appraisalPersonalDetail->department = $request['department'];
             $appraisalPersonalDetail->designation = $request['designation'];
             $appraisalPersonalDetail->date_of_birth = $request['date_of_birth'];
             $appraisalPersonalDetail->employee_category = $request['employee_category'];
             $appraisalPersonalDetail->contract_start_date = $request['contract_start_date'];
             $appraisalPersonalDetail->contract_expiry_date = $request['contract_expiry_date'];
             $appraisalPersonalDetail->appraisal_period_start_date = $request['appraisal_period_start_date'];
             $appraisalPersonalDetail->appraisal_period_end_date = $request['appraisal_period_end_date'];
             $appraisalPersonalDetail->staff_number = $request['staff_number'];
             $appraisalPersonalDetail->save();

             /*
              * in this approach of route-model-binding, the model that matches is injected so we just return it
              * */
             $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisalRef);
             $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
             $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


             /*
              * Log audit trail
              * */
             $trail = new AuditTrail();
             $trail->action_category = AuditTrailActionCategories::$UPDATE;
             $trail->action = "UPDATED PERSONAL DETAILS " . json_encode($appraisal);
             $trail->username = Auth::user()->username;
             $trail->ip_address = $request->ip();
             $trail->api_username = "";
             LogHandler::logAuditTrail($trail);
             /*
              * End of log audit trail
              * */

             return json_encode($resp);

         }catch (\Exception $exception){

             LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
             $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
             return json_encode($resp);

         }

     }


     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalPersonalDetail  $appraisalPersonalDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalPersonalDetail $appraisalPersonalDetail)
    {
        //
    }

    public static function savePersonDetail(AppraisalPersonalDetail $personalDetail){

        $baseResp = new BaseResponse();
        try{

            $personalDetail->save();
            $baseResp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){


            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = Globals::$GENERAL_ERROR_AT_TDS . " WITH ERROR ".$exception->getMessage();
            return $baseResp;
        }

    }

}
