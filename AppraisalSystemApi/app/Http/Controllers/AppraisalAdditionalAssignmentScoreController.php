<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalAdditionalAssignmentScore;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;

class AppraisalAdditionalAssignmentScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'total_maximum_rating'=>'required|numeric',
                'total_appraisee_rating'=>'sometimes|numeric', 'total_appraiser_rating'=>'sometimes|numeric',
                'total_agreed_rating'=>'sometimes|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','total_maximum_rating','total_appraisee_rating', 'total_appraiser_rating','total_agreed_rating',];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $assignmentScore = new AppraisalAdditionalAssignmentScore();
            $assignmentScore->appraisal_reference = $appraisalRef;
            $assignmentScore->total_maximum_rating = $request['total_maximum_rating'];
            $assignmentScore->total_appraisee_rating = isset($request['total_appraisee_rating']) ? $request['total_appraisee_rating'] : null;
            $assignmentScore->total_appraiser_rating = isset($request['total_appraiser_rating']) ? $request['total_appraiser_rating'] : null;
            $assignmentScore->total_agreed_rating = isset($request['total_agreed_rating']) ? $request['total_agreed_rating'] : null;

            $assignmentScore->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "ADDITIONAL ASSIGNMENT SCORES SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED ADDITIONAL ASSIGNMENT SCORES ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalAdditionalAssignmentScore  $appraisalAdditionalAssignmentScore
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalAdditionalAssignmentScore $appraisalAdditionalAssignmentScore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalAdditionalAssignmentScore  $appraisalAdditionalAssignmentScore
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalAdditionalAssignmentScore $appraisalAdditionalAssignmentScore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param AppraisalAdditionalAssignmentScore $assignmentScore
     * @return \Illuminate\Http\Response
     * @internal param AppraisalAdditionalAssignmentScore $appraisalAdditionalAssignmentScore
     */

    public function update(Request $request, AppraisalAdditionalAssignmentScore $assignmentScore)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference'=>'required|exists:appraisals,appraisal_reference',
                'total_maximum_rating'=>'required|numeric',
                'total_appraisee_rating'=>'sometimes|numeric',
                'total_appraiser_rating'=>'sometimes|numeric',
                'total_agreed_rating'=>'sometimes|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','total_maximum_rating','total_appraisee_rating','total_appraiser_rating','total_agreed_rating'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF ADDITIONAL ASSIGNMENT SCORE DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * update the details
             * */
            $assignmentScore->appraisal_reference = $request['appraisal_reference'];
            $assignmentScore->total_maximum_rating = $request['total_maximum_rating'];
            $assignmentScore->total_appraisee_rating = isset($request['total_appraisee_rating']) ? $request['total_appraisee_rating'] : null;
            $assignmentScore->total_appraiser_rating = isset($request['total_appraiser_rating']) ? $request['total_appraiser_rating'] : null;
            $assignmentScore->total_agreed_rating = isset($request['total_agreed_rating']) ? $request['total_agreed_rating'] : null;

            $assignmentScore->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED ADDITIONAL ASSIGNMENT SCORE " . json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalAdditionalAssignmentScore  $appraisalAdditionalAssignmentScore
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalAdditionalAssignmentScore $appraisalAdditionalAssignmentScore)
    {
        //
    }
}
