<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\ActionStatus;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\ConstLetterMovementRoles;
use App\Helpers\ConstLetterMovtStatus;
use App\Helpers\EmailHandler;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\Letter;
use App\LetterDetail;
use App\LetterMovementStatus;
use App\Organization;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Letter as LetterResource;

class LetterController extends Controller
{
    public static $letterRelations = ['letterDetail','receptionUser','letterMovements'];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'work_flow_role'=>'required|min:2',  'status'=>'sometimes|min:2', 'start_date'=>'sometimes|date',  'end_date'=>'sometimes|date',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['work_flow_role','status','start_date','end_date'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            $workFlowRole = $request['work_flow_role'];
            $status = $request['status'];
            $startDate = $request['start_date'];
            $endDate = $request['end_date'];
            $receptionStatus = $request['reception_action_status'];
            $registryStatus = $request['registry_action_status'];
            $edStatus = $request['ed_action_status'];
            $employeeAssignedStatus = $request['employee_assigned_action_status'];


            /*
             * Verify whether it's an acceptable route
             * */
            if(!ConstLetterMovementRoles::validWorkflowRole($workFlowRole)){
                $resp = ResponseHandler::failureResponse("INVALID WORKFLOW ROLE [".$workFlowRole."]");
                return json_encode($resp);
            }


            /*
             * Get the authenticated user
             * */
            $user = Auth::user();
            $username = $user->username;

            $fromDate = isset($startDate) ? $startDate : '2019-01-01';
            $toDate = isset($endDate) ? $endDate : Carbon::now();


            /*
            * Check if user is allowed access to requested for details
            * */
            if(!$this->authenticatedUserHasAccessToLettersAtCurrentStatus($user->letter_movement_role_code, $workFlowRole)){

                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);

            }


            /*
             * Based on the workflow role we get the letters using a stored procedure
             * */
            if($workFlowRole == ConstLetterMovementRoles::RECEPTION){

                /*
                 * call stored procedure to get the  letters for reception, returns only IDS,
                 * Parameters to the procedure are IN paramLetterStatus VARCHAR(191), IN paramReceptionStatus VARCHAR(191), IN paramFromDate VARCHAR(191), IN paramToDate VARCHAR(191)
                 * */
                $executiveDirectorActionStatus = ActionStatus::PENDING;
                $customLetterIds =  \DB::select('call GetLettersAtReception(?,?,?,?)',array($status,$executiveDirectorActionStatus, $fromDate, $toDate));

                /*
                 * retrieve the IDS as an array from the list of objects returned as stdClass, if you ignore this you get an stdClass error
                 * */
                $ids = array_map(function ($value) {  return (array)$value;  }, $customLetterIds);

                /*
                 * retrieve the appraisals with their relationships
                 * */
                $letters = Letter::with(self::$letterRelations)->whereIn('id', $ids)->orderBy('id','desc')->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS.json_encode($request));
                $resp['data'] = $letters;
                return json_encode($resp);

            }
            else if ($workFlowRole == ConstLetterMovementRoles::REGISTRY){

                $executiveDirectorActionStatus = ActionStatus::PENDING;
                $customLetterIds =  \DB::select('call GetLettersAtRegistry(?,?,?,?)',array($status,$executiveDirectorActionStatus, $fromDate, $toDate));
                $ids = array_map(function ($value) {  return (array)$value;  }, $customLetterIds);
                $letters = Letter::with(self::$letterRelations)->whereIn('id', $ids)->orderBy('id','desc')->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS.json_encode($request));
                $resp['data'] = $letters;
                return json_encode($resp);

            }
            else if ($workFlowRole == ConstLetterMovementRoles::EXECUTIVE_DIRECTOR){


                /*
                 * Check if this guy is the executive director
                 * */
                $userOrganization = Organization::where('org_code','=',$user->org_code)->first();

                if($userOrganization == null || $userOrganization->executive_director != $user->username){
                    $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                    return json_encode($resp);
                }

                /*
                 * Now we are sure the guy is the executive director, so we get him the letters at the ED stage
                 * */

                $executiveDirectorActionStatus = ActionStatus::PENDING;
                $customLetterIds =  \DB::select('call GetLettersAtExecutiveDirector(?,?,?,?)',array($status,$executiveDirectorActionStatus, $fromDate, $toDate));
                $ids = array_map(function ($value) {  return (array)$value;  }, $customLetterIds);
                $letters = Letter::with(self::$letterRelations)->whereIn('id', $ids)->orderBy('id','desc')->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS.json_encode($request));
                $resp['data'] = $letters;
                return json_encode($resp);

            }else if ($workFlowRole == ConstLetterMovementRoles::ASSIGNED){

                $employeeAssignedStatus = ActionStatus::PENDING;
                $customLetterIds =  \DB::select('call GetLettersAssigned(?,?,?,?,?)',array($username,$status,$employeeAssignedStatus, $fromDate, $toDate));
                $ids = array_map(function ($value) {  return (array)$value;  }, $customLetterIds);
                $letters = Letter::with(self::$letterRelations)->whereIn('id', $ids)->orderBy('id','desc')->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS.json_encode($request));
                $resp['data'] = $letters;
                return json_encode($resp);

            }

            /*
             * Now if I reach here then I dont understand your request
             * */
            $resp = ResponseHandler::failureResponse("NO CRITERIA FOUND FOR LETTER SELECTION");
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //declare the response object
        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'save_action'=>'required',
                'sender'=>'required|min:2',
                'date_received'=>'required|date',
                'letter_type'=>'required',
                'receptionist_id'=>'required|exists:users,username'
            ]);


            if ($validator->fails()) {

                $validatedFields = ['sender','date_received','receptionist_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $letter = new Letter();
            $letter->letter_code = Security::generateTableIdentifier(Security::TAB_LETTER);
            $letter->movement_status = ConstLetterMovtStatus::PENDING_WORKFLOW_SUBMISSION;
            $letter->receptionist_id = Auth::user()->username;
            $letter->receptionist_status = ActionStatus::PENDING;
            $letter->registry_status = ActionStatus::PENDING;
            $letter->executive_director_status = ActionStatus::PENDING;
            $letter->employee_status = ActionStatus::PENDING;


            /*
             * attempt to save the letter
             * */

            if(!$letter->save()){
                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON SAVING THE LETTER");
                return json_encode($resp);
            }


            /*
             * letter details
             * */
            $details = new LetterDetail();
            $details->letter_code = $letter->letter_code;
            $details->sender = $request['sender'];
            $details->subject = $request['subject'];
            $details->letter_type = $request['letter_type'];
            $details->sender_reference_number = $request['sender_reference_number'];
            $details->letter_date = $request['letter_date'];
            $details->attention_to = $request['attention_to'];
            $details->signatory = $request['signatory'];
            $details->receipt_date = $request['date_received'];
            $details->receiving_officer = $request['receiving_officer'];
            $details->date_to_destination = $request['date_to_destination'];
            $details->invoice_number = $request['invoice_number'];
            $details->invoice_value = $request['invoice_value'];
            $details->currency = $request['currency'];

            if(!LetterDetailController::saveLetterDetails($details)){

                /*
                 * We failed to save the letter details so we rollback
                 * */
                $letter->delete();

                /*
                 * Log trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$CREATE;
                $trail->action = "ERROR OCCURRED ON SAVING LETTER DETAILS AND ACTION ROLLED BACK ".json_encode($letter)." Details ".json_encode($details);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                /*
                 * return an error
                 * */
                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON SAVING THE LETTER DETAILS");
                return json_encode($resp);

            }


            /*
             * If action type is save and submit proceed with submission
             * */
            $saveAction = $request['save_action'];

            if($saveAction == 'save_and_submit'){

                /*
                 * Submit to the workflow
                 * */
                return $this->sendLetterToWorkflow($letter->letter_code, $request->ip());

            }


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED LETTER ".json_encode($letter);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Letter  $letter
     * @return \Illuminate\Http\Response
     */
    public function show(Letter $letter)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $letter = Letter::with(self::$letterRelations)->find($letter->id);
            $resp['data'] = $letter; // json_encode($letter);//new LetterResource($letter);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Letter  $letter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Letter $letter)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'letter_code'=>'required|exists:letters,letter_code',
                'save_action'=>'required',
                'sender'=>'required|min:2',
                'date_received'=>'required|date',
                'letter_type'=>'required',
                'receptionist_id'=>'required|exists:users,username'
            ]);


            if ($validator->fails()) {
                $validatedFields = ['save_action','letter_code','sender','date_received','receptionist_id'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);
            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $letter->sender = strtoupper($request['sender']);
            $letter->external_reference = $request['external_reference'];
            $letter->subject = $request['subject'];
            $letter->letter_type = $request['letter_type'];
            $letter->date_received = $request['date_received'];
            $letter->addressed_to = $request['addressed_to'];


            $details = LetterDetail::where('letter_code','=',$letter->letter_code)->first();
            if($details == null){
                $resp = ResponseHandler::failureResponse("FAILED TO GET LETTER DETAILS FOR LETTER [".$letter->letter_code."]");
                return json_encode($resp);
            }

            $details->sender = $request['sender'];
            $details->subject = $request['subject'];
            $details->letter_type = $request['letter_type'];
            $details->sender_reference_number = $request['sender_reference_number'];
            $details->letter_date = $request['letter_date'];
            $details->attention_to = $request['attention_to'];
            $details->signatory = $request['signatory'];
            $details->receipt_date = $request['date_received'];
            $details->date_to_destination = $request['date_to_destination'];
            $details->invoice_number = $request['invoice_number'];
            $details->invoice_value = $request['invoice_value'];
            $details->currency = $request['currency'];

            $details->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED LETTER ".json_encode($letter);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            $saveAction = $request['save_action'];

            if($saveAction == 'save'){
                return json_encode($resp);
            }

            $letterStatus = $letter->movement_status;
            return $letterStatus == ConstLetterMovtStatus::PENDING_WORKFLOW_SUBMISSION ?
                $this->sendLetterToWorkflow($letter->letter_code, $request->ip()) :
                $this->workFormMove($letter->letter_code, true,"", $request->ip(),"");


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Letter  $letter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Letter $letter)
    {
        //
    }

    /**
     * @param Request $request
     * @return string
     */
    public function letterWorkflowStart(Request $request){

        try{

            $validator = Validator::make($request->all(), [
                'letter_code'=>'required|min:2|exists:letters,letter_code',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['letter_code'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }


            $letterCode = $request['letter_code'];
            $ip = $request->ip();
            return $this->sendLetterToWorkflow($letterCode, $ip);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }


    public function letterWorkflowMove(Request $request){

        try{

            $validator = Validator::make($request->all(), [
                'letter_code'=>'required|min:2|exists:letters,letter_code',
                'is_movement_forward'=>'required|boolean',
                'employee_assigned'=>'sometimes|exists:users,username',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['letter_code','is_movement_forward'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            $letterCode = $request['letter_code'];
            $isMovementForward = $request['is_movement_forward'];
            $remark = $request['remark'];
            $personAssigned = $request['employee_assigned'];

            return $this->workFormMove($letterCode, $isMovementForward, $remark, $request->ip(), $personAssigned);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }


    private function requestIsFromReceptionistWhoCreatedTheLetter($letter, $authUser) {

        return $authUser->username == $letter->receptionist_id;

    }

    private function checkIfAuthenticatedUserIsExpectedAuthorOfLetterAtThisStatus($letter, $authUser) {

        $letterCurrentStatus = $letter->movement_status;
        $requesterUsername = $authUser->username;
        $expectedUsername = "";

        if(
            $letterCurrentStatus == ConstLetterMovtStatus::AT_REGISTRY
        ){
            $expectedUsername = $letter->registry_user_id;
        }
        else if($letterCurrentStatus == ConstLetterMovtStatus::AT_EXECUTIVE_DIRECTOR){
            $expectedUsername = $letter->executive_director_id;
        }
        else if($letterCurrentStatus == ConstLetterMovtStatus::ASSIGNED){
            $expectedUsername = $letter->employee_assigned;
        }

        return $requesterUsername == $expectedUsername;

    }

    private function moveLetterForward(Letter $letter, $currentLetterStatus, $toStatusApproved, $ipAddress, $employeeAssigned) {

        try{

            $dateNow = Carbon::now();
            $letter->movement_status = $toStatusApproved;

            $emailRecipient = "";
            $emailMsg = "";
            $emailSubject = "";

            /*
             * Update the action dates
             * */
            if($currentLetterStatus == ConstLetterMovtStatus::AT_REGISTRY){
                $letter->registry_action_date = $dateNow;
                $letter->registry_status = ActionStatus::SUBMITTED;
            }
            else if($currentLetterStatus == ConstLetterMovtStatus::AT_EXECUTIVE_DIRECTOR){
                $letter->executive_director_action_date = $dateNow;
                $letter->executive_director_status = ActionStatus::SUBMITTED;
                $letter->employee_assigned = $employeeAssigned;
            }
            else if($currentLetterStatus == ConstLetterMovtStatus::ASSIGNED){
                $letter->employee_action_date = $dateNow;
                $letter->employee_status = ActionStatus::ACTIONED;
            }


            /*
             * Update the submission date
             * */
            if($toStatusApproved == ConstLetterMovtStatus::AT_REGISTRY){
                $letter->registry_submission_date = $dateNow;

                $emailRecipient = $letter->registry_user_id;
                $emailMsg = "Hello, there is a letter submitted that is pending your review. Thank You";
                $emailSubject = Globals::$MSG_LETTER_FOR_ACTION;
            }
            else if($toStatusApproved == ConstLetterMovtStatus::AT_EXECUTIVE_DIRECTOR){
                $letter->executive_director_submission_date = $dateNow;

                $emailRecipient = $this->getExecutiveDirectorForOrgOfTheReceptionist($letter->receptionist_id);//$letter->executive_director_id;
                $emailMsg = "Hello, there is a letter submitted that is pending your review. Thank You";
                $emailSubject = Globals::$MSG_LETTER_FOR_ACTION;
            }
            else if($toStatusApproved == ConstLetterMovtStatus::ASSIGNED){
                $letter->employee_submission_date = $dateNow;

                $emailRecipient = $letter->employee_assigned;
                $emailMsg = "Hello, there is a letter submitted that is pending your action. Thank You";
                $emailSubject = Globals::$MSG_LETTER_FOR_ACTION;
            }


            /*
             * Save the appraisal
             * */
            if(!$letter->save()){
                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON MOVING THE LETTER IN WORK FLOW");
                return $resp;
            }

            /*
             * Get the recipient user by username
             * */
            $recipientUser = User::where('username','=',$emailRecipient)->first();

            if($recipientUser == null){
                $resp = ResponseHandler::successResponse("LETTER FORWARDED BUT FAILED TO GET USER DETAILS FOR NOTIFICATION, USERNAME [".$emailRecipient."]");
                return $resp;
            }

            $email = $recipientUser->email;

            /*
             * Send notification email
             * */
            $baseResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $emailSubject,$emailMsg);
            $emailMsg = $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ?
                "OPERATION SUCCESSFULLY" : "OPERATION SUCCESSFULLY BUT EMAIL NOT SENT DUE TO ".$baseResp->statusDescription;

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = $emailMsg;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "MOVED LETTER IN WORKFLOW ".json_encode($letter);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $ipAddress;
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    private function authenticatedUserHasAccessToLettersAtCurrentStatus($userLetterMovementRole, $workFlowRole) {

        return $userLetterMovementRole == $workFlowRole || $workFlowRole == ConstLetterMovementRoles::ASSIGNED || $workFlowRole == ConstLetterMovementRoles::EXECUTIVE_DIRECTOR;

    }

    private function getExecutiveDirectorForOrgOfTheReceptionist($receptionistUsername) {

        try{

            $user = User::with(['organization'])->where('username','=',$receptionistUsername)->first();

            if($user != null && isset($user->organization)){
                return $user->organization->executive_director;
            }

            return '';

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            return '';

        }

    }

    public function sendLetterToWorkflow($letterCode, $ipAddress) {


        /*
         * Get logged in user
         * */
        $authUser = Auth::user();

        /*
         * Failed to get logged in user
         * */
        if($authUser == null){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
            return json_encode($resp);
        }

        $letter = Letter::where('letter_code','=',$letterCode)->first();

        /*
         * Check if the request is from the creator of the letter
         * */
        if(!$this->requestIsFromReceptionistWhoCreatedTheLetter($letter, $authUser)){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
            return json_encode($resp);
        }

        /*
         * Check if the letter has not yet been submitted
         * */

        $movementStatus = $letter->movement_status;
        if($movementStatus != ConstLetterMovtStatus::PENDING_WORKFLOW_SUBMISSION){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_ERROR_CANNOT_BE_SUBMITTED_TO_WORK_FLOW.' ['. $movementStatus .']');
            return json_encode($resp);
        }

        $newLetterStatus = ConstLetterMovtStatus::AT_REGISTRY;


        /*
         * Update the letter details
         * */
        $dateTimeNow = Carbon::now();

        $letter->movement_status = $newLetterStatus;
        $letter->registry_submission_date = $dateTimeNow;
        $letter->receptionist_action_date = $dateTimeNow;
        $letter->receptionist_status = ActionStatus::SUBMITTED;


        /*
         * Attempt to save
         * */
        if(!$letter->save()){
            $resp = ResponseHandler::failureResponse("ERROR SUBMITTING LETTER TO WORKFLOW");
            return json_encode($resp);
        }



        /*
         * Send notification email to registry personnel
         * */

        $letterMovementRole = ConstLetterMovementRoles::REGISTRY;
        $users = UserController::findLetterMovementFlowUsersByLetterMovementRole($letterMovementRole);

        if(count($users) < 1){
            $msg = 'LETTER SUBMITTED TO REGISTRY BUT EMAIL NOT SENT DUE TO UNKNOWN REGISTRY USERS';
        }else{

            $baseResp = EmailHandler::emailLetterPendingReviewToRegistryUsers($letter, $users);
            $msg = $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ?
                'LETTER SUBMITTED TO REGISTRY. '.$baseResp->statusDescription : "LETTER SUBMITTED TO REGISTRY BUT EMAIL NOT SENT DUE TO ".$baseResp->statusDescription;

        }


        /*
         * compose success response
         * */
        $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
        $resp['statusDescription'] = $msg;


        /*
         * Log audit trail
         * */
        $trail = new AuditTrail();
        $trail->action_category = AuditTrailActionCategories::$UPDATE;
        $trail->action = "SUBMITTED LETTER TO WORKFLOW ".json_encode($letter);
        $trail->username = $authUser->username;
        $trail->ip_address = $ipAddress;
        $trail->api_username = "";
        LogHandler::logAuditTrail($trail);


        /*
         * return the success response
         * */
        return json_encode($resp);

    }

    private function workFormMove($letterCode, $isMovtForward, $remark, $ip, $personAssigned) {

        $authUser = Auth::user();

        /*
         * Failed to get logged in user
         * */
        if($authUser == null){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
            return json_encode($resp);
        }

        $letter = Letter::where('letter_code','=',$letterCode)->first();

        /*
         * Check if the letter has not yet been submitted to workflow or has already been actioned by the employee
         * */
        if($letter->movement_status == ConstLetterMovtStatus::PENDING_WORKFLOW_SUBMISSION ||
            $letter->movement_status == ConstLetterMovtStatus::ACTIONED_BY_ASSIGNEE){

            $resp = ResponseHandler::failureResponse(Globals::$MSG_LETTER_MOVEMENT_NOT_ALLOWED.' CURRENT STATUS ['.$letter->movement_status.']');
            return json_encode($resp);

        }

        /*
         * We verify whether the request is coming from the person who is supposed to be with the letter at this status as defined
         * */
        if(!$this->checkIfAuthenticatedUserIsExpectedAuthorOfLetterAtThisStatus($letter, $authUser) && !Globals::$DEBUG_MODE){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
            return json_encode($resp);
        }

        /*
         * Get the details of the current appraisal status ie the FromStatus and the ToStatus
         * */
        $currentLetterStatus = $letter->movement_status;
        $letterStatusDetails = LetterMovementStatus::where('status_code','=', $currentLetterStatus)->first();

        /*
         * We failed to get the status details
         * */
        if($letterStatusDetails == null){
            $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET__LETTER_STATUS_DETAILS. " [".$currentLetterStatus."]");
            return json_encode($resp);
        }

        /*
         * We have the status details
         * */
        $toStatusApproved = $letterStatusDetails->to_status_approved;
        $toStatusRejected = $letterStatusDetails->to_status_rejected;
        $isMovementForward = $isMovtForward;
        $employeeAssigned = $personAssigned;

        /*
         * Request is to approve the Appraisal
         * */
        if($isMovementForward == true){

            return $this->moveLetterForward($letter, $currentLetterStatus, $toStatusApproved, $ip, $employeeAssigned);

        }

        /*
         * Request is to reject the approval
         * */
        $resp = ResponseHandler::failureResponse("Letter Rejection Function Not Yet Supported, Please Contact the System Administrator");
        return json_encode($resp);


    }

}
