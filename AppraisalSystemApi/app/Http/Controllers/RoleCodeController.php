<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\RoleCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\RoleCode as RoleCodeResource;

class RoleCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        /*
        * declare the response object
        * */
        $resp = [];

        try{

            /*
             * get the role codes
             * */
            $roleCodes = RoleCode::all();


            /*
             * build success response
             * */
            $resp['data'] = $roleCodes;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //declare the response object
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'role_name'=>'required|min:2',
                'org_code'=>'required',
                'created_by'=>'required'
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['role_name','role_code','org_code','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }



            /*
             * everything is fine now
             * */



            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $roleCode = new RoleCode();
            $roleCode->role_name = strtoupper($request['role_name']);
            $roleCode->role_code = Security::generateTableIdentifier(Security::TAB_ROLE);
            $roleCode->org_code = $request['org_code'];
            $roleCode->page = $request['page'];
            $roleCode->created_by = $request['created_by'];

            /*
             * save the role code details
             * */
            $roleCode->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED ROLE CODE ".json_encode($roleCode);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoleCode  $roleCode
     * @return \Illuminate\Http\Response
     */
    public function show(RoleCode $roleCode)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new RoleCodeResource($roleCode);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoleCode  $roleCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoleCode $roleCode)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'role_name'=>'required|min:2',
                'role_code'=> 'required|exists:role_codes,role_code',
                'org_code'=> 'exists:organizations,org_code',
                'created_by'=>'required',//add constraint for an existent user
                'updated_by'=>'required',//add constraint for an existent user
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['role_name','role_code','org_code','created_by','updated_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF ROLE CODE DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['updated_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }


            /*
             * update the role code details
             * */
            $roleCode->role_name = $request['role_name'];
            $roleCode->org_code = $request['org_code'];
            $roleCode->created_by = $request['created_by'];
            $roleCode->page = $request['page'];
            $roleCode->active = $request['active'];

            /*
             * save the updated details
             * */
            $roleCode->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new RoleCodeResource($roleCode);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED ROLE CODE " . json_encode($roleCode);
            $trail->username = $request['updated_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\RoleCode $roleCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,RoleCode $roleCode)
    {

        try{

            /*
            * validate using Laravel's validator class
            * */
            $validator = Validator::make($request->all(), [
                'deleted_by'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['deleted_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$DELETE;
                $trail->action = "FAILED ROLE CODE DELETION DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['deleted_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * Attempt to delete
             * */
            if(!$roleCode->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING ROLE CODE"));
            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED ROLE CODE " . json_encode($roleCode);
            $trail->username = $request['deleted_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * Return success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }

}
