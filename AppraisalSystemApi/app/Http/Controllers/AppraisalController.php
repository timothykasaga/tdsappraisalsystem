<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalPersonalDetail;
use App\AppraisalStatus;
use App\AuditTrail;
use App\BaseResponse;
use App\CancelledAppraisal;
use App\ErrorLog;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\ConstAppraisalStatus;
use App\Helpers\DataLoader;
use App\Helpers\EmailHandler;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\PdfHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use App\Helpers\WorkflowRoles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Appraisal as AppraisalResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AppraisalController extends Controller
{

    private static $appraisalRelations = [
        'appraisalPersonalDetail',
        'appraisalAcademicBackgrounds',
        'appraisalKeyDuties',
        'appraisalAssignments',
        'appraisalAdditionalAssignments',
        'appraisalCompetenceAssessments',
        'appraisalTrainPerformanceGaps',
        'appraisalTrainChallenges',
        'appraisalWorkplans',
        'appraisalAppraiserComment',
        'appraisalStrengthAndWeekness',
        'appraisalAppraiserRecommendation',
        'appraisalSupervisorDeclaration',
        'appraisalHodComment',
        'appraisalAppraiseeRemark',
        'appraisalDirectorComment',
        'appraisalTrainSummary',
        'appraisalAssignmentSummary',
        'appraisalAssignmentScore',
        'appraisalAdditionalAssignmentScore',
        'appraisalCompetenceAssessmentSummary',
        'appraisalCompetenceAssessmentScore',
    ];

    public static function findAppraisalsByUsername($username, $top = -1, $initiatedByUser = true) {


        if($initiatedByUser){

            $appraisals = Appraisal::with(self::$appraisalRelations)->where('owner_id','=',$username)->orderBy('id','desc')->limit($top)->get();
            return $appraisals;

        }


        $appraisals = Appraisal::
        with(self::$appraisalRelations)->
        where('supervisor_id','=',$username)->
        orWhere('department_head_id','=',$username)->
        orWhere('executive_director_id','=',$username)->
        orderBy('id','desc')->limit($top)->get();

        return $appraisals;

    }


    public function latestAppraisals()
    {

        try{

            $user = Auth::user();

            /*
             * Get the logged in user
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            /*
             * Get the latest appraisals
             * */
            $username = $user->username;
            $appraisalsOwned = self::findAppraisalsByUsername($username,3,true);
            $appraisalsAssigned = self::findAppraisalsByUsername($username,3,false);


            /*
             * Build the success response
             * */
            $data = [];
            $data['appraisals_owned'] = $appraisalsOwned;
            $data['appraisals_assigned'] = $appraisalsAssigned;

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            $resp['data'] = $data;

            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [

                'work_flow_role'=>'required|min:2',  'status'=>'sometimes|min:2', 'start_date'=>'sometimes|date',  'end_date'=>'sometimes|date',
                'supervisor_decision'=>'sometimes', 'department_head_decision'=>'sometimes', 'director_decision'=>'sometimes',

            ]);

                if ($validator->fails()) {

                    $validatedFields = ['work_flow_role','status','start_date','end_date','supervisor_decision','department_head_decision','director_decision'];
                    $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                    $resp = ResponseHandler::failureResponse($firstValidationError);
                    return json_encode($resp);

            }

            /*
            * Get the authenticated user
            * */
            $bearerToken = $request->header('Authorization');
            if($bearerToken == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }
            $token = str_replace('Bearer ','',$bearerToken);
            $baserResp = DataLoader::getAuthenticatedUser($token);

            if($baserResp->statusCode != Globals::$STATUS_CODE_SUCCESS){
                $resp = ResponseHandler::failureResponse(json_encode($baserResp).Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }
            $user = $baserResp->result;


            $workFlowRole = $request['work_flow_role'];
            $status = $request['status'];
            $startDate = $request['start_date'];
            $endDate = $request['end_date'];
            $supervisorDecision = $request['supervisor_decision'];
            $hodDecision = $request['department_head_decision'];
            $directorDecision = $request['director_decision'];


            /*
             * Verify whether it's an acceptable route
             * */
            if(!WorkflowRoles::validWorkflowRole($workFlowRole)){
                $resp = ResponseHandler::failureResponse("INVALID WORKFLOW ROLE [".$workFlowRole."]");
                return json_encode($resp);
            }

            $username = $user->username;

            $fromDate = isset($startDate) ? $startDate : '2019-01-01';
            $toDate = isset($endDate) ? $endDate : Carbon::now();

            /*
             * Based on the workflow role we get the appraisals using a stored procedure
             * */
            if($workFlowRole == WorkflowRoles::SUPERVISOR){

                /*
                 * call stored procedure to get the  supervisor appraisals, returns only IDS,
                 * Parameters to the procedure are IN paramUsername VARCHAR(191),IN paramAppraisalStatus VARCHAR(191), IN paramSupervisorDecision VARCHAR(191), IN paramFromDate VARCHAR(191), IN paramToDate VARCHAR(191))
                 * */
                $customAppraisalsIds =  \DB::select('call GetSupervisorAppraisals(?,?,?,?,?)',array($username,$status,$supervisorDecision, $fromDate, $toDate));

                /*
                 * retrieve the IDS as an array from the list of objects returned as stdClass, if you ignore this you get an stdClass error
                 * */
                $ids = array_map(function ($value) {  return (array)$value;  }, $customAppraisalsIds);

                /*
                 * retrieve the appraisals with their relationships
                 * */
                $appraisals = Appraisal::with(self::$appraisalRelations)->whereIn('id', $ids)->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS.json_encode($request));
                $resp['data'] = $appraisals;
                return json_encode($resp);

            }
            else if ($workFlowRole == WorkflowRoles::HOD){

                $customAppraisalsIds =  \DB::select('call GetHodAppraisals(?,?,?,?,?)',array($username,$status,$hodDecision, $fromDate, $toDate));
                $ids = array_map(function ($value) {  return (array)$value;  }, $customAppraisalsIds);
                $appraisals = Appraisal::with(self::$appraisalRelations)->whereIn('id', $ids)->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
                $resp['data'] = $appraisals;
                return json_encode($resp);

            }
            else if ($workFlowRole == WorkflowRoles::DIRECTOR){

                $customAppraisalsIds =  \DB::select('call GetExecutiveDirectorAppraisals(?,?,?,?,?)',array($username,$status,$directorDecision, $fromDate, $toDate));
                $ids = array_map(function ($value) {  return (array)$value;  }, $customAppraisalsIds);
                $appraisals = Appraisal::with(self::$appraisalRelations)->whereIn('id', $ids)->get();

                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
                $resp['data'] = $appraisals;
                return json_encode($resp);

            }else if ($workFlowRole == WorkflowRoles::ALL && $status == 'incomplete'){

                $appraisals = Appraisal::with(self::$appraisalRelations)->where('appraisal_status','!=',ConstAppraisalStatus::COMPLETED_SUCCESSFULLY)->orderBy('id','desc')->get();
                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
                $resp['data'] = $appraisals;
                return json_encode($resp);

            }else if ($workFlowRole == WorkflowRoles::ALL && $status == 'complete'){

                $appraisals = Appraisal::with(self::$appraisalRelations)->where('appraisal_status','=',ConstAppraisalStatus::COMPLETED_SUCCESSFULLY)->orderBy('id','desc')->get();
                $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
                $resp['data'] = $appraisals;
                return json_encode($resp);

            }else if ($workFlowRole == WorkflowRoles::ALL){
                $usernameColumnToMatch = 'none';
            }else{
                $usernameColumnToMatch = 'owner_id';
            }


            /*
             * Build the query to fetch the results
             * */
            if($usernameColumnToMatch == 'none'){

                $appraisals = Appraisal::with(AppraisalController::$appraisalRelations)
                    ->where('appraisal_status', '=', $status)
                    ->where(function ($query) use ($supervisorDecision) {
                        $query->where('supervisor_decision', '=', $supervisorDecision)
                            ->orWhere('supervisor_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($hodDecision) {
                        $query->where('department_head_decision', '=', $hodDecision)
                            ->orWhere('department_head_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($directorDecision) {
                        $query->where('executive_director_decision', '=', $directorDecision)
                            ->orWhere('executive_director_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($startDate) {
                        $query->whereDate('created_at', '>=', isset($startDate) ? $startDate : '2019-01-01')
                            ->orWhere('created_at', '!=', '-1');
                    })
                    ->where(function ($query) use ($endDate) {
                        $query->whereDate('created_at', '<=', isset($endDate) ? $endDate : Carbon::now())
                            ->orWhere('created_at', '!=', '-1');
                    })
                    ->orderBy('id','desc')
                    ->get();


            }else{

                $appraisals = Appraisal::with(AppraisalController::$appraisalRelations)
                    ->where($usernameColumnToMatch, '=', $username)
                    ->where(function ($query) use ($status) {
                        $query->where('appraisal_status', '=', $status)
                            ->orWhere('appraisal_status', '!=', '-1');
                    })
                    ->where(function ($query) use ($supervisorDecision) {
                        $query->where('supervisor_decision', '=', $supervisorDecision)
                            ->orWhere('supervisor_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($hodDecision) {
                        $query->where('department_head_decision', '=', $hodDecision)
                            ->orWhere('department_head_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($directorDecision) {
                        $query->where('executive_director_decision', '=', $directorDecision)
                            ->orWhere('executive_director_decision', '!=', '-1');
                    })
                    ->where(function ($query) use ($startDate) {
                        $query->whereDate('created_at', '>=', isset($startDate) ? $startDate : '2019-01-01')
                            ->orWhere('created_at', '!=', '-1');
                    })
                    ->where(function ($query) use ($endDate) {
                        $query->whereDate('created_at', '<=', isset($endDate) ? $endDate : Carbon::now())
                            ->orWhere('created_at', '!=', '-1');
                    })
                    ->orderBy('id','desc')
                    ->get();

            }


            /*
             * Build the success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            $resp['data'] = $appraisals;

            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "FETCH APPRAISALS ".json_encode($resp);
            $trail->username = $username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);



            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [

                'owner_id'=>'required|min:2|exists:users,username',
                'appraisal_type'=>'required|min:2',
                'department_head_id'=>'required|min:2',
                'supervisor_id'=>'required|min:2',
                'executive_director_id'=>'required|min:2',
                'person_info_employee_category'=>'required|min:2',
                'person_info_first_name'=>'required|min:2',
                'person_info_last_name'=>'required|min:2',
                'person_info_department'=>'required|min:2',
                'person_info_designation'=>'required|min:2',
                'person_info_date_of_birth'=>'required|min:2',
                'person_info_appraisal_period_start_date'=>'required|date',
                'person_info_appraisal_period_end_date'=>'required|date',
                'person_info_contract_start_date'=>'required|min:2',
                'person_info_contract_expiry_date'=>'required|min:2',
                'person_info_staff_number'=>'required|min:2'

            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {


                /*
                 * fields validated
                 * */
                $validatedFields = ['owner_id','appraisal_type','person_info_first_name','person_info_employee_category','person_info_last_name','person_info_department','person_info_designation',
                    'person_info_date_of_birth','person_info_appraisal_period_start_date','person_info_appraisal_period_end_date',
                    'person_info_contract_start_date','person_info_contract_expiry_date','person_info_staff_number',
                    'department_head_id','supervisor_id','executive_director_id',
                ];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);


            }


            /*
             * Check if there is no appraisal period overlap
             * */
            $periodStartDate = $request['person_info_appraisal_period_start_date'];
            $periodEndDate = $request['person_info_appraisal_period_end_date'];
            $username = Auth::user()->username;
            $periodCheckResp = $this->appraisalPeriodRangeDoesnotOverlapExistingAppraisal($username,$periodStartDate, $periodEndDate);


            /*
             * If overlap exists, return error
             * */
            if($periodCheckResp->statusCode != Globals::$STATUS_CODE_SUCCESS){
                $error = "APPRAISAL PERIOD OVERLAP EXISTS WITH THE FOLLOWING APPRAISAL(S)\n\n" . $periodCheckResp->statusDescription;
                $resp = ResponseHandler::failureResponse($error);
                return json_encode($resp);
            }


            /*
             * Generate an appraisal reference ID
             * */
            $reference = Security::getCreateReference();


            /*
             * Generate a PDF Name for the Appraisal PDF to be generated
             * */
            $docNameFirstName = $request['person_info_first_name'];
            $docNameLastName = $request['person_info_last_name'];
            $docNameStartDate = $periodStartDate;
            $docNameEndDate = $periodEndDate;
            $documentName = Security::createDocumentName($docNameFirstName,$docNameLastName, $docNameStartDate, $docNameEndDate);


            /*
             * Save the appraisal header details
             * */
            $appraisal = new Appraisal();

            $appraisal->appraisal_reference = $reference;
            $appraisal->appraisal_type = $request['appraisal_type'];
            $appraisal->department_head_id = $request['department_head_id'];
            $appraisal->supervisor_id = $request['supervisor_id'];
            $appraisal->executive_director_id = $request['executive_director_id'];
            $appraisal->generated_pdf_name = $documentName;
            $appraisal->appraisal_status = ConstAppraisalStatus::PENDING_WORKFLOW_SUBMISSION;
            $appraisal->created_at = Carbon::now();
            $appraisal->owner_id = $request['owner_id'];

            /*
             * Save the appraisal details,
             * I am thinking I should have a stored procedure that saves these details together with the details
             * of section A (Person Details model)
             * */
            if(!$appraisal->save()){

                $msg = "ERROR OCCURRED ON SAVING APPRAISAL FORM";
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);
            }


            /*
             * We have successfully saved the appraisal we now have to save the Personal Details for this appraisal
             * */

            $personDetails = new AppraisalPersonalDetail();
            $personDetails->appraisal_reference = $appraisal->appraisal_reference;
            $personDetails->first_name = $request['person_info_first_name'];
            $personDetails->last_name = $request['person_info_last_name'];
            $personDetails->other_name = $request['person_info_other_name'];
            $personDetails->department = $request['person_info_department'];
            $personDetails->designation = $request['person_info_designation'];
            $personDetails->date_of_birth = $request['person_info_date_of_birth'];
            $personDetails->contract_start_date = $request['person_info_contract_start_date'];
            $personDetails->contract_expiry_date = $request['person_info_contract_expiry_date'];
            $personDetails->appraisal_period_start_date = $periodStartDate;
            $personDetails->appraisal_period_end_date = $periodEndDate;
            $personDetails->staff_number = $request['person_info_staff_number'];
                $personDetails->employee_category = $request['person_info_employee_category'];


            /*
             * Save using the PersonDetailsController for consistency
             * */
            $baseResp = AppraisalPersonalDetailController::savePersonDetail($personDetails);

            if($baseResp->statusCode != Globals::$STATUS_CODE_SUCCESS){

                /*
                 * Rollback creation of appraisal
                 * */
                $appraisal->delete();

                $msg = "FAILED TO SAVE APPRAISAL FORM DUE TO FAILURE TO SAVE PERSONAL INFO WITH ERROR " . $baseResp->statusDescription;
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);

            }


            /*
             * compose success response
             * */
            $resp['data'] = self::findAppraisalByAppraisalReference($reference);//new AppraisalResource(Appraisal::find($appraisal->id));
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED APPRAISAL ".json_encode($appraisal);
            $trail->username = $request['owner_id'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function show(Appraisal $appraisal)
    {

        try{

            $appraisal = self::findAppraisalByAppraisalReference($appraisal->appraisal_reference);

            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisal->appraisal_reference."]");
                return json_encode($resp);
            }

            $resp = ResponseHandler::successResponse(Globals::$STATUS_CODE_SUCCESS);
            $resp['data'] = $appraisal;
            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appraisal $appraisal)
    {
        //
    }


    public function updateApprovers(Request $request)
    {
        //declare the response object
        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_ref'=>'required|min:2',
                'department_head_id'=>'required|min:2',
                'supervisor_id'=>'required|min:2',
                'executive_director_id'=>'required|min:2',
            ]);

            if ($validator->fails()) {

                $validatedFields = ['appraisal_ref', 'department_head_id','supervisor_id','executive_director_id',
                ];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);


            }


            $appraisal = self::findAppraisalByAppraisalReference($request['appraisal_ref'], false);

            if($appraisal == null){
                $msg = Globals::$MSG_FAILED_TO_GET_APPRAISAL;
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);
            }

            $appraisal->department_head_id = $request['department_head_id'];
            $appraisal->supervisor_id = $request['supervisor_id'];
            $appraisal->executive_director_id = $request['executive_director_id'];

            if(!$appraisal->save()){

                $msg = "ERROR OCCURRED ON SAVING APPRAISAL APPROVERS";
                $resp = ResponseHandler::failureResponse($msg);
                return json_encode($resp);
            }


            /*
             * compose success response
             * */
            $resp['data'] = self::findAppraisalByAppraisalReference($appraisal->appraisal_reference);//new AppraisalResource(Appraisal::find($appraisal->id));
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED APPRAISAL ".json_encode($appraisal);
            $trail->username = $request['owner_id'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appraisal $appraisal)
    {
        //
    }


    /**
     * @param Request $request
     * @return string
     */
    public function workflowStart(Request $request){

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'appraisal_reference'=>'required|min:2|exists:appraisals,appraisal_reference',
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {


                /*
                 * fields validated
                 * */
                $validatedFields = ['appraisal_reference','supervisor_id','department_head_id','executive_director_id'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);


            }


            /*
             * Get logged in user
             * */
            $authUser = Auth::user();

            /*
             * Failed to get logged in user
             * */
            if($authUser == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=',$appraisalRef)->first();

            /*
             * Check if the request is from the owner of the appraisal
             * */
            if(!$this->requestIsFromOwnerOfAppraisal($appraisal, $authUser)){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Check if approvers are assigned
             * */
            if(!$this->areAllApproversAssigned($appraisal)){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_ERROR_APPRAISAL_APPROVERS_NOT_ASSIGNED);
                return json_encode($resp);
            }

            $newAppraisalStatus = ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL;


            /*
             * Update the appraisal details
             * */
            $appraisal->appraisal_status = $newAppraisalStatus;
            $appraisal->supervisor_submission_date = Carbon::now();

            /*
             * Now i have added this because after re-submission incase of a rejection, the appraisal was still having the
             * rejected status, now I am thinking wont need to keep track of the status of the appraisal or we will depend
             * on the audit trail only???????
             * */
            $appraisal->supervisor_decision = 'PENDING';
            $appraisal->department_head_decision = 'PENDING';
            $appraisal->executive_director_decision = 'PENDING';


            /*
             * Attempt to save
             * */
            if(!$appraisal->save()){
                $resp = ResponseHandler::failureResponse("ERROR SUBMITTING APPRAISAL TO WORKFLOW");
                return json_encode($resp);
            }


            /*
             * Send notification email to supervisor
             * */
            $baseResp = EmailHandler::emailAppraisalPendingSupervisorApproval($appraisal);
            $msg = $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ?
            "APPRAISAL SUCCESSFULLY SUBMITTED FOR APPROVAL" : "APPRAISAL SUBMITTED FOR APPROVAL BUT EMAIL NOT SENT DUE TO ".$baseResp->statusDescription;


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = $msg;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "SUBMITTED APPRAISAL TO WORKFLOW ".json_encode($appraisal);
            $trail->username = $authUser->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    public function workflowMove(Request $request){

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'appraisal_reference'=>'required|min:2|exists:appraisals,appraisal_reference',
                'is_movement_forward'=>'required|boolean',
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['appraisal_reference','is_movement_forward'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);


            }


            /*
             * Get logged in user
             * */
            $authUser = Auth::user();

            /*
             * Failed to get logged in user
             * */
            if($authUser == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=',$appraisalRef)->first();

           /*
            * We verify whether the request is coming from the person who is supposed to be with form at this status as defined
            * */
            if(!$this->checkIfAuthenticatedUserIsExpectedAuthorOfAppraisalAtThisStatus($appraisal, $authUser) && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Get the details of the current appraisal status ie the FromStatus and the ToStatus
             * */
            $currentAppraisalStatus = $appraisal->appraisal_status;
            $appraisalStatus = AppraisalStatus::where('status_code','=', $currentAppraisalStatus)->first();

            /*
             * We failed to get the status details
             * */
            if($appraisalStatus == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_STATUS_DETAILS. " ".$currentAppraisalStatus);
                return json_encode($resp);
            }

            /*
             * We have the status details
             * */
            $toStatusApproved = $appraisalStatus->to_status_approved;
            $toStatusRejected = $appraisalStatus->to_status_rejected;
            $isMovementForward = $request['is_movement_forward'];
            $remark = $request['remark'];

            /*
             * Request is to approve the Appraisal
             * */
            if($isMovementForward == true){

                return $this->appraisalApproval($appraisal, $currentAppraisalStatus, $toStatusApproved, $request);

            }

            /*
             * Request is to reject the approval
             * */
            return $this->appraisalRejection($appraisal, $currentAppraisalStatus, $toStatusRejected, $remark, $request);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    /**
     * @param Appraisal $appraisal
     * @return string
     */
    public function workflowCancel(Appraisal $appraisal){

        try{

            $status = $appraisal->appraisal_status;
            $appraisalOwner = $appraisal->owner_id;

            /*
             * Check if appraisal can be cancel at this stage
             * */
            if(!ConstAppraisalStatus::appraisalCanBeCancelled($status)){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_ACTION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * Get logged in user
             * */
            $authUser = Auth::user();

            /*
             * Failed to get logged in user
             * */
            if($authUser == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);
            }

            /*
             * We verify whether the request is  from the owner of the appraisal
             * */
            if($appraisalOwner != $authUser->username){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * Call stored procedure to cancel the appraisal
             * */
            \DB::statement('call CancelAppraisalByAppraisalOwner(?,?)',array($appraisal->appraisal_reference,$authUser->username));


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CANCELLED APPRAISAL ".json_encode($appraisal);
            $trail->username = $authUser->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            /*
             * Return a valid response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_CODE_SUCCESS);
            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


    private function requestIsFromOwnerOfAppraisal($appraisal, $authUser) {

       return $appraisal->owner_id == $authUser->username;

    }


    private function checkIfAuthenticatedUserIsExpectedAuthorOfAppraisalAtThisStatus($appraisal, $authUser) {

        $appraisalCurrentStatus = $appraisal->appraisal_status;
        $requesterUsername = $authUser->username;
        $expectedUsername = "";

        if(
            $appraisalCurrentStatus == ConstAppraisalStatus::PENDING_WORKFLOW_SUBMISSION ||
            $appraisalCurrentStatus == ConstAppraisalStatus::REJECTED_BY_SUPERVISOR ||
            $appraisalCurrentStatus == ConstAppraisalStatus::REJECTED_BY_DEPARTMENT_HEAD ||
            $appraisalCurrentStatus == ConstAppraisalStatus::REJECTED_BY_EXECUTIVE_DIRECTOR
        ){
            $expectedUsername = $appraisal->owner_id;
        }
        else if($appraisalCurrentStatus == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL){
            $expectedUsername = $appraisal->supervisor_id;
        }
        else if($appraisalCurrentStatus == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL){
            $expectedUsername = $appraisal->department_head_id;
        }
        else if($appraisalCurrentStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL){
            $expectedUsername = $appraisal->executive_director_id;
        }

        return $requesterUsername == $expectedUsername;

    }


    private function appraisalApproval(Appraisal $appraisal, $currentAppraisalStatus, $toStatusApproved, $request) {

        try{

            $dateNow = Carbon::now();
            $appraisal->appraisal_status = $toStatusApproved;

            $emailRecipient = "";
            $emailMsg = "";
            $emailSubject = "";

            /*
             * Update the action dates
             * */
            if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL){
                $appraisal->supervisor_action_date = $dateNow;
                $appraisal->supervisor_decision = Globals::$DECISION_APPROVED;
            }
            else if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL){
                $appraisal->department_head_action_date = $dateNow;
                $appraisal->department_head_decision = Globals::$DECISION_APPROVED;
            }
            else if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL){
                $appraisal->executive_director_action_date = $dateNow;
                $appraisal->executive_director_decision = Globals::$DECISION_APPROVED;
            }


            /*
             * Update the submission date
             * */
            if($toStatusApproved == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL){
                $appraisal->supervisor_submission_date = $dateNow;

                $emailRecipient = $appraisal->supervisor_id;
                $emailMsg = "Hello, there is an appraisal submitted that is pending your approval. Thank You";
                $emailSubject = Globals::$MSG_APPRAISAL_PENDING_APPROVAL;
            }
            else if($toStatusApproved == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL){
                $appraisal->department_head_submission_date = $dateNow;
                $emailRecipient = $appraisal->department_head_id;

                $emailMsg = "Hello, there is an appraisal submitted that is pending your approval. Thank You";
                $emailSubject = Globals::$MSG_APPRAISAL_PENDING_APPROVAL;
            }
            else if($toStatusApproved == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL){
                $appraisal->executive_director_submission_date = $dateNow;
                $emailRecipient = $appraisal->executive_director_id;

                $emailMsg = "Hello, there is an appraisal submitted that is pending your approval. Thank You";
                $emailSubject = Globals::$MSG_APPRAISAL_PENDING_APPROVAL;
            }
            else if( $toStatusApproved == ConstAppraisalStatus::COMPLETED_SUCCESSFULLY ){

                $emailRecipient = $appraisal->owner_id;
                $emailMsg = "Hello, Your approval has been successfully completed. Thank You";
                $emailSubject = Globals::$MSG_APPRAISAL_APPROVED;
            }


            /*
             * Save the appraisal
             * */
            if(!$appraisal->save()){
                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON MOVING THE APPRAISAL IN WORK FLOW");
                return $resp;
            }

            /*
             * Get the recipient user by username
             * */
            $recipientUser = User::where('username','=',$emailRecipient)->first();

            if($recipientUser == null){
                $resp = ResponseHandler::successResponse("APPRAISAL FORWARDED BUT FAILED TO GET USER DETAILS FOR NOTIFICATION, USERNAME ".$emailRecipient);
                return $resp;
            }

            $email = $recipientUser->email;

            /*
             * Send notification email
             * */
            $baseResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $emailSubject,$emailMsg);
            $emailMsg = $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ?
                "OPERATION SUCCESSFULLY" : "OPERATION SUCCESSFULLY BUT EMAIL NOT SENT DUE TO ".$baseResp->statusDescription;


            /*
             * Generate PDF Document if the appraisal has been completed
             * */
            $this->generatePdfIfTheAppraisalCompleted($appraisal, $toStatusApproved);


            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = $emailMsg;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "MOVED APPRAISAL IN WORKFLOW ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }


    }


    private function appraisalRejection($appraisal, $currentAppraisalStatus, $toStatusRejected, $remark, $request) {

        try{


            $dateNow = Carbon::now();
            $appraisal->appraisal_status = $toStatusRejected;

            $rejectedBy = "";

            /*
             * Update the action dates
             * */
            if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL){
                $appraisal->supervisor_action_date = $dateNow;
                $appraisal->supervisor_decision = Globals::$DECISION_REJECTED;
                $appraisal->supervisor_remark = $remark;
                $rejectedBy ="SUPERVISOR";
            }
            else if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL){
                $appraisal->department_head_action_date = $dateNow;
                $appraisal->department_head_decision = Globals::$DECISION_REJECTED;
                $appraisal->department_head_remark = $remark;
                $rejectedBy ="HEAD OF DEPARTMENT";
            }
            else if($currentAppraisalStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL){
                $appraisal->executive_director_action_date = $dateNow;
                $appraisal->executive_director_decision = Globals::$DECISION_REJECTED;
                $appraisal->executive_director_remark = $remark;
                $rejectedBy ="EXECUTIVE DIRECTOR";
            }


            /*
             * Save the appraisal
             * */
            if(!$appraisal->save()){
                $resp = ResponseHandler::failureResponse("ERROR OCCURRED ON MOVING THE APPRAISAL IN WORK FLOW");
                return $resp;
            }

            /*
             *
             * */
            $emailSubject = Globals::$MSG_APPRAISAL_REJECTED;
            $emailRecipient = $appraisal->owner_id;
            $emailMsg = "Hello, your approval has been rejected by the ".$rejectedBy.". Thank You";

            /*
             * Get the recipient user by username
             * */
            $recipientUser = User::where('username','=',$emailRecipient)->first();

            if($recipientUser == null){
                $resp = ResponseHandler::successResponse("APPRAISAL REJECTED BUT FAILED TO GET USER DETAILS FOR NOTIFICATION, USERNAME ".$emailRecipient);
                return $resp;
            }

            $email = $recipientUser->email;

            /*
             * Send notification email
             * */
            $baseResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $emailSubject,$emailMsg);
            $emailMsg = $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ?
                "OPERATION SUCCESSFULLY" : "OPERATION SUCCESSFULLY BUT EMAIL NOT SENT DUE TO ".$baseResp->statusDescription;

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = $emailMsg;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "MOVED APPRAISAL IN WORKFLOW ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }

    public static function findAppraisalByAppraisalReference($appraisalReference, $jsonEncode = true){

        $appraisal = Appraisal::with(self::$appraisalRelations)->where('appraisal_reference','=',$appraisalReference)->first();
        return $jsonEncode ? json_encode($appraisal) : $appraisal;

    }

    public function testAppraisal(){

        $appraisalReference = "VXOIGTUB5WXGOGB";
         return self::findAppraisalByAppraisalReference($appraisalReference);

    }

    private function areAllApproversAssigned($appraisal) {

        if($appraisal == null){
            return false;
        }

        return isset($appraisal->supervisor_id) && isset($appraisal->department_head_id) && isset($appraisal->executive_director_id);

    }


    public function testPDF($appraisalReference){

        $baseResponse = PdfHandler::saveToFolder($appraisalReference);

        if($baseResponse->statusCode == Globals::$STATUS_CODE_SUCCESS){
            return json_encode($baseResponse);
        }else{
            return json_encode($baseResponse);
        }

    }

    /**
     * @param Appraisal $appraisal
     * @param $toStatusApproved
     */
    private function generatePdfIfTheAppraisalCompleted(Appraisal $appraisal, $toStatusApproved) {

        try{

            /*
             * This is not the final stage of the appraisal
             * */
            if ($toStatusApproved != ConstAppraisalStatus::COMPLETED_SUCCESSFULLY){
                return;
            }


            /*
             * Generate the PDF and save it to a file
             * */
            $pdfGenResp = PdfHandler::saveToFolder($appraisal->appraisal_reference);


            /*
             * Something went wrong on generating the PDF
             * */
            if ($pdfGenResp->statusCode != Globals::$STATUS_CODE_SUCCESS) {

                $error = new ErrorLog();
                $error->error_message = "Failed to generate PDF for appraisal reference [".$appraisal->appraisal_reference."] with error ".$pdfGenResp->statusDescription;
                $error->error_code = "";
                $error->line_number = "";
                $error->stack_trace = "";
                $error->class_name = __CLASS__;
                $error->method = __METHOD__;
                LogHandler::logError($error);
                return;

            }


            /*
             * We save the download link for the PDF for future use
             * */
            $downloadLink = $pdfGenResp->statusDescription;
            $appraisal->download_link = $downloadLink;
            $appraisal->save();

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

        }

    }


    private function appraisalPeriodRangeDoesnotOverlapExistingAppraisal($username, $periodStartDate, $periodEndDate, $appraisalRef = null){

        $resp = new BaseResponse();

        try{

            $overlapAppraisals = \DB::select('call CheckIfPeriodRangeOverlapsAnExistingAppraisalRange(?,?,?)',array($username,$periodStartDate,$periodEndDate));
            $appraisals = array_map(function ($value) {  return (array)$value;  }, $overlapAppraisals);

            /*
             * No overlaps detected
             * */
            if(count($appraisals) == 0){
                $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
                $resp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
                return $resp;
            }


            /*
             * Get the names of the appraisals that are overlapping
             * */
            $overlaps = "[";
            foreach ($appraisals as $appraisal){

                $existentRef = $appraisal['appraisal_reference'];
                $generatedPdfName = $appraisal['generated_pdf_name'];
                $overlaps = $overlaps . " ".$generatedPdfName.".";


                /*
                 * This is an update so the only guy found is the one we are updating. So this is not an overlap
                 * */
                if(count($appraisals) == 1 && $appraisalRef == $existentRef){
                    $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
                    $resp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
                    return $resp;
                }

            }

            $overlaps = $overlaps . "]";

            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = $overlaps;
            return $resp;

        }catch (\Exception $exception){

            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = Globals::generalError($exception);
            return $resp;
        }

    }

}
