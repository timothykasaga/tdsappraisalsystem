<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\AppraisalCompetenceAssessmentSummary;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;

use App\Http\Resources\Appraisal as AppraisalResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalCompetenceAssessmentSummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'section_e_percentage_score'=>'required|numeric',
                'section_e_weighed_score'=>'required|numeric',
                'section_d_score'=>'required|numeric',
                'section_e_score'=>'required|numeric',
                'appraisal_total_score'=>'required|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','section_e_percentage_score','section_e_weighed_score',
                    'section_d_score','section_e_score','appraisal_total_score'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }

            /*
             * Loop through the data array as save the details
             * */
            $summary = new AppraisalCompetenceAssessmentSummary();
            $summary->appraisal_reference = $appraisalRef;
            $summary->section_e_percentage_score = $request['section_e_percentage_score'];
            $summary->section_e_weighed_score = $request['section_e_weighed_score'];
            $summary->section_d_score = $request['section_d_score'];
            $summary->section_e_score = $request['section_e_score'];
            $summary->appraisal_total_score = $request['appraisal_total_score'];

            $summary->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = "COMPETENCE ASSESSMENT SUMMARY SUCCESSFULLY SAVED]";
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "SAVED COMPETENCE ASSESSMENT SUMMARY ".json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::generalError($exception->getMessage()));
            return json_encode( $resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalCompetenceAssessmentSummary  $appraisalCompetenceAssessmentSummary
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalCompetenceAssessmentSummary $appraisalCompetenceAssessmentSummary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalCompetenceAssessmentSummary  $appraisalCompetenceAssessmentSummary
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalCompetenceAssessmentSummary $appraisalCompetenceAssessmentSummary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalCompetenceAssessmentSummary  $assessmentSummary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalCompetenceAssessmentSummary $assessmentSummary)
    {

        $resp = [];

        try{

            $validator = Validator::make($request->all(), [
                'appraisal_reference' => 'required|exists:appraisals,appraisal_reference',
                'section_e_percentage_score'=>'required|numeric',
                'section_e_weighed_score'=>'required|numeric',
                'section_d_score'=>'required|numeric',
                'section_e_score'=>'required|numeric',
                'appraisal_total_score'=>'required|numeric',
            ]);


            if ($validator->fails()) {

                $validatedFields = ['appraisal_reference','section_e_percentage_score','section_e_weighed_score',
                    'section_d_score','section_e_score','appraisal_total_score'];
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);
                $resp = ResponseHandler::failureResponse($firstValidationError);

                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF COMPETENCE ASSESSMENT SUMMARY DUE TO ERROR ".json_encode($resp);
                $trail->username = Auth::user()->username;
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);

                return json_encode($resp);

            }

            /*
             * Get the appraisal
             * */
            $appraisalRef = $request['appraisal_reference'];
            $appraisal = Appraisal::where('appraisal_reference','=', $appraisalRef)->first();


            /*
             * Error occurred on getting the appraisal using the reference
             * */
            if($appraisal == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_APPRAISAL." [".$appraisalRef."]");
                return json_encode($resp);
            }

            /*
             * Check if the request is coming from the owner of the application
             * */
            if(Auth::user()->username != $appraisal->owner_id && Auth::user()->username != $appraisal->supervisor_id  && !Globals::$DEBUG_MODE){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_PERMISSION_NOT_ALLOWED);
                return json_encode($resp);
            }


            /*
             * update the details
             * */
            $assessmentSummary->section_e_percentage_score = $request['section_e_percentage_score'];
            $assessmentSummary->section_e_weighed_score = $request['section_e_weighed_score'];
            $assessmentSummary->section_d_score = $request['section_d_score'];
            $assessmentSummary->section_e_score = $request['section_e_score'];
            $assessmentSummary->appraisal_total_score = $request['appraisal_total_score'];

            $assessmentSummary->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = AppraisalController::findAppraisalByAppraisalReference($appraisal->appraisal_reference);
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED COMPETENCE ASSESSMENT SUMMARY " . json_encode($appraisal);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);

        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalCompetenceAssessmentSummary  $appraisalCompetenceAssessmentSummary
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalCompetenceAssessmentSummary $appraisalCompetenceAssessmentSummary)
    {
        //
    }
}
