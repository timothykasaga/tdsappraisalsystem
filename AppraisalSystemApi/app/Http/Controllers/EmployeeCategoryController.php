<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\EmployeeCategory;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use App\Helpers\Security;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\EmployeeCategory as EmployeeCategoryResource;

class EmployeeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {

        /*
        * declare the response object
        * */
        $resp = [];

        try{

            /*
             * get the categories
             * */
            $employeeCategories = EmployeeCategory::all();


            /*
             * build success response
             * */
            $resp['data'] = $employeeCategories;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{


            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'category'=>'required|min:2',
                'org_code'=>'required|exists:organizations,org_code',
                'created_by'=>'required'
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['category','category_code','org_code','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }



            /*
             * everything is fine now
             * */



            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $employeeCategory = new EmployeeCategory();
            $employeeCategory->category = strtoupper($request['category']);
            $employeeCategory->category_code = Security::generateTableIdentifier(Security::TAB_EMPLOYEE_CATEGORY);
            $employeeCategory->org_code = $request['org_code'];
            $employeeCategory->created_by = $request['created_by'];

            /*
             * save the category details
             * */
            $employeeCategory->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED EMPLOYEE CATEGORY ".json_encode($employeeCategory);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeCategory $employeeCategory)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new EmployeeCategoryResource($employeeCategory);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeCategory $employeeCategory)
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */
            $validator = Validator::make($request->all(), [
                'category'=>'required|min:2',
                'category_code'=> 'required|exists:employee_categories,category_code',
                'org_code'=> 'exists:organizations,org_code',
                'created_by'=>'required',//add constraint for an existent user
                'updated_by'=>'required',//add constraint for an existent user
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['category','category_code','org_code','created_by','updated_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$UPDATE;
                $trail->action = "FAILED UPDATE OF EMPLOYEE CATEGORY DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['updated_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * update employee category details
             * */
            $employeeCategory->category = $request['category'];
            $employeeCategory->org_code = $request['org_code'];
            $employeeCategory->created_by = $request['created_by'];

            /*
             * save the updated details
             * */
            $employeeCategory->save();

            /*
             * in this approach of route-model-binding, the model that matches is injected so we just return it
             * */
            $resp['data'] = new EmployeeCategoryResource($employeeCategory);

            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$UPDATE;
            $trail->action = "UPDATED EMPLOYEE CATEGORY " . json_encode($employeeCategory);
            $trail->username = $request['updated_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\EmployeeCategory $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeCategory $employeeCategory)
    {

        try{

            /*
            * validate using Laravel's validator class
            * */
            $validator = Validator::make($request->all(), [
                'deleted_by'=>'required|exists:users,username',
            ]);

            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['deleted_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);


                /*
                 * Log audit trail
                 * */
                $trail = new AuditTrail();
                $trail->action_category = AuditTrailActionCategories::$DELETE;
                $trail->action = "FAILED CATEGORY DELETION DUE TO ERROR ".json_encode($resp);
                $trail->username = $request['deleted_by'];
                $trail->ip_address = $request->ip();
                $trail->api_username = "";
                LogHandler::logAuditTrail($trail);
                /*
                 * End of log audit trail
                 * */

                return json_encode($resp);

            }

            /*
             * Attempt to delete
             * */
            if(!$employeeCategory->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE EMPLOYEE CATEGORY"));
            }


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED EMPLOYEE CATEGORY " . json_encode($employeeCategory);
            $trail->username = $request['deleted_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * Return success response
             * */
            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }

    }

}
