<?php

namespace App\Http\Controllers;

use App\AppraisalCompetence;
use App\AppraisalCompetenceCategory;
use App\AuditTrail;
use App\Helpers\AuditTrailActionCategories;
use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppraisalCompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {

        /*
         * declare the response object
         * */
        $resp = [];

        try{

            $user = Auth::user();

            /*
             * Failed to get authenticated user
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);

            }

            /*
             * get data
             * */
            $userOrgCode = $user->org_code;
            $userEmpCategoryCode= $user->category_code;

            /*
             * Get competence categories that apply to the user
             * */
            $competenceCategoryIds = AppraisalCompetenceCategory::where('org_code','=',$userOrgCode)->
            where('employee_category_code', '=', $userEmpCategoryCode)->get(['id']);

            $ids = $this->getIdsNumberFromIdFieldList($competenceCategoryIds);

            /*
             * Get all competences for these competence categories above
             * */
            $competences = AppraisalCompetence::whereIn('appraisal_competence_category_id', $ids)->
            with('appraisalCompetenceCategory')->get();


            /*
             * build success response
             * */
            $resp['data'] = $competences;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'competence'=>'required|min:2',
                'appraisal_competence_category_id'=>'required|exists:appraisal_competence_categories,id',
                'rank'=>'required|numeric','rating'=>'required|numeric',
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['appraisal_competence_category_id','competence','rank','rating','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $competence = new AppraisalCompetence();
            $competence->appraisal_competence_category_id = $request['appraisal_competence_category_id'];
            $competence->competence = $request['competence'];
            $competence->rank = $request['rank'];
            $competence->created_by = Auth::user()->username;
            $competence->rating = $request['rating'];

            /*
             * save the details
             * */
            $competence->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;


            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "CREATED APPRAISAL COMPETENCE ".json_encode($competence);
            $trail->username = $request['created_by'];
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */


            /*
             * return the success response
             * */
            return json_encode($resp);


        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppraisalCompetence  $appraisalCompetence
     * @return \Illuminate\Http\Response
     */
    public function show(AppraisalCompetence $appraisalCompetence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppraisalCompetence  $appraisalCompetence
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraisalCompetence $appraisalCompetence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppraisalCompetence  $appraisalCompetence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraisalCompetence $appraisalCompetence)
    {

        //declare the response object
        $resp = [];

        try{

            /*
             * validate using Laravel's validator class
             * */

            $validator = Validator::make($request->all(), [
                'competence'=>'required|min:2',
                'appraisal_competence_category_id'=>'required|exists:appraisal_competence_categories,id',
                'rank'=>'required|numeric','rating'=>'required|numeric',
            ]);


            /*
             * validation has failed so, we return the validation errors
             * */
            if ($validator->fails()) {

                /*
                 * fields validated
                 * */
                $validatedFields = ['appraisal_competence_category_id','competence','rank','rating','created_by'];

                /*
                 * get the first validation error
                 * */
                $firstValidationError = ResponseHandler::getFirstValidationError($validator, $validatedFields);

                /*
                 * return the validation error
                 * */
                $resp = ResponseHandler::failureResponse($firstValidationError);
                return json_encode($resp);

            }

            /*
             * I decide to get these fields manually just to sure but you can as well saving using the request object
             * */
            $appraisalCompetence->appraisal_competence_category_id = $request['appraisal_competence_category_id'];
            $appraisalCompetence->competence = $request['competence'];
            $appraisalCompetence->rank = $request['rank'];
            $appraisalCompetence->rating = $request['rating'];
            $appraisalCompetence->save();

            /*
             * compose success response
             * */
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$CREATE;
            $trail->action = "UPDATED APPRAISAL COMPETENCE ".json_encode($appraisalCompetence);
            $trail->username = Auth::user()->username;
            $trail->ip_address = $request->ip();
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);
            /*
             * End of log audit trail
             * */

            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppraisalCompetence  $appraisalCompetence
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraisalCompetence $appraisalCompetence)
    {

        try{

            if(!$appraisalCompetence->delete()){
                return json_encode(ResponseHandler::failureResponse("ERROR OCCURRED ON DELETING THE COMPETENCE"));
            }

            /*
             * Log audit trail
             * */
            $trail = new AuditTrail();
            $trail->action_category = AuditTrailActionCategories::$DELETE;
            $trail->action = "DELETED COMPETENCE " . json_encode($appraisalCompetence);
            $trail->username = Auth::user()->username;
            $trail->ip_address = "";
            $trail->api_username = "";
            LogHandler::logAuditTrail($trail);

            $resp = ResponseHandler::successResponse(Globals::$STATUS_DESC_SUCCESS);
            return json_encode($resp);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());
            return json_encode($resp);
        }

    }

    private function getIdsNumberFromIdFieldList($competenceCategoryIds) {

        $ids = [];
        foreach ($competenceCategoryIds as $categoryId){
            $ids[] = $categoryId['id'];
        }
        return $ids;
    }


    public function allCompetenceCategoryCompetences($id)
    {

        $resp = [];

        try{

            $user = Auth::user();

            /*
             * Failed to get authenticated user
             * */
            if($user == null){
                $resp = ResponseHandler::failureResponse(Globals::$MSG_FAILED_TO_GET_LOGGED_IN_USER);
                return json_encode($resp);

            }

            /*
             * Get all competences for these competence category above
             * */
            $competences = AppraisalCompetence::with('appraisalCompetenceCategory')
                ->where('appraisal_competence_category_id','=', $id)
                ->get();


            /*
             * build success response
             * */
            $resp['data'] = $competences;
            $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
            $resp['statusDescription'] = Globals::$STATUS_DESC_SUCCESS;

            return json_encode($resp);

        }catch (\Exception $exception){

            /*
             * log the error
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * get the failure response
             * */
            $resp = ResponseHandler::failureResponse(Globals::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage());

            /*
             * return the response
             * */
            return json_encode($resp);

        }


    }


}
