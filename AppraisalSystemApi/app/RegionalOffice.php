<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionalOffice extends Model
{


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'regional_office_code';
    }

    public function organization(){

        return $this->belongsTo('App\Organization','org_code','org_code');

    }

    public function users(){

        return $this->hasMany('App\Users','regional_office_code','regional_office_code');

    }

}
