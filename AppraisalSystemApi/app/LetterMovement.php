<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterMovement extends Model
{

    public function letter(){
        return $this->belongsTo('App\Letter','letter_code','letter_code');
    }

}
