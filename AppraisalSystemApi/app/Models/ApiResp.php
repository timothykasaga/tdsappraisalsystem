<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/16/2019
 * Time: 00:54
 */


namespace App\Models;


class ApiResp {

    public $statusCode = "";
    public $statusDescription = "";
    public $result;

}