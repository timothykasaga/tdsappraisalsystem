<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/16/2019
 * Time: 23:19
 */


namespace App\Models;


class ApiRegionalOffice {

    public $name = "";
    public $orgCode = "";
    public $regionalOfficeCode = "";
    public $email = "";
    public $location = "";
    public $contactPersonName = "";
    public $contactPersonContact = "";
    public $description = "";
    public $createdBy = "";

}