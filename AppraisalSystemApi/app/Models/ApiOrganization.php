<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/16/2019
 * Time: 21:30
 */


namespace App\Models;


class ApiOrganization {

    public $name = "";
    public $orgCode = "";
    public $email = "";
    public $location = "";
    public $contactPersonName = "";
    public $contactPersonContact = "";
    public $description = "";
    public $createdBy = "";
    public $executiveDirector = "";

}