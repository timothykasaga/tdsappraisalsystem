<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/17/2019
 * Time: 00:05
 */


namespace App\Models;


class ApiRoleCode {

    public $roleName = "";
    public $roleCode = "";
    public $orgCode = "";
    public $defaultPage = "";
    public $active = "";
    public $createdBy = "";

}