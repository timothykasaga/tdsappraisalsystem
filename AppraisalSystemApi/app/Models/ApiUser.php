<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/17/2019
 * Time: 00:05
 */


namespace App\Models;


class ApiUser {

    public $id = "";
    public $username = "";
    public $firstName = "";
    public $lastName = "";
    public $otherName = "";
    public $email = "";
    public $phone = "";
    public $createdBy = "";
    public $orgCode = "";
    public $orgName = "";
    public $orgEdUsername = "";
    public $roleCode = "";
    public $roleName = "";
    public $letterMovementRole = "";
    public $departmentCode = "";
    public $departmentName = "";
    public $departmentHeadUsername = "";
    public $departmentHeadFullName = "";
    public $regionalOfficeCode = "";
    public $regionalOfficeName = "";
    public $categoryCode = "";
    public $category = "";
    public $createdAt = "";

    public $fullName = "";

    public $staffNumber = "";
    public $designation = "";
    public $dateOfBirth = "";
    public $contractStartDate = "";
    public $contractExpiryDate = "";

}