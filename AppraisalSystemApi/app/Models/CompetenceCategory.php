<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/26/2019
 * Time: 17:43
 */


namespace App\Models;


class CompetenceCategory {

    public $id = "";
    public $category = "";
    public $maximumRating = "";
    public $competences = [];

}