<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/25/2019
 * Time: 14:45
 */


namespace App\Models;


class ContractAboutToExpire {

    public $username = "";
    public $firstName = "";
    public $lastName = "";
    public $fullName = "";
    public $contractReference = "";
    public $startDate = "";
    public $expiryDate = "";
    public $monthsLeft = "";
    public $daysLeft = "";
    public $periodGroup = "";

    /**
     * @return string
     */
    public function getFullName() {
        return $this->firstName . ' ' .$this->lastName;
    }



}