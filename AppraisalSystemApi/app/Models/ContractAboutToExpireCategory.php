<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/25/2019
 * Time: 14:49
 */


namespace App\Models;


class ContractAboutToExpireCategory {

    public $category = "";
    public $contracts = [];

}