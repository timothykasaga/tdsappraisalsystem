<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/26/2019
 * Time: 16:58
 */


namespace App\Models;


class CompetenceAssessmentScore {

    public $assessmentId;
    public $competenceId;
    public $competence;
    public $rank;
    public $maxRating;
    public $appraiseeRating;
    public $appraiserRating;
    public $agreedRating;

}