<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAcademicBackground extends Model
{

    public function user(){
        return $this->belongsTo('App\User','username','username');
    }

}
