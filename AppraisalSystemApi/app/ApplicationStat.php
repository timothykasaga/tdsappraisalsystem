<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationStat
{

    public $countAppraisals  = "";
    public $countAppraisalsNew  = "";
    public $countUsers  = "";
    public $countUsersNew  = "";
    public $countRegionalOffice  = "";
    public $countRegionalOfficeNew  = "";
    public $countDepartments  = "";
    public $countDepartmentsNew  = "";

}
