<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalCompetenceCategory extends Model
{

    public function appraisalCompetences(){
        return $this->hasMany('App\AppraisalCompetence');
    }

}
