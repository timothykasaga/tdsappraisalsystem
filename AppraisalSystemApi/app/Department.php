<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'department_code';
    }


    public function organization(){

        return $this->belongsTo('App\Organization','org_code','org_code');

    }

    public function users(){

        return $this->hasMany('App\Users','department_code','department_code');

    }

}
