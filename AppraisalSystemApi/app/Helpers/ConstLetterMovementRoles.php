<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/25/2019
 * Time: 11:10
 */


namespace App\Helpers;


class ConstLetterMovementRoles {

    const REGISTRY = "REGISTRY";
    const RECEPTION = "RECEPTION";
    const NORMAL_USER = "NORMAL";
    const EXECUTIVE_DIRECTOR = "ED";
    const ASSIGNED = "ASSIGNED";

    public static function validWorkflowRole($workFlowRole) {

        $roles = [self::REGISTRY, self::RECEPTION, self::EXECUTIVE_DIRECTOR, self::NORMAL_USER, self::ASSIGNED];
        return in_array($workFlowRole, $roles);

    }

}