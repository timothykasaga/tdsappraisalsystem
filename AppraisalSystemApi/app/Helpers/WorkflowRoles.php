<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/25/2019
 * Time: 10:45
 */


namespace App\Helpers;


class WorkflowRoles {

    const OWNED = "owner";
    const SUPERVISOR = "supervisor";
    const HOD = "hod";
    const DIRECTOR = "director";
    const ALL = "all";

    public static function validWorkflowRole($workflowRole) {

        $validRoles = [self::OWNED, self::SUPERVISOR, self::HOD, self::DIRECTOR, self::ALL];
        return in_array($workflowRole, $validRoles);

    }

}