<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/14/2019
 * Time: 13:05
 */


namespace App\Helpers;


use App\Appraisal;
use App\Department;
use App\EmployeeCategory;
use App\Letter;
use App\Organization;
use App\RegionalOffice;
use App\RoleCode;
use Carbon\Carbon;
use DateTime;

class Security {

    const TAB_ORG = 'organization';
    const TAB_OFFICE = 'office';
    const TAB_ROLE = 'role';
    const TAB_DEPARTMENT = 'department';
    const TAB_EMPLOYEE_CATEGORY = 'emp-category';
    const TAB_LETTER = 'letter';

    public static function randomPassword($length = 6){
        return strtoupper(str_random($length));
    }

    public static function createDocumentName($firstName, $lastName, $startDate, $endDate, $prefix = 0) {

        $formattedStartDate = Carbon::createFromFormat('Y-m-d',$startDate)->format('F-d-Y');
        $formattedEndDate = Carbon::createFromFormat('Y-m-d',$endDate)->format('F-d-Y');

        $prefixValue = $prefix == 0 ? '' : '-'.$prefix;
        $generatedName = $firstName .'-'.$lastName.'-'.'Appraisal'.'-'.$formattedStartDate.'-'.$formattedEndDate.$prefixValue;//.'.pdf';

        $appraisal = Appraisal::where('generated_pdf_name','=',$generatedName)->first();
        if($appraisal == null){
            return $generatedName;
        }

        return self::createDocumentName($firstName,$lastName,$startDate,$endDate,$prefix+1);

    }

    public static function getCreateReference() {

        $reference = strtoupper(self::randomPassword(15));
        $appraisal = Appraisal::where('appraisal_reference','=',$reference)->first();
        return $appraisal == null ? $reference : self::getCreateReference();

    }

    public static function generateTableIdentifier($tableName){

        $prefix = "";
        $maxId = 0;

        try{

            if($tableName == self::TAB_ORG){

                $prefix = "ORG_";
                $maxId = Organization::max('id');

            }else if($tableName ==  self::TAB_OFFICE){

                $prefix = "OFF_";
                $maxId = RegionalOffice::max('id');

            }else if($tableName ==  self::TAB_ROLE){

                $prefix = "ROLE_";
                $maxId = RoleCode::max('id');

            }else if($tableName ==  self::TAB_DEPARTMENT){

                $prefix = "DEPT_";
                $maxId = Department::max('id');

            }else if($tableName ==  self::TAB_EMPLOYEE_CATEGORY){

                $prefix = "EMP_CAT_";
                $maxId = EmployeeCategory::max('id');

            }else if($tableName ==  self::TAB_LETTER){

                $prefix = "LT_";
                $maxId = Letter::max('id');

            }

            return is_numeric($maxId) ? $prefix . ($maxId + 1) : $prefix . random_int(100, 300000);

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception, __CLASS__, __METHOD__);
            return 'ERROR_' . $prefix . $maxId;

        }

    }


}