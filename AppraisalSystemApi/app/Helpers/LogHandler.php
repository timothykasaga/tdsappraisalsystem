<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/13/2019
 * Time: 15:58
 */


namespace App\Helpers;


use App\AuditTrail;
use App\ErrorLog;

class LogHandler {


    public static function logError(ErrorLog $errorLog) {

        try{

            //save the error details, about we create a thread for this.
            //and shouldn't these errors be logged to an error queue for performance
            $errorLog->save();

        }catch (\Exception $exception){

            //what happens if we fail to log

        }


    }

    public static function logExceptionError(\Exception $exception, $className, $methodName) {

        try{

            /* build the error object */
            $errorLog = new ErrorLog();
            $errorLog->error_message = $exception->getMessage();
            $errorLog->error_code = $exception->getCode();
            $errorLog->line_number = $exception->getLine();
            $errorLog->stack_trace = $exception->getTraceAsString();
            $errorLog->class_name = $className;
            $errorLog->method = $methodName;

            /* log the error details*/
            LogHandler::logError($errorLog);


        }catch (\Exception $exception){

            //what happens if we fail to log

        }

    }

    public static function logAuditTrail(AuditTrail $trail) {

        try{

            $trail->save();

        }catch (\Exception $exception){

        }

    }


}