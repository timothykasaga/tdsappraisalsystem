<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/16/2019
 * Time: 15:28
 */


namespace app\Helpers;


class EndPoints {


    public static $BASE_URL = 'http:localhost:8001/api';
    public static $BASE_URL_USER_MANAGEMENT = 'http:localhost:7001/api';

    /*
     * End points for users
     * */
    public static $USERS_LOGIN = '/user/login';
    public static $USERS_RESET_PASSWORD = '/user/reset-password';
    public static $USERS_CHANGE_PASSWORD = '/user/change-password';
    public static $USERS_CHANGE_DEFAULT_PASSWORD = '/user/password/change-default';
    public static $USERS_STORE = '/user';
    public static $USERS_UPDATE = '/user';
    public static $USERS_DELETE = '/user';
    public static $USERS_ALL = '/users';
    public static $USERS_SHOW = '/user';
    public static $USERS_ALL_BY_ROLE_CODE = '/users';
    public static $USERS_GET_AUTHENTICATED_USER = '/users/auth-user';

    public static $USERS_UPDATE_PROFILE_BY_USER = '/user/owner-profile-update';

    public static $USERS_USER_ACADEMIC_BG_SAVE = '/user-academic-background';
    public static $USERS_USER_ACADEMIC_BG_UPDATE = '/user-academic-background';
    public static $USERS_USER_ACADEMIC_BG_ALL = '/user-academic-backgrounds';
    public static $USERS_USER_ACADEMIC_BG_DELETE = '/user-academic-background';

    public static $USER_CONTRACT_SAVE = '/user-contract';
    public static $USER_CONTRACT_UPDATE = '/user-contract';
    public static $USER_CONTRACT_ALL = '/user-contracts';
    public static $USER_CONTRACT_DELETE = '/user-contract';

    /*
     * End points for organizations
     * */
    public static $ORGANIZATIONS_STORE = '/organization';
    public static $ORGANIZATIONS_UPDATE = '/organization';
    public static $ORGANIZATIONS_DELETE = '/organization';
    public static $ORGANIZATIONS_ALL = '/organizations';

    /*
     * End points for regional offices
     * */
    public static $REGIONAL_OFFICES_STORE = '/regional-office';
    public static $REGIONAL_OFFICES_UPDATE = '/regional-office';
    public static $REGIONAL_OFFICES_DELETE = '/regional-office';
    public static $REGIONAL_OFFICES_ALL = '/regional-offices';

    /*
     * End points for departments
     * */
    public static $DEPARTMENTS_STORE = '/department';
    public static $DEPARTMENTS_UPDATE = '/department';
    public static $DEPARTMENTS_DELETE = '/department';
    public static $DEPARTMENTS_ALL = '/departments';

    /*
    * End points for role codes
    * */
    public static $ROLE_CODES_STORE = '/role-code';
    public static $ROLE_CODES_UPDATE = '/role-code';
    public static $ROLE_CODES_DELETE = '/role-code';
    public static $ROLE_CODES_ALL = '/role-codes';

    /*
   * End points for employee categories
   * */
    public static $EMPLOYEE_CATEGORIES_STORE = '/employee-category';
    public static $EMPLOYEE_CATEGORIES_UPDATE = '/employee-category';
    public static $EMPLOYEE_CATEGORIES_DELETE = '/employee-category';
    public static $EMPLOYEE_CATEGORIES_ALL = '/employee-categories';


    /*
     * Application Stats
     * */
    public static $APPLICATION_STATS_ALL = '/app-stats';


    /*
    * End points for appraisal
    * */
    public static $APPRAISAL_STRATEGIC_OBJECTIVES_ALL = '/appraisal-strategic-objectives';
    public static $APPRAISAL_STRATEGIC_OBJECTIVES_SAVE = '/appraisal-strategic-objective';
    public static $APPRAISAL_STRATEGIC_OBJECTIVES_UPDATE = '/appraisal-strategic-objective';
    public static $APPRAISAL_STRATEGIC_OBJECTIVES_DELETE = '/appraisal-strategic-objective';


    public static $APPRAISAL_COMPETENCE_CATEGORIES_ALL = '/appraisal-competence-categories';
    public static $APPRAISAL_SAVE_APPRAISAL = '/appraisal';
    public static $APPRAISAL_ALL = '/appraisals';
    public static $APPRAISAL_GET = '/appraisal';
    public static $APPRAISAL_TOP_3 = '/appraisals/latest';


    public static $APPRAISAL_PERSONAL_DETAILS_UPDATE = '/appraisal-personal-detail';

    public static $APPRAISAL_ACADEMIC_BG_SAVE = '/appraisal-academic-backgrounds/save';
    public static $APPRAISAL_ACADEMIC_BG_UPDATE = '/appraisal-academic-backgrounds/update';

    public static $APPRAISAL_KEY_DUTIES_SAVE = '/appraisal-key-duties/save';
    public static $APPRAISAL_KEY_DUTIES_UPDATE = '/appraisal-key-duties/update';

    public static $APPRAISAL_ASSIGNMENTS_SAVE = '/appraisal-assignments/save';
    public static $APPRAISAL_ASSIGNMENTS_UPDATE = '/appraisal-assignments/update';

    public static $APPRAISAL_ASSIGNMENTS_SCORES_SAVE = '/appraisal-assignment-score';
    public static $APPRAISAL_ASSIGNMENTS_SCORES_UPDATE = '/appraisal-assignment-score';

    public static $APPRAISAL_ASSIGNMENTS_SUMMARY_SAVE = '/appraisal-assignment-summary';
    public static $APPRAISAL_ASSIGNMENTS_SUMMARY_UPDATE = '/appraisal-assignment-summary';

    public static $APPRAISAL_ADDITIONAL_ASSIGNMENTS_SAVE = '/appraisal-additional-assignments/save';
    public static $APPRAISAL_ADDITIONAL_ASSIGNMENTS_UPDATE = '/appraisal-additional-assignments/update';

    public static $APPRAISAL_ADDITIONAL_ASSIGNMENTS_SCORES_SAVE = '/appraisal-additional-assignment-score';
    public static $APPRAISAL_ADDITIONAL_ASSIGNMENTS_SCORES_UPDATE = '/appraisal-additional-assignment-score';

    public static $APPRAISAL_PERFORMANCE_GAPS_SAVE = '/appraisal-performance-gaps/save';
    public static $APPRAISAL_PERFORMANCE_GAPS_UPDATE = '/appraisal-performance-gaps/update';

    public static $APPRAISAL_PERFORMANCE_CHALLENGES_SAVE = '/appraisal-performance-challenges/save';
    public static $APPRAISAL_PERFORMANCE_CHALLENGES_UPDATE = '/appraisal-performance-challenges/update';

    public static $APPRAISAL_PERFORMANCE_SUMMARY_SAVE = '/appraisal-train-summary';
    public static $APPRAISAL_PERFORMANCE_SUMMARY_UPDATE = '/appraisal-train-summary';

    public static $APPRAISAL_COMPETENCE_ASSESSMENT_SAVE = '/appraisal-competence-assessments/save';
    public static $APPRAISAL_COMPETENCE_ASSESSMENT_UPDATE = '/appraisal-competence-assessments/update';

    public static $APPRAISAL_COMPETENCE_ASSESSMENT_SCORES_SAVE = '/appraisal-competence-assessment-score';
    public static $APPRAISAL_COMPETENCE_ASSESSMENT_SCORES_UPDATE = '/appraisal-competence-assessment-score';
    public static $APPRAISAL_COMPETENCE_ASSESSMENT_SUMMARY_SAVE = '/appraisal-competence-assessment-summary';
    public static $APPRAISAL_COMPETENCE_ASSESSMENT_SUMMARY_UPDATE = '/appraisal-competence-assessment-summary';

    public static $APPRAISAL_WORK_PLAN_SAVE = '/appraisal-work-plans/save';
    public static $APPRAISAL_WORK_PLAN_UPDATE = '/appraisal-work-plans/update';

    public static $APPRAISAL_APPRAISER_COMMENT_SAVE = '/appraisal-appraiser-comment';
    public static $APPRAISAL_APPRAISER_COMMENT_UPDATE = '/appraisal-appraiser-comment';

    public static $APPRAISAL_STRENGTH_AND_WEAKNESS_SAVE = '/appraisal-strengths-and-weakness';
    public static $APPRAISAL_STRENGTH_AND_WEAKNESS_UPDATE = '/appraisal-strengths-and-weakness';

    public static $APPRAISAL_APPRAISER_RECOMMENDATION_SAVE = '/appraisal-appraiser-recommendation';
    public static $APPRAISAL_APPRAISER_RECOMMENDATION_UPDATE = '/appraisal-appraiser-recommendation';

    public static $APPRAISAL_SUPERVISOR_DECLARATION_SAVE = '/appraisal-supervisor-declaration';
    public static $APPRAISAL_SUPERVISOR_DECLARATION_UPDATE = '/appraisal-supervisor-declaration';

    public static $APPRAISAL_HOD_COMMENT_SAVE = '/appraisal-hod-comment';
    public static $APPRAISAL_HOD_COMMENT_UPDATE = '/appraisal-hod-comment';

    public static $APPRAISAL_APPRAISEE_REMARK_SAVE = '/appraisal-appraisee-remark';
    public static $APPRAISAL_APPRAISEE_REMARK_UPDATE = '/appraisal-appraisee-remark';

    public static $APPRAISAL_DIRECTOR_COMMENT_SAVE = '/appraisal-director-comment';
    public static $APPRAISAL_DIRECTOR_COMMENT_UPDATE = '/appraisal-director-comment';

    public static $APPRAISAL_WORKFLOW_START = '/appraisal/workflow-start';
    public static $APPRAISAL_WORKFLOW_MOVE = '/appraisal/workflow-move';

    public static $APPRAISAL_UPDATE_APPROVERS = '/appraisal/update-approvers';


    public static $COMPETENCE_CATEGORY_ALL = '/admin/appraisal-competence-categories';
    public static $COMPETENCE_CATEGORY_SAVE = '/appraisal-competence-category';
    public static $COMPETENCE_CATEGORY_UPDATE = '/appraisal-competence-category';
    public static $COMPETENCE_CATEGORY_DELETE = '/appraisal-competence-category';

    public static $COMPETENCES_FOR_CATEGORY_ID = '/appraisal-competences';
    public static $COMPETENCES_SAVE = '/appraisal-competence';
    public static $COMPETENCES_UPDATE = '/appraisal-competence';
    public static $COMPETENCES_DELETE = '/appraisal-competence';

    public static $ERROR_LOG_SAVE = '/error-log';

    public static $USER_MANAGEMENT_LOGIN_PAGE =  'http://127.0.0.1:7000/';
    public static $USER_MANAGEMENT_LOGOUT_LINK =  'http://127.0.0.1:7000/admin/signout';



    public static $LETTER_ALL = '/letters';
    public static $LETTER_SAVE = '/letter';
    public static $LETTER_UPDATE = '/letter';
    public static $LETTER_SHOW = '/letter';
    public static $LETTER_DELETE = '/letter';

    public static $LETTER_WORK_FLOW_START = '/letter/workflow-start';
    public static $LETTER_WORK_FLOW_MOVE = '/letter/workflow-move';

    public static $LETTER_MOVEMENT_ALL = '/letter-movements';
    public static $LETTER_MOVEMENT_SAVE = '/letter-movement';
    public static $LETTER_MOVEMENT_UPDATE = '/letter-movement';
    public static $LETTER_MOVEMENT_SHOW = '/letter-movement';
    public static $LETTER_MOVEMENT_DELETE = '/letter-movement';

}