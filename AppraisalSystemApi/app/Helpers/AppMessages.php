<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/13/2019
 * Time: 15:52
 */


namespace App\Helpers;


class AppMessages {

    public static $ERR_ORG_CODE_EXISTS = "Organization Code Already Exists in the System";

}