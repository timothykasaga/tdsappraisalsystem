<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/8/2019
 * Time: 13:45
 */


namespace App\Helpers;


use App\Models\ApiResp;
use Illuminate\Support\Facades\Cookie;

class DataLoader {


    public static function getAuthenticatedUser($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$USERS_GET_AUTHENTICATED_USER;
            $resp = ApiHandler::makeGetRequest($action, true,$token, null, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != Globals::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != Globals::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = DataFormatter::getApiUser($apiResult['data']);
            $baseResp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = Globals::generalError($exception);
            return $baseResp;
        }

    }

    public static function findLetterMovementFlowUsersByLetterMovementRole($letterMovementRole, $token) {

        $baseResp = self::allUsersByRoleCode($letterMovementRole, $token);
        return $baseResp->statusCode == Globals::$STATUS_CODE_SUCCESS ? $baseResp->result : [];
    }

    public static function allUsersByRoleCode($letterMovementRole, $token){
        $baseResp = new ApiResp();

        try{

            $identifier = $letterMovementRole .'letter-movement/'.$letterMovementRole;
            $resp = ApiHandler::makeGetRequest(EndPoints::$USERS_ALL_BY_ROLE_CODE, true, $token, $identifier, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * We didn't get a response from API
             * */
            if($resp->statusCode != Globals::$STATUS_CODE_SUCCESS){
                /*
                 * Return error
                 * */
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp->statusDescription;
            }

            /*
             * We got a response from the API
             * */
            $apiResp = json_decode($resp->result, true);

            /*
             * Get statusCode, statusDescription
             * */
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Check if got a success on fetching the data from the API
             * */
            if($statusCode != Globals::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            /*
             * We got the data from the API
             * */
            $data = $apiResp['data'];

            /*
             * Format data
             * */
            $users = DataFormatter::formatUsers($data);

            $baseResp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            $baseResp->result = $users;

            return $baseResp;


        }catch (\Exception $exception){
            $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = Globals::generalError($exception->getMessage());
            return $baseResp;
        }

    }



    public static function getUserByUsername($username, $token) {

        $baseResp = new ApiResp();

        try{

            $resp = ApiHandler::makeGetRequest(EndPoints::$USERS_SHOW, true, $token,$username,EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * We didn't get a response from API
             * */
            if($resp->statusCode != Globals::$STATUS_CODE_SUCCESS){
                /*
                 * Return error
                 * */
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp->statusDescription;
            }

            /*
             * We got a response from the API
             * */
            $apiResp = json_decode($resp->result, true);

            /*
             * Get statusCode, statusDescription
             * */
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Check if got a success on fetching the data from the API
             * */
            if($statusCode != Globals::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            /*
             * We got the data from the API
             * */
            $data = $apiResp['data'];

            /*
             * Format data
             * */
            $user = DataFormatter::getApiUserWithNoRelationsShips($data);

            $baseResp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            $baseResp->result = $user;

            return $baseResp;


        }catch (\Exception $exception){
            $baseResp->statusCode = Globals::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = Globals::generalError($exception->getMessage());
            return $baseResp;
        }

    }




}