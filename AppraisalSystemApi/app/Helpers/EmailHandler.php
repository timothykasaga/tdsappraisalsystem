<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/17/2019
 * Time: 11:40
 */


namespace App\Helpers;


use App\Appraisal;
use App\BaseResponse;
use App\Letter;
use App\Mail\AccountCreationMail;
use App\User;
use PHPMailer\PHPMailer\PHPMailer;

//use Illuminate\Support\Facades\Mail;


class EmailHandler {


    public static function sendPlainTextEmail($mailTo, $content){

        $resp = new BaseResponse();

        try{

            /*
             * Send the email
             * */


            $result = \Mail::to($mailTo)->send(new AccountCreationMail($content));

            // Laravel tells us exactly what email addresses failed, let's send back the first
            $fail = \Mail::failures();
            if(!empty($fail)) throw new \Exception('Could not send message to '.$fail[0]);

            if(empty($result)) throw new \Exception('Email could not be sent.');


            /*
             * Build response
             * */
            $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            return $resp;

        }catch (\Exception $exception){

            /*
            * log the error
            * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * Error occurred on sending the email
             * */
            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = $exception->getMessage();
            return $resp;

        }

    }


    public static function sendPlainTextEmailPHPMAILER($mailTo, $subject, $message){


        $resp = new BaseResponse();

        try{

            /*
             * Send the email
             * */


            $mail             = new PHPMailer(true);
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;  // debugging: 0 = off, 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth   = true;  // authentication enabled
            $mail->SMTPSecure = env('MAIL_ENCRYPTION', 'tls');;  // secure transfer enabled REQUIRED for Gmail
            $mail->Host       = env('MAIL_HOST', 'smtp.hostinger.com');
            $mail->Port       = env('MAIL_PORT', 587);
            $mail->IsHTML(true);


            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Username = env('MAIL_USERNAME', 'info@savemysoulinitiative.com');
            $mail->Password = env('MAIL_PASSWORD', 'Timothy93.com');
            $mail->SetFrom(env('MAIL_USERNAME', 'info@savemysoulinitiative.com'));
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AddAddress($mailTo);


            /*
             * We attempt to send the email
             * */
            if (!$mail->Send()) {

                $resp->statusCode = Globals::$STATUS_CODE_FAILED;
                $resp->statusDescription = 'Failed to Send Email';
                return $resp;

            }


            /*
             * Build success response
             * */
            $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = Globals::$STATUS_DESC_SUCCESS;
            return $resp;


        }catch (\Exception $exception){

            /*
            * log the error
            * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            /*
             * Error occurred on sending the email
             * */
            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = $exception->getMessage();
            return $resp;

        }

    }

    public static function emailAppraisalPendingSupervisorApproval(Appraisal $appraisal) {

        $resp = new BaseResponse();

        try{

            $supervisorUsername = $appraisal->supervisor_id;
            $user = User::where('username','=',$supervisorUsername)->first();

            if($user == null){
                $resp->statusCode = Globals::$STATUS_CODE_FAILED;
                $resp->statusDescription = "FAILED TO GET USER WITH USERNAME, ".$supervisorUsername;
                return $resp;
            }

            $email = $user->email;
            $msg = "Hello ".$user->first_name.", a new appraisal has been submitted pending your approval. Thank you";

            $emailResp = EmailHandler::sendPlainTextEmailPHPMAILER($email,"APPRAISAL PENDING APPROVAL", $msg);
            $resp->statusCode = $emailResp->statusCode;
            $resp->statusDescription = $emailResp->statusDescription;
            return $resp;

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = $exception->getMessage();
            return $resp;

        }

    }


    public static function emailLetterPendingReviewToRegistryUsers(Letter $letter, $users) {

        $resp = new BaseResponse();

        try{

            $countSuccess = 0;
            foreach ($users as $user){

                $email = $user->email;
                $msg = "Hello ".$user->first_name.", a new letter from ".$letter->sender." has been submitted to the registry pending review. Thank you";

                $emailResp = EmailHandler::sendPlainTextEmailPHPMAILER($email,Globals::$MSG_LETTER_FOR_ACTION, $msg);

                if($emailResp->statusCode == Globals::$STATUS_CODE_SUCCESS){
                    $countSuccess++;
                }

            }


            $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = "EMAIL SENT TO ".$countSuccess." OUT OF ".count($users). " REGISTRY USERS";
            return $resp;

        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = $exception->getMessage();
            return $resp;

        }

    }

}