<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/22/2019
 * Time: 10:49
 */


namespace App\Helpers;


class ConstAppraisalStatus {

    const PENDING_WORKFLOW_SUBMISSION = "000";
    const PENDING_SUPERVISOR_APPROVAL = "001";
    const REJECTED_BY_SUPERVISOR = "002";
    const PENDING_DEPARTMENT_HEAD_APPROVAL = "003";
    const REJECTED_BY_DEPARTMENT_HEAD = "004";
    const PENDING_EXECUTIVE_DIRECTOR_APPROVAL = "005";
    const REJECTED_BY_EXECUTIVE_DIRECTOR = "006";
    const COMPLETED_SUCCESSFULLY = "007";
    const CANCELED_BY_OWNER = "008";

    public static function appraisalCanBeCancelled($status) {

        $cancellableStatus = [self::PENDING_WORKFLOW_SUBMISSION, self::REJECTED_BY_SUPERVISOR,
            self::REJECTED_BY_DEPARTMENT_HEAD, self::REJECTED_BY_EXECUTIVE_DIRECTOR];

        return in_array($status, $cancellableStatus);

    }


}