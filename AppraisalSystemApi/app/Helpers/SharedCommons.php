<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/28/2019
 * Time: 08:49
 */


namespace App\Helpers;


use DateTime;

class SharedCommons {

    public static function dateRangesOverlap(
        DateTime $range1StartDate, DateTime $range1EndDate, DateTime $range2StartDate, DateTime $range2EndDate){

        try{

           // (StartDate1 <= EndDate2) and (StartDate2 <= EndDate1)
            return ($range1StartDate <= $range2EndDate) and ($range2StartDate <= $range1EndDate);

        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception, __CLASS__,__METHOD__);
            return false;
        }

    }

}