<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/21/2019
 * Time: 12:52
 */


namespace App\Helpers;


class ActionStatus {

    const PENDING = 'PENDING';
    const REJECTED = 'REJECTED';
    const SUBMITTED = 'SUBMITTED';
    const ACTIONED = 'ACTIONED';

}