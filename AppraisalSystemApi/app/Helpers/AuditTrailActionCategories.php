<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/14/2019
 * Time: 10:20
 */


namespace App\Helpers;


class AuditTrailActionCategories {

    public static $UPDATE = "UPDATE";
    public static $DELETE = "DELETE";
    public static $CREATE = "CREATE";
    public static $LOGIN = "LOGIN";
    public static $LOGOUT = "LOGOUT";
    public static $PASSWORD_RESET = "PASSWORD RESET";
    public static $PASSWORD_CHANGE = "PASSWORD CHANGE";

}