<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/26/2019
 * Time: 11:10
 */


namespace App\Helpers;


use App\AppraisalCompetenceCategory;
use App\BaseResponse;
use App\Http\Controllers\AppraisalCompetenceCategoryController;
use App\Http\Controllers\AppraisalController;
use App\Models\CompetenceAssessmentScore;
use App\Models\CompetenceCategory;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;

class PdfHandler {

    public static function saveToFolder($appraisalRef){

        $resp = new BaseResponse();

        try{

            /*
             * Get the appraisal
             * */
            $appraisal = AppraisalController::findAppraisalByAppraisalReference($appraisalRef, false);

            /*
             * Get the different relation details
             * */
            $user = $appraisal->user;
            $appraisalPersonalDetail = $appraisal->appraisalPersonalDetail;
            $appraisalAcademicBackgrounds = $appraisal->appraisalAcademicBackgrounds;
            $appraisalKeyDuties = $appraisal->appraisalKeyDuties;
            $appraisalAssignments = $appraisal->appraisalAssignments;
            $appraisalAdditionalAssignments = $appraisal->appraisalAdditionalAssignments;
            $appraisalCompetenceAssessments = $appraisal->appraisalCompetenceAssessments;
            $appraisalTrainPerformanceGaps = $appraisal->appraisalTrainPerformanceGaps;
            $appraisalTrainChallenges = $appraisal->appraisalTrainChallenges;
            $appraisalWorkplans = $appraisal->appraisalWorkplans;
            $appraisalAppraiserComment = $appraisal->appraisalAppraiserComment;
            $appraisalStrengthAndWeekness = $appraisal->appraisalStrengthAndWeekness;
            $appraisalAppraiserRecommendation = $appraisal->appraisalAppraiserRecommendation;
            $appraisalSupervisorDeclaration = $appraisal->appraisalSupervisorDeclaration;
            $appraisalHodComment = $appraisal->appraisalHodComment;
            $appraisalAppraiseeRemark = $appraisal->appraisalAppraiseeRemark;
            $appraisalDirectorComment = $appraisal->appraisalDirectorComment;
            $appraisalTrainSummary = $appraisal->appraisalTrainSummary;
            $appraisalAssignmentSummary = $appraisal->appraisalAssignmentSummary;
            $appraisalAssignmentScore = $appraisal->appraisalAssignmentScore;
            $appraisalAdditionalAssignmentScore = $appraisal->appraisalAdditionalAssignmentScore;
            $appraisalCompetenceAssessmentSummary = $appraisal->appraisalCompetenceAssessmentSummary;
            $appraisalCompetenceAssessmentScore = $appraisal->appraisalCompetenceAssessmentScore;


            /*
             * Get the competence categories based on the user's employee categories
             * */
            $competenceCategories = (new AppraisalCompetenceCategoryController())->getCategorizedCompetencesForAUser($user);
            /*
             * Attach a list of competence assessment scores to each of these categories
             * */
            $categorizedCompetences = self::getCompetenceCategoriesWithCompetencesAssessmentScores($appraisalRef, $competenceCategories);

            $params = [
                'appraisal',
                'user',
                'appraisalPersonalDetail',
                'appraisalAcademicBackgrounds',
                'appraisalKeyDuties',
                'appraisalAssignments',
                'appraisalAdditionalAssignments',
                'appraisalCompetenceAssessments',
                'appraisalTrainPerformanceGaps',
                'appraisalTrainChallenges',
                'appraisalWorkplans',
                'appraisalAppraiserComment',
                'appraisalStrengthAndWeekness',
                'appraisalAppraiserRecommendation',
                'appraisalSupervisorDeclaration',
                'appraisalHodComment',
                'appraisalAppraiseeRemark',
                'appraisalDirectorComment',
                'appraisalTrainSummary',
                'appraisalAssignmentSummary',
                'appraisalAssignmentScore',
                'appraisalAdditionalAssignmentScore',
                'appraisalCompetenceAssessmentSummary',
                'appraisalCompetenceAssessmentScore',
                'categorizedCompetences'
            ];


            /*
             * Create PDF instance using the template
             * */
            $pdf = PDF::loadView('templates.appraisal', compact($params));

            /*
             * Get the folder where to store the PDFs
             * */
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

            /*
             * Get the file name
             * */
            $fileName = $appraisal->generated_pdf_name . '.pdf';
            $filePath = $storagePath . '/public/' . $fileName;

            /*
             * Save the PDF
             * */
            $pdf->save($filePath);

            /*
             * Create download link
             * */
            $downloadLink =  asset('storage/'.$fileName);

            $resp->statusCode = Globals::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = $downloadLink;
            return $resp;


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            $resp->statusCode = Globals::$STATUS_CODE_FAILED;
            $resp->statusDescription = Globals::generalError($exception->getMessage());
            return $resp;

        }

    }

    private static function getCompetenceCategoriesWithCompetencesAssessmentScores($appraisalRef, $competenceCategories) {

        $updatedList = [];

        foreach ($competenceCategories as $competenceCategory){

            $customCategory = self::updateTheCategoryByAttachingTheAssessmentScores($appraisalRef, $competenceCategory);
            $updatedList[] = $customCategory;

        }

        return $updatedList;

    }

    private static function formatCompetenceAssessmentScores(array $assessmentList) {

        $dataList = [];

        foreach ($assessmentList as $item){

            $assessmentScore = new CompetenceAssessmentScore();
            $assessmentScore->assessmentId = $item['competence_assessment_id'];
            $assessmentScore->competenceId = $item['competence_id'];
            $assessmentScore->competence = $item['competence'];
            $assessmentScore->maxRating = $item['maximum_rating'];
            $assessmentScore->appraiseeRating = $item['appraisee_rating'];
            $assessmentScore->appraiserRating = $item['appraiser_rating'];
            $assessmentScore->agreedRating = $item['agreed_rating'];
            $assessmentScore->rank = $item['rank'];

            $dataList[] = $assessmentScore;

        }

        return $dataList;

    }

    /**
     * @param $appraisalRef
     * @param $competenceCategory
     * @return CompetenceCategory
     */
    private static function updateTheCategoryByAttachingTheAssessmentScores($appraisalRef, $competenceCategory) {

        $categoryId = $competenceCategory->id;
        $competenceAssessmentsResultSet = \DB::select('call GetAppraisalCompetenceAssessmentsForAGivenCompetenceCategory(?,?)', array($appraisalRef, $categoryId));

        $assessmentList = array_map(function ($value) { return (array)$value;  }, $competenceAssessmentsResultSet);

        /*
         * Put the assessments in an object of @CompetenceAssessmentScore
         * */
        $customCategory = new CompetenceCategory();
        $customCategory->category = $competenceCategory->competence_category;
        $customCategory->maximumRating = $competenceCategory->max_rating;
        $customCategory->competences = self::formatCompetenceAssessmentScores($assessmentList);
        return $customCategory;

    }

}