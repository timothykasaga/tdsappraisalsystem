<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/22/2019
 * Time: 10:49
 */


namespace App\Helpers;


class ConstLetterMovtStatus {

    const PENDING_WORKFLOW_SUBMISSION = "000";
    const AT_REGISTRY = "001";
    const AT_EXECUTIVE_DIRECTOR = "002";
    const ASSIGNED = "003";
    const ACTIONED_BY_ASSIGNEE = "004";

}