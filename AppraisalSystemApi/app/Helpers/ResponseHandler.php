<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/13/2019
 * Time: 16:01
 */


namespace App\Helpers;


class ResponseHandler {

    public static function failureResponse($statusDescription){

        $resp = [];

        //build the failure response
        $resp['statusCode'] = Globals::$STATUS_CODE_FAILED;
        $resp['statusDescription'] = $statusDescription;

        //return the failure response
        return $resp;

    }


    public static function successResponse($statusDescription){

        $resp = [];

        //build the success response
        $resp['statusCode'] = Globals::$STATUS_CODE_SUCCESS;
        $resp['statusDescription'] = $statusDescription;

        //return the success response
        return $resp;

    }


    public static function getFirstValidationError($validator, array $fieldsValidated) {

        //get the errors object
        $errors = $validator->errors();

        //return the first error you find
        foreach ($fieldsValidated as $field){

            if ($errors->has($field)) {
                return $errors->first($field);
            }
        }

        //this weird no error returned so check your $fieldsValidated list
        return "No error field found " . json_encode($errors);

    }


}