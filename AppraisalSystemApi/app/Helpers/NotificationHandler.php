<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/25/2019
 * Time: 11:58
 */


namespace App\Helpers;


use App\Models\ContractAboutToExpire;
use App\Models\ContractAboutToExpireCategory;
use App\User;
use Carbon\Carbon;

class NotificationHandler {

    const DATE_FORMAT_BIRTH_DATE = 'm-d';
    public static $CONTRACT_GP_0_1 = "0 - 1";
    public static $CONTRACT_GP_2_3 = "2 - 3";
    public static $CONTRACT_GP_4_5 = "4 - 5";
    public static $CONTRACT_GP_UNKNOWN = "unknown";

    const SUBJECT_BIRTHDAY_GREETING = "BIRTHDAY GREETING";
    const SUBJECT_CONTRACT_EXPIRY = "CONTRACT EXPIRY NOTIFICATION";

    /**
     * The method is responsible for sending birthday emails to users on their birthdays,
     * It is supposed to run every day at 8am
     */
    public static function notifyBirthday(){

        try{

            /*
             * Get list of users whose birthday is today
             * */
            $users = self::getUsersToSendBirthdayNotification();

            /*
             * No users found
             * */
            if(count($users) < 0){
                return false;
            }


            /*
             * For each user send the email
             * */
            foreach ($users as $user){

                self::sendBirthDayNotificationEmail($user);

            }

            return true;

        }catch (\Exception $exception){

            /*
             * Log the exception details
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            return false;

        }


    }

    private static function sendBirthDayNotificationEmail($user) {

        try{

            $email = $user->email;
            $subject = "BIRTHDAY GREETING";

            /*
             * Generate the email body
             * */
            $msg = self::buildBirthdayMessageBody($user);

            /*
             * Send the email
             * */
            EmailHandler::sendPlainTextEmailPHPMAILER($email,self::SUBJECT_BIRTHDAY_GREETING,$msg);


        }catch (\Exception $exception){
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
        }

    }

    private static function buildBirthdayMessageBody($user) {

        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $fullName = $firstName.' '.$lastName;

        $msg = "Hi ".strtoupper($fullName).", Destiny has a way of always bringing us positive influences in our lives. On the case of our company, 
        that influence was you! Keep on being amazing, and happy birthday!";

        return $msg;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private static function getUsersToSendBirthdayNotification() {

        /*
         * Get the date and month in form m-d e.g 01-31
         * */
        $dateNow = Carbon::now();
        $dateAndMonth = $dateNow->format(self::DATE_FORMAT_BIRTH_DATE);


        /*
         * Call stored procedure to get people who birthday is today
         * */
        $usersResultSet = \DB::select('call GetUsersBornToday(?)', array($dateAndMonth));
        $users = User::hydrate($usersResultSet);
        return $users;

    }


    /*
     * Sends an email of contracts about to expire to the Human Resource
     * */
    public static function notifyContractExpiry(){

        try{

            self::sendContractExpiryEmails();
            return true;

        }catch (\Exception $exception){

            /*
             * Log the exception details
             * */
            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);
            return false;

        }


    }

    private static function getListOfContractsThatAreAboutToExpiryByPeriodGroup($noOfMonthsLeftForNotification) {

        /*
         * Call stored procedure to get the contracts that are about to expire
         * */
        $contractsResultSet = \DB::select('call GetContractsThatAreAboutToExpire(?)', array($noOfMonthsLeftForNotification));

        /*
         * Convert this StdClass to an array to avoid that exception
         * */
        $resultSet = array_map(function ($value) {  return (array)$value;  }, $contractsResultSet);

        /*
         * Return a formatted list of contracts grouped by periods e.g 0-1, 1-2
         * */
        return self::getPeriodGroupedDataForContractsAboutToExpire($resultSet);

    }

    private static function getPeriodGroupForMonthsLeft($monthsLeft) {

        try{

            $zeroToOne = [0,1];
            $twoToThree = [2,3];
            $fourToFive = [4,5];

            if(in_array($monthsLeft, $zeroToOne)){
                return self::$CONTRACT_GP_0_1;
            }
            else if(in_array($monthsLeft, $twoToThree)){
                return self::$CONTRACT_GP_2_3;
            }
            else if(in_array($monthsLeft, $fourToFive)){
                return self::$CONTRACT_GP_4_5;
            }else{
                return self::$CONTRACT_GP_UNKNOWN;
            }

        }catch (\Exception $exception){

            return self::$CONTRACT_GP_UNKNOWN;

        }

    }

    /**
     * @param $contract
     * @return ContractAboutToExpire
     * @internal param $monthsLeft
     * @internal param $periodGroup
     */
    private static function getContractDetails($contract) {

        $monthsLeft = $contract['months_left'];
        $periodGroup = self::getPeriodGroupForMonthsLeft($monthsLeft);

        $item = new ContractAboutToExpire();
        $item->username = $contract['username'];
        $item->firstName = $contract['first_name'];
        $item->lastName = $contract['last_name'];
        $item->fullName = $item->firstName . ' ' . $item->lastName;
        $item->contractReference = $contract['contract_reference'];
        $item->monthsLeft = $monthsLeft;
        $item->daysLeft = $contract['days_left'];;
        $item->startDate = $contract['start_date'];
        $item->expiryDate = $contract['expiry_date'];
        $item->periodGroup = $periodGroup;
        return $item;
    }


    /**
     * Method is used to send contract expiry emails to the HR and to each individual
     */
    private static function sendContractExpiryEmails() {

        /*
         * Send email to HR
         * */
        self::sendContractExpiryEmailToHumanResource();


        /*
         * Send email to individuals
         * */
        self::sendContractExpiryEmailToIndividuals();

    }

    private static function sendContractExpiryEmailToHumanResource() {

        try{

            /*
             * Get list of contracts that are about to expire
             * */
            $contractsByPeriodGroup = self::getListOfContractsThatAreAboutToExpiryByPeriodGroup(Globals::$NO_OF_MONTHS_LEFT_FOR_NOTIFICATION);

            /*
             * No contracts found
             * */
            if(count($contractsByPeriodGroup) <= 0){
                return false;
            }

            /*
             * Send the contracts to HR users
             * */
            self::sendToHrUsers($contractsByPeriodGroup);
            return true;


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception, __CLASS__,__METHOD__);
            return false;

        }

    }

    private static function sendContractExpiryEmailToIndividuals() {

        try{

            /*
             * Get list of users with contracts that are about to expire
             * */
            $userResultSet = \DB::select('call GetUsersWithContractsThatAreAboutToExpire(?)', array(Globals::$NO_OF_MONTHS_LEFT_FOR_NOTIFICATION));
            $userList = array_map(function ($value) {  return (array)$value;  }, $userResultSet);


            /*
             * If no users with contracts that are about to expire, return
             * */
            if(count($userList) < 1){
                return;
            }


            /*
             * Send notifications for each user
             * */
            self::fetchIndividualContractsAndSendNotification($userList);


        }catch (\Exception $exception){

            LogHandler::logExceptionError($exception, __CLASS__,__METHOD__);

        }

    }

    private static function buildContractDetailsEmailSection(array $contractsByPeriodGroups) {

        $body = "";

        foreach ($contractsByPeriodGroups as $periodGroup ){

            if($periodGroup instanceof ContractAboutToExpireCategory){

                /*
                 * Build the HTML using blade template to simplify your life
                 * */
                $periodGroupData = view('emails.contract-details-hr', compact('periodGroup'))->render();
                $body = $body . $periodGroupData;

            }

        }

        return $body;

    }

    private static function fetchIndividualContractsAndSendNotification(array $userList) {

        foreach ($userList as $user){

            $username = $user['username'];
            $fullName = $user['first_name'] .' '.$user['last_name'];
            $email = $user['email'];
            self::sendContractDetailsEmailToEmployee($username, $fullName, $email);

        }

    }

    private static function sendContractDetailsEmailToEmployee($username, $fullName, $email) {

        /*
         * Use a stored procedure to get the list of contracts that are about to expire
         * */
        $contractsResultSet = \DB::select('call GetUsersContractsThatAreAboutToExpireByUserName(?,?)', array($username,Globals::$NO_OF_MONTHS_LEFT_FOR_NOTIFICATION));
        $resultSet = array_map(function ($value) {  return (array)$value;  }, $contractsResultSet);

        /*
         * Group the contracts based on the period e.g 0-1, 2-3, 4-5
         * */
        $contractsByPeriodGroup = self::getPeriodGroupedDataForContractsAboutToExpire($resultSet);

        /*
         * Create a table with the details of the contracts
         * */
        $contractDetails = self::buildContractDetailsEmailSection($contractsByPeriodGroup);

        /*
         * Other email details
         * */
        $subject = self::SUBJECT_CONTRACT_EXPIRY;
        $msgHeader = "<div>Hello ".$fullName. ", please find below the list of your contract(s) that are about to expire.</div>";
        $footer =    "<br><div style='font-weight: bold'>THANK YOU</div>";

        $msg = $msgHeader . $contractDetails . $footer;

        /*
         * Send the email
         * */
        EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $msg);

    }

    /**
     * This method gets an array of user contracts and groups them into period groups based on the number of months left
     * @param $resultSet
     * @return array
     */
    private static function getPeriodGroupedDataForContractsAboutToExpire(array $resultSet) {

        /*
         * Creating the different contract period groups
         * */
        $contractsOneToTwo = new ContractAboutToExpireCategory();
        $contractsOneToTwo->category = self::$CONTRACT_GP_0_1;

        $contractsTwoToThree = new ContractAboutToExpireCategory();
        $contractsTwoToThree->category = self::$CONTRACT_GP_2_3;

        $contractsFourToFive = new ContractAboutToExpireCategory();
        $contractsFourToFive->category = self::$CONTRACT_GP_4_5;

        $contractsOther = new ContractAboutToExpireCategory();
        $contractsOther->category = self::$CONTRACT_GP_UNKNOWN;


        /*
         * Loop through the list of contracts as you attach the contract to one of the groups above
         * */
        foreach ($resultSet as $contract) {

            /*
             * Get the contract details
             * */
            $item = self::getContractDetails($contract);

            /*
             * Based on its periodGroup, attach it to the right group
             * */
            if ($item->periodGroup == self::$CONTRACT_GP_0_1) {

                $contractsOneToTwo->contracts[] = $item;

            } else if ($item->periodGroup == self::$CONTRACT_GP_2_3) {

                $contractsTwoToThree->contracts[] = $item;

            } else if ($item->periodGroup == self::$CONTRACT_GP_4_5) {

                $contractsFourToFive->contracts[] = $item;

            } else if ($item->periodGroup == self::$CONTRACT_GP_UNKNOWN) {

                $contractsOther->contracts[] = $item;

            }

        }

        /*
         * Holds a list of data type ContractAboutToExpireCategory
         * */
        $listContractsAboutToExpire = [];

        /*
         * Add the different categories to the array
         * */
        if (count($contractsOneToTwo->contracts) > 0) {
            $listContractsAboutToExpire[] = $contractsOneToTwo;
        }

        if (count($contractsTwoToThree->contracts) > 0) {
            $listContractsAboutToExpire[] = $contractsTwoToThree;
        }

        if (count($contractsFourToFive->contracts) > 0) {
            $listContractsAboutToExpire[] = $contractsFourToFive;
        }

        return $listContractsAboutToExpire;

    }

    /**
     * @param $contractsByPeriodGroup
     */
    private static function sendToHrUsers(array $contractsByPeriodGroup) {

        /*
         *Get HR users
         * */
        $usersResultSet = \DB::select('call GetHumanResourceUsers()', array());
        $users = User::hydrate($usersResultSet);


        /*
         * Put the contract details in table
         * */
        $contractDetails = self::buildContractDetailsEmailSection($contractsByPeriodGroup);


        /*
         * Send email to HR users
         * */
        foreach ($users as $user) {

            $fullName = $user->first_name . ' ' . $user->last_name;
            $email = $user->email;
            $subject = "HR " . self::SUBJECT_CONTRACT_EXPIRY;

            $msgHeader = "<div>Hello " . $fullName . ", please find below the list of users whose contracts are about to expire.</div>";
            $footer = "<br><div style='font-weight: bold'>THANK YOU</div>";

            $msg = $msgHeader . $contractDetails . $footer;

            EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $msg);

        }

    }

}