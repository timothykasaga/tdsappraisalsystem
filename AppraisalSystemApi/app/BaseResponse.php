<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/17/2019
 * Time: 11:44
 */


namespace App;


class BaseResponse {

    public $statusCode = "";
    public $statusDescription = "";

}