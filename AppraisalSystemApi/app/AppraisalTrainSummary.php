<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalTrainSummary extends Model
{

    public function appraisal(){
        return $this->belongsTo('App\Appraisal','appraisal_reference','appraisal_reference');
    }

}
