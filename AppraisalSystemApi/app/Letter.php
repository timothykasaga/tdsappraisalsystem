<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'letter_code';
    }

    public function receptionUser(){
        return $this->belongsTo('App\User','receptionist_id','username');
    }

    public function letterDetail(){
        return $this->hasOne('App\LetterDetail','letter_code','letter_code');
    }

    public function letterMovements(){
        return $this->hasMany('App\LetterMovement','letter_code','letter_code');
    }

}
