<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountCreationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $content;

    /**
     * Create a new message instance.
     *
     * @param $message
     */
    public function __construct($content)
    {
        $this->$content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@savemysoulinitiative.com','TDS SUPPORT TEAM')
            ->subject('TDS APPRAISAL SYSTEM CREDENTIALS')
            ->view('emails.account-created');
    }

}
