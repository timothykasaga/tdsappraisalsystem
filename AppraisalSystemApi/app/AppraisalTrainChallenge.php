<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalTrainChallenge extends Model
{

    public function appraisal(){
        return $this->belongsTo('App\Appraisal','appraisal_reference','appraisal_reference');
    }

}
