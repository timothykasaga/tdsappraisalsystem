<?php

namespace App\Exceptions;

use App\Helpers\Globals;
use App\Helpers\LogHandler;
use App\Helpers\ResponseHandler;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof ModelNotFoundException || $exception instanceof NotFoundHttpException)  {

            $message = $exception  instanceof ModelNotFoundException ? "Not Found" : "Page Not Found";

            $resp = ResponseHandler::failureResponse($message);
            return response()->json($resp, 200);

        }

        if($request->ajax()){

            LogHandler::logExceptionError($exception,__CLASS__,__METHOD__);

            $error = "[".$exception->getCode(). "] " .Globals::$MSG_ERROR_CATASTROPHE;
            return response()->json($error, 500);
        }

        return parent::render($request, $exception);

    }

}
