<?php

namespace App\Console\Commands;

use App\Helpers\NotificationHandler;
use Illuminate\Console\Command;

class NotificationsForHrContractExpiries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:contract-expiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies HR and individual staff members of contracts that are about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(NotificationHandler::notifyContractExpiry()){

            $this->info('Contract expiry notifications have been sent');

        }else{

            $this->info('Something went wrong on sending contract expiry notifications');

        }

    }
}
