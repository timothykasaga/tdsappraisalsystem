<?php

namespace App\Console\Commands;

use App\Helpers\NotificationHandler;
use Illuminate\Console\Command;

class NotificationsForBirthDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends birthday notifications to staff members';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(NotificationHandler::notifyBirthday()){

            $this->info('Birthday notifications have been sent');

        }else{

            $this->info('Something went wrong on sending birthday notifications');

        }

    }
}
