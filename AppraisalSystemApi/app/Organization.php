<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'org_code';
    }




    /*
     *
     * Beginning of relations for this end
     * */

    public function regionalOffices(){

        return $this->hasMany('App\RegionalOffice','org_code','org_code');

    }

    public function departments(){

        return $this->hasMany('App\Department','org_code','org_code');

    }

    public function roleCodes(){

        return $this->hasMany('App\RoleCode','org_code','org_code');

    }

    public function employeeCategories(){

        return $this->hasMany('App\EmployeeCategory','org_code','org_code');

    }

    public function users(){

        return $this->hasMany('App\User','org_code','org_code');

    }

    public function appraisalStrategicObjectives(){

        return $this->hasMany('App\AppraisalStrategicObjective','org_code','org_code');

    }

    /*
     *
     * End of relationships for this model
     * */


}
