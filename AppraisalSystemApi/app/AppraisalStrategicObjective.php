<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalStrategicObjective extends Model
{

    public function organization(){

        return $this->belongsTo('App\Organization','org_code','org_code');

    }

}
