<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DocumentController extends CI_Controller {
 	  
	  public function __construct()
        {
                parent::__construct();
				$this->load->model(['CartModel']);
				$this->load->library('email');
                 
        }
	
	public function webuserlogin(){
		$post = $this->input->post();
		
		$secret="6Lf3QVkUAAAAAP7GsTVkl8gpLB3O-dcGv7I8UHr2";
		$response=$post["captcha"];
		
		$verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
		$captcha_success=json_decode($verify);
		if ($captcha_success->success==false) {
		  //This user was not verified by recaptcha.
			 
			$message = ['status' => 0, 'message' => 'Please re-enter your reCAPTCHA ! '];
			echo json_encode($message); die;
		}
		else if ($captcha_success->success==true) {
		  //This user is verified by recaptcha
 
		$this->OuthModel->CSRFVerify();
  		
   		$data = [
  					'username' => $post['email'],
  				];
 		
 		$result = $this->OveModel->Authentication_Check($data);
		if($result != false){
			
			 $login_user_id = $result['id'];
			 $user = $this->OveModel->Read_User_Information($login_user_id);
			 $hashed=$user['password'];
 			
 			  if($this->OuthModel->VerifyPassword($post['password'],$hashed) == 1){
				  
				 if($user['role'] == 'document'){
					 
					  if($user['is_email_verify'] == '0'){ // email verify
 						$message = ['status' => 0, 'message' => 'Your email has not been verified. please check your registerd email !'];
					  }else if($user['status'] == '2'){ // admin status blocked
						$message = ['status' => 0, 'message' => 'Your account is blocked please contact administrator ! '];
 					  }else{
					 	$userdata = [
							'id'  => $user['id'],
							'username'  => $user['username'],
							'email'     => $user['email'],
							'name'     => $user['name'],
							'role' => $user['role'],
							//'last_logged' =>  $user['lastlogged'],
							'created' =>  $user['created'], 
							'User_logged_in' => 'TRUE'
						];
						 
 						$this->session->set_userdata('User',$userdata);
						$guest_user_id = $this->session->userdata('guest_user_id');
						$check_user_session_cart_data =  $this->CartModel->GetGuestUserDataLoginTime($guest_user_id);
						
						if($check_user_session_cart_data != false){
							$this->CartModel->UpdateGuestUserDataLoginSuccess($guest_user_id,$user['id']);
						}
  						$message = [ 'status' =>1 , 
								'message' => 'You are now successfully Login !', 
								'userDataDB' => $userdata, 
								'redirectUrl' => base_url('my-account')
							];
					}//else
				
 				 }else if($user['role'] == 'Vendor' || $user['role'] == 'Client_cs'){
					
					if($user['is_email_verify'] == '0'){ // email verify
 						$message = ['status' => 0, 'message' => 'Your email has not been verified. please check your registerd email !'];
							
					}else if($user['status'] == '0'){ // admin approve status
						$message = ['status' => 0, 'message' => 'Your account not activated please contact administrator ! '];
							
					}else if($user['status'] == '2'){ // admin status blocked
						$message = ['status' => 0, 'message' => 'Your account is blocked please contact administrator ! '];
 					}else{ // finally visit account page
					
					$userdata = [
							'id'  => $user['id'],
							'username'  => $user['username'],
							'email'     => $user['email'],
							'name'     => $user['name'],
							'role' => $user['role'],
							//'last_logged' =>  $user['lastlogged'],
							'created' =>  $user['created'], 
							'logged_in' => 'TRUE'
						];
					
 				 	 	$this->session->set_userdata('Admin',$userdata);
						$message = [ 'status' =>1 , 
								'message' => 'You are now successfully Login !', 
								'userDataDB' => $userdata, 
								'redirectUrl' => base_url('v3/dashboard')
							];
 					}
 				 }else{
 				 	$message = [ 'status' =>0 , 'message' => 'Unauthorized access !' ];
				 }
 			}else{
				$message = [ 'status' =>0 , 'message' => 'Your password is Incorrect  !' ];
			}
 		}else{
			 $message = [ 'status' =>0 , 'message' => 'Your username is Incorrect  !' ];
		}
 		 echo json_encode($message);
	  }
 	}
   	public function create_doc(){

		$post = $this->input->post();

		$document_subject = $post['document_subject'];
		$document_type = $post['document_type'];
		$receipt_date = $post['receipt_date'];
		$receiving_officer = $post['receiving_officer'];
		$date_to_destination = $post['date_to_destination'];
		$invoice_number = $post['invoice_number'];
		$invoice_value = $post['invoice_value'];
		$currency = $post['invoice_value'];
		$sender = $post['sender'];
		$sender_reference_number = $post['sender_reference_number'];
		$letter_date = $post['letter_date'];
		$attention_to = $post['attention_to'];

		$signatory = $post['signatory'];
			$link_to_edms = $post['link_to_edms'];

		 $this->UserModel->createDocument2($document_subject, $document_type,
			$receipt_date, $receiving_officer,
			$date_to_destination, $invoice_number, $invoice_value,
			$currency, $sender, $sender_reference_number, $letter_date,
			$attention_to, $signatory,
			$currency, $link_to_edms);

//		if($user_id != false) {
			$from_email = 'info@ppda.go.ug';


			$message = "New document created\n" . $document_type . "\n" . $receipt_date;
			// Un comment later
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'smtp.sendgrid.net', //smtp host name
				'smtp_port' => '587',
				'smtp_user' => 'muytaba',
				'smtp_pass' => '@Password24',
				'crlf' => "\r\n",
				'newline' => "\r\n",
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = 'management@ppda.go.ug';
		  $to = 'julianahebwa@gmail.com';
		$this->load->library('email', $config);
			$this->email->from($from_email, 'Docman');
			$this->email->to($to);
			$this->email->subject('DOCMAN Support: New Document Created ' . $document_type);
			$this->email->message($message);
			$this->email->set_newline("\r\n");
			$this->email->send();
		$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);
			$data['msg'] = '<p style="color:green!important;">Document successfully created.</p>';

			$this->load->view('document/document_create_template', $data);

//		}else{
//
//			$data['msg'] ='Failed to create document';
//			$this->load->view('document/document_create_template', $data);
//		}

 		
 	}

	public function receptionist_create_doc(){

		$post = $this->input->post();

		 $document_subject = $post['document_subject'];
		 $document_type = $post['document_type'];
		 $receipt_date = $post['receipt_date'];
		 $receiving_officer = $post['receiving_officer'];
		 $date_to_destination = $post['date_to_destination'];
		 $invoice_number = $post['invoice_number'];
		 $invoice_value = $post['invoice_value'];
		 $currency = $post['invoice_value'];
		 $sender = $post['sender'];
		 $sender_reference_number = $post['sender_reference_number'];
		 $letter_date = $post['letter_date'];
		 $attention_to = $post['attention_to'];

		 $signatory = $post['signatory'];




		 $this->UserModel->createDocument($document_subject, $document_type, $receipt_date,
			$receiving_officer, $date_to_destination, $invoice_number, $invoice_value, $currency, $sender,
			$sender_reference_number, $letter_date, $attention_to,$signatory);
         $doctype = $document_type;


			$from_email = 'info@ppda.go.ug';

//			$subject = $post['document_subject'];
//			$date_of_receipt = $post['receipt_date'];
			$message = "A document has been received by PPDA with the following details: \n". 'Sender:' .$sender."\n". 'Subject' .$document_subject."\n". 'Date of receipt: ' .$receipt_date;
		$message_invoice = "A document has been received by PPDA with the following details: \n". 'Invoice origin:' .$signatory."\n". 'Invoice value' .$invoice_value."\n".'Invoice Subject'.$document_subject."\n".'Date of receipt: ' .$receipt_date;
			// Un comment later
			$config=Array(
				'protocol'=> 'smtp',
				'smtp_host' => 'smtp.sendgrid.net', //smtp host name
				'smtp_port' => '587',
				'smtp_user' => 'muytaba',
				'smtp_pass' => '@Password24',
				'crlf' => "\r\n",
				'newline' => "\r\n",
				'mailtype' =>'text',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			);

			$this->email->initialize($config);
		$this->load->library('email',$config);

		if($doctype === 'Letter' || $doctype === 'Letter-Review'){

			//$to = 'finance@ppda.go.ug';
			$to = 'julianahebwa@gmail.com';

			$this->email->from($from_email, 'Docman');
			$this->email->to($to);
			$this->email->subject('A document has been Received by PPDA');
			$this->email->message($message);
			$this->email->set_newline("\r\n");
			$this->email->send();
			$this->load->library('encryption');
			$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

		}

			if($doctype === 'Letter-Invoice'){

				//$to = 'finance@ppda.go.ug';
				$to = 'julianahebwa@gmail.com';
				$this->email->from($from_email, 'Docman');
				$this->email->to($to);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message_invoice);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

			}

			if($doctype === 'Letter-General'){

				//$to = 'management@ppda.go.ug';
				$to = 'julianahebwa@gmail.com';

				$this->email->from($from_email, 'Docman');
				$this->email->to($to);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				$this->UserModel->save_mail($from_email,$document_subject,$receipt_date);

			}

			if($doctype === 'Letter-Report' || $doctype === 'Letter-Procurement'){

				//$to = 'management@ppda.go.ug';
				$to = 'julianahebwa@gmail.com';

				$this->email->from($from_email, 'Docman');
				$this->email->to($to);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

			}

			if($doctype === 'Letter-Administrative'){

				//$to1 = 'management@ppda.go.ug';
				$to1 ='julianahebwa@gmail.com';

				$this->email->from($from_email, 'Docman');
				$this->email->to($to1);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
//				$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);
				//$to2 = 'crc@ppda.go.ug';
				$to2 = 'julianahebwa@gmail.com';

				$this->email->from($from_email, 'Docman');
				$this->email->to($to2);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

			}

			if($doctype === 'Letter-Investigation' || $doctype === 'Letter-Tribunal'){

				//$to1 = 'management@ppda.go.ug';
				$to1 = 'julianahebwa@gmail.com';
				$this->email->from($from_email, 'Docman');
				$this->email->to($to1);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				//$to2 = 'legal@ppda.go.ug';
				$to2 = 'julianahebwa@gmail.com';
				$this->email->from($from_email, 'Docman');
				$this->email->to($to2);
				$this->email->subject('A document has been Received by PPDA');
				$this->email->message($message);
				$this->email->set_newline("\r\n");
				$this->email->send();
				//$this->load->library('encryption');
				$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

			}



			$data['msg']='<p style="color:green!important;">Document successfully created.</p>';

			$this->load->view('document/receptionist_document_create_template', $data);

	//	}else{

			//$data['msg'] ='Failed to create document';
		//	$this->load->view('document/document_create_template');
	//	}

	}

	public function verify_mail(){

 		$get = $this->input->get();
		$name = $this->OuthModel->Encryptor('decrypt',$get['is_email_verify']);
		$user_id = $this->OuthModel->Encryptor('decrypt',$get['txtdomain']);
 		$res = $this->UserModel->EmailVerifyStatusUpdate($user_id);
 		if(!empty($res)){
			if($res['is_email_verify']==0){
 				$r = $this->db->update('users',['is_email_verify' => 1],['id' => $user_id ] );
				if($r==1){
					$data['title']='Thank you your account has been activated successfully';
					$this->parser->parse('website/email_verify',$data);
				}
			}else{
 			 redirect(base_url());
			}
		}else{
			redirect(base_url());
		}


	}

 	public function webuserlogout(){

		$this->session->unset_userdata('User');
		redirect(base_url());
	}

	public function template(){
 		$this->parser->parse('document/document_create_template',[]);
 	}

 	public function receptionist_template(){
		$this->parser->parse('document/receptionist_document_create_template',[]);
	}
	public function ed(){
		$id = $this->input->get('id');
		$result['data'] = $this->UserModel->displayrecordsById($id);
		$this->load->view('document/distribution', $result);
	}

 	public function update(){
		$id = $this->input->get('id');
		$result['data'] = $this->UserModel->displayrecordsById($id);
	  	$this->load->view('document/update_template', $result);
	}




	public function update_doc(){
	  	$id = $this->input->post('did');
		$document_subject = $this->input->post('document_subject');
		$document_type = $this->input->post('document_type');
		$document_origin = $this->input->post('document_origin');
		$receipt_date = $this->input->post('receipt_date');
		$receiving_officer = $this->input->post('receiving_officer');
		$date_to_destination = $this->input->post('date_to_destination');
		$invoice_number = $this->input->post('invoice_number');
		$invoice_value = $this->input->post('invoice_value');
		$currency = $this->input->post('currency');
		$link_to_edms = $this->input->post('link_to_edms');

		$sender =   $this->input->post('sender');
		$sender_reference_number = $this->input->post('sender_reference_number');
		$letter_date = $this->input->post('letter_date');
		$attention_to = $this->input->post('attention_to');

		$signatory = $this->input->post('signatory');


		$data = array('document_subject'=>$document_subject,
			'document_type'=>$document_type,
			'document_origin'=>$document_origin,
			'receipt_date'=>$receipt_date,
			'receiving_officer'=>$receiving_officer,
			'date_to_destination'=>$date_to_destination,
			'invoice_number'=>$invoice_number,
			'invoice_value'=>$invoice_value,
			'currency'=>$currency,
			'link_to_edms'=>$link_to_edms,
			'sender'=>$sender,
			'sender_reference_number'=>$sender_reference_number,
			'letter_date'=>$letter_date,
			'signatory'=>$signatory);


		 $this->UserModel->updateDoc($id, $data);


		$id = $this->uri->segment(3);

	  	$data['data'] = $this->UserModel->displayrecordsById($id);

				$data['msg']='<p style="color:green!important;">Document successfully updated.</p>';

		$from_email = 'info@ppda.go.ug';

		$message = "New document created Type: \n".$document_type."\n".$receipt_date;

		// Un comment later
		$config=Array(
			'protocol'=> 'smtp',
			'smtp_host' => 'smtp.sendgrid.net', //smtp host name
			'smtp_port' => '587',
			'smtp_user' => 'muytaba',
			'smtp_pass' => '@Password24',
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'mailtype' =>'text',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);

		$this->email->initialize($config);
		$this->load->library('email',$config);

		//$to = 'management@ppda.go.ug';
		$to = 'julianahebwa@gmail.com';
		$this->email->from($from_email, 'Docman');
		$this->email->to($to);
		$this->email->subject('DOCMAN Support: New Document Created ' . $document_type);
		$this->email->message($message);
		$this->email->set_newline("\r\n");
		$this->email->send();
		//$this->load->library('encryption');
		$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

		$this->load->view('document/update_template', $data);


	}

	public function update_doc_ed(){
		$id = $this->input->post('did');
		$document_subject = $this->input->post('document_subject');
		$document_type = $this->input->post('document_type');
		$document_origin = $this->input->post('document_origin');
		$receipt_date = $this->input->post('receipt_date');
		$receiving_officer = $this->input->post('receiving_officer');
		$date_to_destination = $this->input->post('date_to_destination');
		$invoice_number = $this->input->post('invoice_number');
		$invoice_value = $this->input->post('invoice_value');
		$currency = $this->input->post('currency');
		$link_to_edms = $this->input->post('link_to_edms');

		$sender =   $this->input->post('sender');
		$sender_reference_number = $this->input->post('sender_reference_number');
		$letter_date = $this->input->post('letter_date');
		$attention_to = $this->input->post('attention_to');

		$signatory = $this->input->post('signatory');

		$action_officer = $this->input->post('action_officer');

		$action_officer_email = $this->input->post('action_officer_email');
		$action_officer_department = $this->input->post('action_officer_department');




		$data = array('document_subject'=>$document_subject,
			'document_type'=>$document_type,
			'document_origin'=>$document_origin,
			'receipt_date'=>$receipt_date,
			'receiving_officer'=>$receiving_officer,
			'date_to_destination'=>$date_to_destination,
			'invoice_number'=>$invoice_number,
			'invoice_value'=>$invoice_value,
			'currency'=>$currency,
			'link_to_edms'=>$link_to_edms,
			'sender'=>$sender,
			'sender_reference_number'=>$sender_reference_number,
			'letter_date'=>$letter_date,
			'signatory'=>$signatory,
			'action_officer'=>$action_officer,
			'action_officer_email'=>$action_officer_email,
			'action_officer_department'=>$action_officer_department
			);



		$this->UserModel->updateDoc($id, $data);


		$id = $this->uri->segment(3);

		$data['data'] = $this->UserModel->displayrecordsById($id);
		$from_email = 'info@ppda.go.ug';

		$message = "A document has been received by PPDA with the following details: \n". 'Invoice origin:' .$signatory."\n". 'Invoice value' .$invoice_value."\n".'Invoice Subject'.$document_subject."\n".'Date of receipt: ' .$receipt_date;



		// Un comment later
		$config=Array(
			'protocol'=> 'smtp',
			'smtp_host' => 'smtp.sendgrid.net', //smtp host name
			'smtp_port' => '587',
			'smtp_user' => 'muytaba',
			'smtp_pass' => '@Password24',
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'mailtype' =>'text',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
		);

		$this->email->initialize($config);
		$this->load->library('email',$config);

		//$to = 'management@ppda.go.ug';
		$to = $action_officer_email;
		$this->email->from($from_email, 'Docman');
		$this->email->to($to);
		$this->email->subject('DOCMAN Support: New Document Created ' . $document_type);
		$this->email->message($message);
		$this->email->set_newline("\r\n");
		$this->email->send();
		//$this->load->library('encryption');
		$this->UserModel->save_mail($from_email,$document_subject, $receipt_date);

		$data['msg']='<p style="color:green!important;">Document successfully updated.</p>';


		$this->load->view('document/update_template', $data);


	}
	public function doclist(){
		$this->OuthModel->CSRFVerify();
		 $id = '';
		$this->OuthModel->Encryptor('encrypt',$id);
	  	$result['docs'] = $this->UserModel->displayrecords();
	  	$this->load->view('document/doclist_template', $result);
	}



	public function deletedoc(){
	  	$id = $this->uri->segment(3);
	  	$this->UserModel->deleterecord($id);
	  	$result['docs'] = $this->UserModel->displayrecords();
	  	$this->load->view('document/doclist_template', $result);
	}

 	public function grid_data()
	{
		$this->OuthModel->CSRFVerify();
 		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
 		//print_r($requestData);

  		$table = "users";
		$fields = "id,name,email, mobile_no, created ,username,picture_url,status";
 		$id = '';
		$where = " WHERE `role` = 'Admin' ";
 		$sql = "SELECT ".$fields;
		$sql.=" FROM ".$table. $where;
 		//echo $sql;
 		$query = $this->db->query($sql);
		$queryqResults = $query->result();
 		$totalData = $query->num_rows(); // rules datatable
		$totalFiltered = $totalData; // rules datatable

		$where = " WHERE `role` = 'Admin' ";
 		$sql = "SELECT ".$fields;
 		$sql.=" FROM ".$table . $where ;


		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter

			$searchValue = $requestData['search']['value'];

			$sql.=" AND `name` LIKE '%".$searchValue."%' ";
			$sql.=" AND `username` LIKE '%".$searchValue."%' ";
 			$sql.=" OR `email` LIKE '%".$searchValue."%' ";
 			$sql.=" OR `mobile_no` LIKE '%".$searchValue."%' ";
		}


 		$query = $this->db->query($sql);
 		$totalFiltered = $query->num_rows(); // rules datatable
 		//ORDER BY id DESC
 		$sql.=" ORDER BY  created  ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
 		$query = $this->db->query($sql);
 		//echo $sql;
 		$SearchResults = $query->result();

  		$data = array();
		foreach($SearchResults as $row){
			$nestedData=array();

			$id = $row->id;

			$url_id= '"'.$this->OuthModel->Encryptor('encrypt',$row->id).'"';

			$tableCheckTD = "<label class='pos-rel'><input type='checkbox' class='ace' /><span class='lbl'></span></label>";
 			$action =  "<div class='action-buttons'>&nbsp;&nbsp;&nbsp;
 																<a onclick='trash($url_id)'  class='red trashID' href='javascript:void(0);'>
																	<i class='ace-icon fa fa-trash-o bigger-130'></i>
																</a>
 															</div>";

			if($row->status==2){
				$action1 = "<a onclick='block($url_id,1)' href='javascript:void(0);'><span class='label label-danger'>Unblock</span>&nbsp;";
			}if($row->status==0 ||$row->status==1){
				$action1 = "<a onclick='block($url_id,2)' href='javascript:void(0);'><span class='label label-danger'>Block</span>&nbsp;";
			}

			$imgUrl = base_url('public/images/user-icon.jpg');
			if(!empty($row->picture_url)){$imgUrl = base_url('uploads/profiles/'.$row->picture_url);	 }


			//if($row->status==0){ $statusTxt = "<a onclick='status($url_id,1)' href='javascript:void(0);'><span class='label label-warning'>InActive</span>";}
			//if($row->status==1){ $statusTxt = "<a onclick='status($url_id,0)' href='javascript:void(0);'><span class='label label-primary'>Active</span>";}

 			$nestedData[] = '<span class="nameID_'.$id.'">'.$row->id.'</span>';
			$nestedData[] = '<span class="nameID_'.$id.'"><img class="img-thumbnail imgcls" src="'.$imgUrl.'"></span>';
			$nestedData[] = '<span class="contactID_'.$id.'">'.$row->email.'</span>';
			$nestedData[] = '<span class="contactID_'.$id.'">'.$row->name.'</span>';
			$nestedData[] = '<span class="contactID_'.$id.'">'.$row->mobile_no.'</span>';

			//$nestedData[] = '<span class="contactID_'.$id.'">'.$statusTxt.'</span>';

 			$nestedData[] = $row->created;
			$nestedData[] =  $action.$action1;
  			$data[] = $nestedData;
		}
 		$json_data = array(
					"draw"            => intval( $requestData['draw'] ),
					"recordsTotal"    => intval( $totalData ),  // total number of records
					"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching,
					"data"            => $data   // total data array
					);
 		echo json_encode($json_data);  // send data as json format
 	}


	public function status(){
		$this->OuthModel->CSRFVerify();

		$status = $this->input->get('status');
		$q = $this->UserModel->StatusUpdateByID($this->OuthModel->Encryptor('decrypt',$this->input->get('id')),$status);
		$txtdomain='InActive';
		if($status==1){$txtdomain='Activate';}
		
 		if($q==true){
			
		 	$json=['status'=>1,'message'=>'User '.$txtdomain.' !'];
		}else{
			$json=['status'=>0,'message'=>'Failed to '.$txtdomain.' !'];
		}
		echo json_encode($json);
	}

//
//	public function sun_mailer($from_email, $message,$name){
//		$config=Array(
//			'protocol'=> 'smtp',
//			'smtp_host' => 'smtp.sendgrid.net', //smtp host name
//			'smtp_port' => '587',
//			'smtp_user' => 'muytaba',
//			'smtp_pass' => '@Password24',
//			'crlf' => "\r\n",
//			'newline' => "\r\n",
//			'mailtype' =>'html',
//			'charset' => 'iso-8859-1',
//			'wordwrap' => TRUE
//		);
//
//		$data['client_message'] = "Thank you for your feed back";
//
//
//		if($post['document_type'] = "Invoice"){
//			// Notification to registry
//			$this->load->library('email',$config);
//			$this->email->from('support@sunesiswarehouse.co', 'Docman');
//			$this->email->to($from_email);
//			$this->email->subject('DOCMAN Support: Document Created');
//			$this->email->message("Test message");
//			$this->email->set_newline("\r\n");
//			$this->email->send();
//		}
//
//
//
//		// Notification to edms
//		$data['support_msg'] = $message;
//		//send mail to mp3gorilla support
//		$this->load->library('email',$config);
//		$this->email->from($from_email, $name);
//		$this->email->to('julianahebwa@gmail.com');
//		$this->email->subject('DOCMAN Support: Document Created');
//		$support_message = $this->load->view('email/email_tmp_support',$data,TRUE);
//		$this->email->message($support_message);
//		$this->email->set_newline("\r\n");
//	}

	public function search(){
		$this->OuthModel->CSRFVerify();
		$data['query'] = $this->UserModel->get_search();
		$this->load->view('document/result', $data);
	}


	public function outgoing_list(){
		$result['docs'] = $this->UserModel->displayoutgoingrecords();
		$this->load->view('outgoing/outgoing_template', $result);
	}

	public function outgoing(){
	  	$this->load->view('outgoing/outgoing_create_template');
	}

	public function create_outgoing(){
		$post = $this->input->post();

		$outgoing_document_data = [
			'our_reference' => $post['our_reference'],
			'letter_date' => $post['letter_date'],
			'addressee' => $post['addressee'],
			'addressee_entity' => $post['addressee_entity'],
			'subject' => $post['subject'],
			'signatory' => $post['signatory'],
			'originator' => $post['originator'],
			'cc' => $post['cc'],
			'registry_receipt_date' => $post['registry_receipt_date'],
			'registry_dispatch_date' => $post['registry_dispatch_date'],
			'acknowledge_date' => $post['acknowledge_date'],
			'dispatch_officer' => $post['dispatch_officer']
		];
		$user_id = $this->UserModel->createOutgoingDocument($outgoing_document_data);


		if($user_id != false){

			$data['msg']='<p style="color:green!important;">Document successfully created.</p>';

			$this->load->view('outgoing/outgoing_create_template', $data);

		}else{

			$data['msg'] ='Failed to create outgoing document';
			$this->load->view('outgoing/outgoing_create_template', $data);
		}
	}

	public function outgoingupdate_template(){
		$id = $this->input->get('id');
		$result['data'] = $this->UserModel->displayoutgoingrecordsById($id);
		$this->load->view('outgoing/outgoing_update_template', $result);
	}

	public function updateoutgoing(){
		$id = $this->input->post('did');
		$our_reference = $this->input->post('our_reference');
		$subject = $this->input->post('subject');
		$letter_date = $this->input->post('letter_date');
		$addressee = $this->input->post('addressee');
		$addressee_entity = $this->input->post('addressee_entity');
		$signatory = $this->input->post('signatory');
		$originator = $this->input->post('originator');
		$cc = $this->input->post('cc');
		$registry_receipt_date = $this->input->post('registry_receipt_date');
		$acknowledge_date = $this->input->post('acknowledge_date');

		$dispatch_officer = $this->input->post('dispatch_officer');


		$data = array('our_reference'=>$our_reference,
			'subject'=>$subject,
			'letter_date'=>$letter_date,
			'addressee'=>$addressee,
			'addressee_entity'=>$addressee_entity,
			'signatory'=>$signatory,
			'originator'=>$originator,
			'cc'=>$cc,
			'registry_receipt_date'=>$registry_receipt_date,
			'acknowledge_date'=>$acknowledge_date,
			'dispatch_officer'=> $dispatch_officer);


		$this->UserModel->updateOutgoingDoc($id, $data);


		$id = $this->uri->segment(3);

		$data['data'] = $this->UserModel->displayrecordsById($id);

		$data['msg']='<p style="color:green!important;">Document successfully updated.</p>';

		$this->load->view('outgoing/outgoing_update_template', $data);
	}

	public function deleteoutgoingdoc(){
		$id = $this->uri->segment(3);
		$this->UserModel->deleteoutgoingrecord($id);
		$result['docs'] = $this->UserModel->displayoutgoingrecords();
		$this->load->view('outgoing/outgoing_template', $result);
	}


}
